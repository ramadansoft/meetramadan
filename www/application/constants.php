<?

// Для отладки
define('PR_NORMAL','Основной цикл программы');
define('PR_EXT','Обработка особых данных');

define('MAX_FILE_SIZE',10); // Максимальный размер загружаемого файла в мегабайтах

/**
 * Скрытое имя поле которое передавать для проверки от CSRF
 */
define('TOKEN', 'system_case');

define('const',1);

define('JSONA_NOTHING','nothing');
define('JSONA_REFRESHPAGE','refreshpage');
define('JSONA_RELOADPAGE','reloadpage');
define('JSONA_REDIRECT','redirect');
define('JSONA_SHOW_MESSAGE','showmessage');
define('JSONA_ERROR','error');
define('JSONA_COMPLETED','completed');
define('JSONA_NOTICE','notice');
define('JSONA_WARNING','warning');
define('JSONA_SET_TOKEN','set_system_case');
define('JSONA_SET_ADMIN_MENU','setadminmenu');
define('JSONA_CLOSE_CALL','closecall');
define('JSONA_SHOWLEFTBUTTONS','showleftuttons');
define('JSONA_DATA','data');
define('JSONA_UPDATE_STORY_INFO','updateStoryInfo');
define('JSONA_SHOW_LOGIN_WINDOW','showLoginWindow');

define('ROLE_LOGIN',1);
define('ROLE_ADMIN',2);

// Типы блогов в системе
define('BLOG_MALIK',1);
define('BLOG_NEWS',2);

define('RAMADAN_NEAR',1);       // Рамадан приближается
define('RAMADAN_STARTED',2);    // Рамадан идет полным ходом :)
define('RAMADAN_ENDED',3);      // Рамадан завершился
define('RAMADAN_UNKNOWN',4);    // Ошибка определения времени наступления Рамадана


