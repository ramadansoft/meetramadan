<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Template extends Controller_Template {

    /**
     * Шаблон подключаемых JS и CSS файлов
     * @var Document
     */
    protected $document_template = 'main';

    /**
     * Класс пользователя
     * @var user
     */
    protected $auth;  // Объект авторизации

    /**
     * Модель юзера, null если не авторизован
     * @var model_user
     */
    protected $user;  // Объект модели пользователя

    /**
     * Модель компании, null если нет компании
     * @var model_company
     */
    protected $_company = null;  // Объект модели компании, в которой работает пользователь

    protected $json_error = false; // Флаг, который переводится в True, если в где-то в коде было вызвано SendJSONData(JSONA_ERROR,'mess');

    protected $geo = null; // Объект с информацией по айпишнику

    /**
     * Шаблон
     * @var templateView
     */
    public $template;

    /**
     * Флаг, показывающий, чем является запрос - просто загрузкой страницы или открытием окошка по середине экрана
     * @var bool флаг
     */
    public $SHOWMESSAGE = false;

    private $benchmark;
    
    private $ramaran = null;
    /**
     * Возвразает объект БД с моделью с информацией о начале текущего рамадана
     * @return type
     */
    protected function ramadan() {
        if ($this->ramaran === null) {
            $this->ramaran = (int) ORM::factory('Ramadan',['year' => date('Y')])->startday;
        }
        return $this->ramaran;
    }
    private $mode = null;
    /**
     * Функция, возвращающая режим сайта, который зависит от текущего времени: до Рамадана, время Рамадана и после Рамадана
     */
    protected function mode() {
        if ($this->mode) return $this->mode;

        $date = new DateTime();
        $currentday = (int) $date->format('z');
        $ramadan = $this->ramadan();
        if ($ramadan) {
            if (($ramadan - 60) < $currentday && $ramadan > $currentday) {
                $this->mode = RAMADAN_NEAR;
            } elseif ($ramadan <= $currentday && ($ramadan + 29) >= $currentday) {
                $this->mode = RAMADAN_STARTED;
            } else {
                $this->mode = RAMADAN_ENDED;
            }
        } else {
            $this->mode = RAMADAN_UNKNOWN;
        }
        
        return $this->mode;
    }
    private $day = null;
    /**
     * Возвращает номер дня в текущем рамадане
     */
    protected function day() {
        if ($this->day) return $this->day;
        $date = new DateTime();
        $currentday = (int) $date->format('z');
        $this->day = $currentday - $this->ramadan() + 1;
        return $this->day;
    }

    public function before() {
        if (Kohana::$profiling === TRUE)
            $this->benchmark = Profiler::start(PR_NORMAL, __METHOD__);

        $this->auth = Auth::instance();
        $this->user = $this->auth->get_user();

		$this->template = 'templates/default';

        parent::before(); // Теперь превратим $this->template из текстовой строки в объект View

        // Пользователь всегда будет доступен в любой вьюшке по-умолчанию
        View::set_global('auth', $this->auth);
        View::set_global('user', $this->user);
        View::set_global('controller_link', $this->request->controller());
        View::set_global('MODE',$this->mode());
        View::set_global('CURRENT_DAY',$this->day());

        // Выплевываем заглушку, если человек не авторизован и не наступил месяц Рамадан
        //if (isset($_SERVER['SERVER_NAME']) && !$this->auth->logged_in() && $this->mode == RAMADAN_NEAR && !in_array($this->request->controller(),array('auth','page','statistics','today')))
    	//	die(View::factory('templates/cap')->render());
        
        // Определяем, что грузит клиент - всю страницу, или маленькое окошко
        if (isset($_GET['showmessage']) && $_GET['showmessage'] == 'true')
        {
            $this->SHOWMESSAGE = true;
            View::set_global('SHOWMESSAGE',true);
        } else {
            $this->SHOWMESSAGE = false;
            View::set_global('SHOWMESSAGE',false);
        }

        $this->template->content = '';
        $this->template->title = 'Встреть Рамадан!';
        $this->template->desc = '';
        $this->template->keywords = '';

        Document::loadTheme($this->document_template);
    }

    /**
     * Fill in default values for our properties before rendering the output.
     */
    public function after() {
        // Если это подзапрос или аяксовый запрос, то отдаем только тело контента, не рендеря весь шаблон
        if (((!$this->request->is_initial() || $this->request->is_ajax()) && ($this->auto_render === true)) && ((!isset($this->needfulltemplate)) || $this->needfulltemplate === false )) {
            $template = View::factory('templates/ajah');
            $template->content = $this->template->content;
            $template->title = $this->template->title;

            $this->response->body($template);
            $this->auto_render = false;
        }

        if ($this->auto_render) {
            // Получение html-кода заголовка
            $this->template->header = Document::compile($this->document_template.'-header');
            $this->template->footer = Document::compile($this->document_template.'-footer');
        }

        // Run anything that needs to run after this.
        parent::after();

        if (isset($this->benchmark))
            Profiler::stop($this->benchmark);
    }

    /**
     * Посылает клиенту данные в JSON-формате, которые потом клиентский яваскрипт распарсит и сделает, что надо.
     * @param string $type
     * @param string $data
     */
    protected function SendJSONData($type, $data = null) {
        $this->response->headers('Content-Type', 'application/json');

        if (isset($type['error']))
        {
            $this->json_error = true;
        }
        else
        {
            if ($type === JSONA_ERROR)
                $this->json_error = true;
            else
                $this->json_error = false;
        }

        $json_messages = array();
        if (!empty($type) && is_array($type))
        {
            foreach ($type as $key => $mess)
            {
                array_push($json_messages,array(
                    'type' => $key,
                    'data' => $mess
                ));
            }
        } else {
            $json_messages = array(array('type' => $type,'data'=>$data));
        }

        $this->response->body(json_encode($json_messages));
        $this->auto_render = false;
    }

    /**
     * Проверить, залогинен ли пользователь. Если не залогинен - отправить его на страницу авторизации.
     */
    public function checkLogin() {
        if (!$this->auth->logged_in()) {
            $this->redirect('/auth');
        }
    }

    /**
     * Проверить, завершил ли регистрацию пользователь. Если нет - отправить его на страницу завершения регистрации.
     */
    public function checkRegister() {
        $this->checkLogin();

        if (!$this->user->completed_register)
        {
            $this->redirect('/auth/completeregister');
        }
    }

    /**
     * Проверить, залогинен ли пользователь и является ли он админом.
     */
    public function checkAdminLogin() {
        if (!$this->auth->logged_in() || !$this->user->has('roles',ROLE_ADMIN)) {
            throw new HTTP_Exception_404;
        }
    }

    /**
     * Сообщает в каком формате требуется вернуть данные (HTML или JSON)
     */
    public function isJson()
    {
        if (isset($_REQUEST['json']) && $_REQUEST['json'] === 'true')
            return TRUE;
        else
            return FALSE;
    }

}

