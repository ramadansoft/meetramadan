<?

class Kohana_Exception extends Kohana_Kohana_Exception
{

    /**
     * При создании нового сообщения о ошибке добавим информацию в БД
     *
     * @param   string $message error message
     * @param   array $variables translation variables
     * @param   integer|string $code the exception code
     * @param   Exception $previous Previous exception
     */
    public function __construct($message = "", array $variables = null, $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $variables, $code, $previous);
        self::toDb($this->getCode(), get_class($this), $this->getFile(), $this->getMessage(), $this->getLine(), $this->getTrace());
     
        // Если мы на продакшене, то возвратим сжатое сообщение об ошибке
        if (! $this instanceof HTTP_Exception_Redirect
                && ! $this instanceof HTTP_Exception
                && ! $this instanceof ORM_Validation_Exception
                && Kohana::$environment == Kohana::PRODUCTION && PHP_SAPI != 'cli') {
            $this->log($this);
            $response = new Response;
            if ((isset($_POST['json']) && $_POST['json'] === 'true')
                        || mb_strtolower(Arr::get($_SERVER,'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
                $response->status(200); // Это чтобы JS вывел системное сообщение об ошибке
                $response->headers('Content-Type', 'application/json');
                $message = '[{"type":"error","data":"'.__('Data processing error. Try again later.').'"}]';
            } else {
                $response->status(500);
                $response->headers('Content-Type','text/html');
                $message = Request::factory('/throwMessage/error/500')->execute()->body();
            }
            die($response->body($message)->send_headers()->body());
        }
    }

    public static function toDb($code, $class, $file, $message, $line, $trace)
    {
        // Стираем аргументы методов, так как в них могут быть объекты, 
        // которые невозможно сериализовать, да и не особо необходимо их сохранять, 
        // и без них все понятно
        foreach ($trace as &$trline) {
            $trline['args'] = null;
        }

        if (defined('INIT_COMPLETED') && !in_array($code, [302, 404, 403]) && !in_array(
                $class,
                ['ORM_Validation_Exception']
            )
        ) {
            $user_id = Auth::instance()->get_user();
            if ($user_id !== null) {
                $user_id = $user_id->id;
            }

            $dberror = ORM::factory('Error');
            $dberror->date = time();
            $dberror->code = $code;
            $dberror->class = $class;
            $dberror->file = $file;
            $dberror->message = $message;
            $dberror->line = $line;
            $dberror->trace = gzcompress(serialize($trace));
            $dberror->users_id = $user_id;
            $dberror->uri = Arr::get($_SERVER,'HTTP_HOST') . Arr::get($_SERVER,'REQUEST_URI');
            $dberror->save();
        }
    }
}