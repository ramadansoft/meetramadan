<?

// Класс с разными небольшими функциями, которые нам необходимы в разных частях сайта
class etc {

    // Для правильного роутинга. Сгенерировать правило с добавлением всех статических страниц в него
    static function generatePages()
    {
        $pages = ORM::factory('Page')->find_all();
        return '(' . implode('|',Arr::Make1Array($pages,'lat_name')) . ')';
    }

}