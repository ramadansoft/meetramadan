<? class Model_Country extends ORM {
	protected $_table_name = 'countries';
	protected $_has_many = array(
		'regions' => array(
            'model' => 'Region',
            'foreign_key' => 'countries_id',
        ),
	);
}