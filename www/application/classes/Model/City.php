<? class Model_City extends ORM {
	protected $_table_name = 'cities';
	protected $_belongs_to = array(
		'country' => array(
            'model' => 'Country',
            'foreign_key' => 'country_id',
        ),
	);
    // Возвратить массив городов с названиями и координатами в формате массива
    static function getCities() {
        $cities = DB::select(
                    array('cities.id','id'),
                    array('cities.city','title'),
                    array('cities.lat','x'),
                    array('cities.long','y'),
                    array(DB::expr('COUNT(users.id)'),'users_count'),
                    array(DB::expr('ROUND((SELECT SUM(scores.score) FROM scores JOIN users as t2 ON scores.users_id = t2.id WHERE t2.cities_id = cities.id AND t2.completed_register = 1 AND scores.year = '.date('Y').' ) / COUNT(users.id))'),'worships')
                )
                ->from('cities')
                ->join('users','right')->on('users.cities_id','=','cities.id')
                ->where('users.cities_id','IS NOT',null)
                ->where('users.completed_register','=',1)
                ->where('cities.lat','IS NOT',null)
                ->group_by('cities.city')
                ->having('worships','>','0')
                //->limit(20)
                ->execute()
                ->as_array();

        return $cities;

    }

    // Возвратить подробный массив городов с названиями и координатами в формате массива
    static function getCitiesFull()
    {

        $cities = DB::select(
                    array('cities.id','id'),
                    array('cities.city','title'),
                    array(DB::expr('COUNT(users.id)'),'users_count'),
                    array(DB::expr('ROUND((SELECT SUM(scores.score) FROM scores JOIN users as t2 ON scores.users_id = t2.id WHERE t2.cities_id = cities.id AND t2.completed_register = 1 AND scores.year = '.date('Y').' ) / COUNT(users.id))'),'worships')
                )
                ->from('cities')
                ->join('users','right')->on('users.cities_id','=','cities.id')
                ->where('users.cities_id','IS NOT',null)
                ->where('cities.lat','IS NOT',null)
                ->order_by('worships','desc')
                ->group_by('cities.city')
                ->having('worships','>','0')
                ->as_object()
                ->execute();

        return $cities;
    }
}