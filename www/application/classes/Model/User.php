<?php

class Model_User extends Model_Auth_User {

    public $_add = array('fields' => array('fio','username', 'email','password'));
    public $_edit = array('fields' => array('fio','username', 'email'));

	public function __toString() {
		if ($this->loaded())
            if (!empty($this->username))
                return $this->username;
            else
                return $this->email;
		else
			return '';
	}

	protected $_has_many = array(
		'ulogins' => array(),
		'user_tokens' => array('model' => 'User_Token'),
		'roles'       => array('model' => 'Role', 'through' => 'roles_users'),
		'modificators' => array(
            'model' => 'Tests_Modificator',
            'through' => 'tests_modificators_has_users',
            'foreign_key' => 'users_id',
            'far_key' => 'tests_modificators_id',
        ),
		'likes' => array(
            'model'         => 'Story',
            'through'       => 'stories_likes',
            'foreign_key'   => 'user_id',
            'far_key'       => 'story_id',
        ),
	);

	protected $_belongs_to = array(
		'city' => array(
            'model' => 'City',
            'foreign_key' => 'cities_id',
        ),
	);

	/**
	 * Rules for the user model. Because the password is _always_ a hash
	 * when it's set,you need to run an additional not_empty rule in your controller
	 * to make sure you didn't hash an empty string. The password rules
	 * should be enforced outside the model or with a model helper method.
	 *
	 * @return array Rules
	 */
	public function rules()
	{
		return array(
			'password' => array(
				array('not_empty'),
			),
			'email' => array(
				array('not_empty'),
				array('email'),
				array(array($this, 'unique'), array('email', ':value')),
			),
		);
	}


    	/**
	 * Allows a model use both email and username as unique identifiers for login
	 *
	 * @param   string  unique value
	 * @return  string  field name
	 */
	public function unique_key($value)
	{
		return 'email';
	}

    static function getUsersByCity($city_id)
    {
        return DB::select(
                array('users.id','id'),
                array('users.username','username'),
                array(DB::expr('(SELECT SUM(score) FROM scores WHERE users_id = users.id AND year = '.date('Y').' )'),'worships'),
                array(DB::expr('(SELECT tests.name FROM scores JOIN tests ON tests.id = scores.tests_id WHERE users_id = users.id AND scores.year = '.date('Y').' GROUP BY tests_id ORDER BY count(tests_id) DESC LIMIT 1)'),'lovelyworship')
            )
            ->from('users')
            ->where('cities_id','=',$city_id)
            ->where('completed_register','=',1)
            ->order_by('worships','desc')
            ->having('worships','>','0')
            ->as_object()
            ->execute();
    }

    // Получить количество поклонений пользователя
    function getWorshipsCount()
    {
        if (!$this->loaded())
            return 0;

        return DB::select(array(DB::expr('COUNT(*)'),'worships'))
            ->from('scores')
            ->where('users_id','=',$this->id)
            ->where('year','=',date('Y'))
            ->execute()
            ->get('worships');

    }

    // Получить конечный счет пользователя
    function getTotalScore()
    {
        if (!$this->loaded())
            return 0;

        $db = DB::select(array(DB::expr('SUM(score)'),'worships'))
                ->from('scores')
                ->where('users_id','=',$this->id)
                ->where('year','=',date('Y'))
                ->execute()
                ->get('worships');
        if (empty($db))
            return 0;
        else return $db;

    }

    // Получить средний показатель среди пользователей всей системы
    static function getAverageUsersScore() {
        $db = DB::select(array(DB::expr('ROUND((SELECT SUM(score) FROM scores WHERE scores.year = '.date('Y').') / COUNT(*))'),'average'))
                ->from('users')
                ->where('completed_register','=',1)
                ->execute()
                ->get('average');
        return $db;
    }


}