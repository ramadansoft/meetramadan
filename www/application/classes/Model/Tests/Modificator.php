<?

class Model_Tests_Modificator extends ORM {

	protected $_table_name = 'tests_modificators';

	protected $_has_many = array(
		'scores' => array(
            'model' => 'Tests_Score',
            'foreign_key' => 'tests_modificators_id',
        ),
		'scores_tests' => array(
            'model'         => 'Test',
            'through'       => 'tests_scores',
            'foreign_key'   => 'tests_id',
            'far_key'       => 'tests_modificators_id',
        ),
		'users' => array(
            'model'         => 'User',
            'through'       => 'tests_modificators_has_users',
            'foreign_key'   => 'users_id',
            'far_key'       => 'tests_modificators_id',
        ),
	);

    function tests() {
        $scores = $this->scores->find_all();
        $tests = array();

        foreach($scores as $score) {
            $tests[] = array(
                'id' => $score->test->id,
                'name' => $score->test->name
            );
        }

        return $tests;
    }
}

?>
