<?

class Model_Tests_Score extends ORM {

	protected $_table_name = 'tests_scores';

	protected $_belongs_to = array(
		'test' => array(
            'model' => 'Test',
            'foreign_key' => 'tests_id',
        ),
		'modificator' => array(
            'model' => 'Tests_Modificator',
            'foreign_key' => 'tests_modificators_id',
        ),
	);
}

?>
