<?

class Model_Video extends ORM {

	protected $_table_name = 'videos';

    static function getRandom() {
        $videos = Arr::Make1Array(DB::select('id')->from('videos')->execute(),'id');
        $video_id = $videos[array_rand($videos)];
        return ORM::factory('Video',$video_id);
    }

}

?>
