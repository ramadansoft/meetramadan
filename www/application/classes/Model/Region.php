<? class Model_Region extends ORM {
	protected $_table_name = 'regions';
	protected $_has_many = array(
		'cities' => array(
            'model' => 'City',
            'foreign_key' => 'regions_id',
        ),
	);
	protected $_belongs_to = array(
		'country' => array(
            'model' => 'Country',
            'foreign_key' => 'countries_id',
        ),
	);
}