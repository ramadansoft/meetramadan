<? class Model_Countries_Country extends ORM {
	protected $_table_name = 'countries_new';
	protected $_has_many = array(
		'cities' => array(
            'model' => 'Countries_City',
            'foreign_key' => 'countries_id',
        ),
	);
}