<? class Model_Countries_City extends ORM {
	protected $_table_name = 'cities_new';
	protected $_belongs_to = array(
		'country' => array(
            'model' => 'Countries_Country',
            'foreign_key' => 'country_id',
        ),
	);
}