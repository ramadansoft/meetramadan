<? class Model_Test extends ORM {
	protected $_has_many = array(
		'test_scores' => array(
            'model' => 'Tests_Scrore',
            'foreign_key' => 'tests_id',
        ),
	);
    // Возвратить массив тестов с количеством наград для конкретного пользователя
    static function getUserTests($user_id,$all_tests = false) {
        if ($all_tests)
        {
            $tests = DB::query(DATABASE::SELECT, '
                SELECT
                    t1.id,t1.name,
                    t1.score + IFNULL((SELECT SUM(t2.add_score) FROM tests_scores as t2
                            INNER JOIN tests_modificators as t3 ON t2.tests_modificators_id = t3.id
                            INNER JOIN tests_modificators_has_users as t4 ON t3.id = t4.tests_modificators_id
                        WHERE t4.users_id = '. $user_id .' AND t2.tests_id = t1.id
                    ),0) as score, t1.desc
                FROM tests as t1
            ')->execute()->as_array();
        } else {
            $tests = DB::query(DATABASE::SELECT, '
                SELECT
                    t1.id,t1.name,
                    t1.score + IFNULL((SELECT SUM(t2.add_score) FROM tests_scores as t2
                            INNER JOIN tests_modificators as t3 ON t2.tests_modificators_id = t3.id
                            INNER JOIN tests_modificators_has_users as t4 ON t3.id = t4.tests_modificators_id
                        WHERE t4.users_id = '. $user_id .' AND t2.tests_id = t1.id
                    ),0) as score, t1.desc


                    FROM tests as t1
                    WHERE (need_modificator = 0)
                    OR t1.id IN (SELECT t2.tests_id FROM tests_scores as t2
                            INNER JOIN tests_modificators as t3 ON t2.tests_modificators_id = t3.id
                            INNER JOIN tests_modificators_has_users as t4 ON t3.id = t4.tests_modificators_id
                        WHERE t4.users_id = '. $user_id .'
                    )
            ')->execute()->as_array();
        }
        return $tests;
    }
    // Проверить, поставил ли уже галочку нужный пользователь на нужном тесте в нужный день
    static function checkCompleted($user_id,$test_id,$year,$day) {
        $result = DB::select('id')->from('scores')
                ->where('tests_id','=',$test_id)
                ->where('users_id','=',$user_id)
                ->where('day','=',$day)
                ->where('year','=',$year)
                ->execute();

        if ($result->count() > 0)
            return TRUE;
        else
            return FALSE;
    }
    // Возвращает общее количество набранных баллов за день
    static function getScore($user_id,$year,$day) {
        return DB::select(array(db::expr('IFNULL(sum(score),0)'),'score'))->from('scores')
                ->where('users_id','=',$user_id)
                ->where('year','=',$year)
                ->where('day','=',$day)
                ->execute()
                ->get('score');
    }
}