<?

class Model_Score extends ORM {

	protected $_belongs_to = array(
		'user' => array(
            'model' => 'User',
            'foreign_key' => 'users_id',
        ),
		'test' => array(
            'model' => 'Test',
            'foreign_key' => 'tests_id',
        ),
	);
}

?>
