<?

class Model_Day extends ORM {

	protected $_has_many = array(
		'tests' => array(
            'model' => 'Test',
            'through' => 'tests_has_days',
            'foreign_key' => 'tests_id',
            'far_key' => 'days_id',
        ),
	);

	protected $_belongs_to = array(
		'user' => array(
            'model' => 'User',
            'foreign_key' => 'users_id',
        ),
	);
}

?>
