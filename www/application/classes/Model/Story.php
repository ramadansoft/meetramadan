<?
class Model_Story extends ORM {
	protected $_table_name = 'stories';
	protected $_belongs_to = array(
		'user' => array(
            'model' => 'User',
            'foreign_key' => 'users_id',
        ),
    );
	protected $_has_many = array(
		'likes' => array(
            'model'         => 'User',
            'through'       => 'stories_likes',
            'foreign_key'   => 'story_id',
            'far_key'       => 'user_id',
        ),
    );
    public function filters() {
        return array(
            'text' => array(
                array('strip_tags'),
            ),
            'title' => array(
                array('strip_tags'),
            ),
        );
    }
}