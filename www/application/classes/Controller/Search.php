<? class Controller_Search extends Template {
    function action_json_city() {
        $cities = ORM::factory('City')
            ->with('country')
            ->where('city','LIKE','%'.Arr::get($_POST,'search').'%')
            ->order_by('name')
            ->find_all();
        $items = [];
        foreach($cities as $city) {
            $items[$city->id] = $city->city . ' (' . $city->region . ', ' . $city->country->name . ')';
        }
        $this->auto_render = FALSE;
        $this->response->body(json_encode([
            'error' => 'no',
            'items' => $items,
        ]));
    }
}