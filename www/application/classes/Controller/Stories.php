<?
class Controller_Stories extends Template {
    function action_index() {
        $this->action_list();
    }
    function action_list() {
        $type = $this->request->param('type');
        
        switch ($type) {
            case 'best':
                $stories = ORM::factory('Story')->order_by('rating','desc');
            break;
            case 'random':
                $stories = ORM::factory('Story')->find_all();
                if ($stories->count() > 0)
                    $stories = ORM::factory('Story')->where('id','=',$stories[rand(0,$stories->count()-1)]->id);
                else
                    $stories = ORM::factory('Story');
            break;
            default:
                $stories = ORM::factory('Story')->order_by('date','desc');
            break;
        }        
        
        $view = View::factory('pages/stories/index');
        $view->stories = $stories->pagination(7);
        $view->pager = $stories->pagination_html(7);
        $view->type = $type;
        
        $this->template->content = $view;
        $this->template->title = 'Клуб анонимных благодеятелей';
        $this->template->desc = 'Расскажи историю своего благого поступка, не объявляя о себе.';
    }
    function action_add() {
        if ($this->isJson()) {
            if (Security::check_token()) {
                if ($this->auth->logged_in()) {
                    $story = ORM::factory('Story');
                    $story->users_id = $this->user->id;
                    $story->title = Arr::get($_POST,'title');
                    $story->text = Arr::get($_POST,'text');
                    $story->date = time();
                    $story->save();

                    $this->SendJSONData([
                        JSONA_REDIRECT => '/stories/show/' . $story->id,
                    ]);
                } else {
                    // Зарегистрировать пользователя и добавить историю
                    $email = Arr::get($_POST,'email');
                    $password = Arr::get($_POST,'password');
                    $result = Controller_Auth::register_user($email, $password);
                    
                    if ($result[0] === JSONA_COMPLETED) {
                        $story = ORM::factory('Story');
                        $story->users_id = $this->auth->get_user()->id;
                        $story->title = Arr::get($_POST,'title');
                        $story->text = Arr::get($_POST,'text');
                        $story->date = time();
                        $story->save();
                        
                        $this->SendJSONData([
                            JSONA_RELOADPAGE => '/stories/show/' . $story->id
                        ]);
                    } else {
                        $this->SendJSONData([
                            $result[0] => $result[1]
                        ]);
                    }
                }
            } else {
                $this->SendJSONData([
                    JSONA_ERROR => 'Ошибка ключа безопасности. Обновите страницу и повторите попытку.'
                ]);
            }
        } else {
            $view = View::factory('pages/stories/add');

            $this->template->content = $view;
            $this->template->title = 'Добавить историю';
        }
    }
    function action_show() {
        $story = ORM::factory('Story',$this->request->param('id'));
        if ($story->loaded()) {
            $view = View::factory('pages/stories/show');
            $view->story = $story;
            $this->template->content = $view;
            $this->template->title = $story->title;
            $this->template->desc = Text::limit_chars($story->text,150,'...');
        } else {
            throw new HTTP_Exception_404;
        }
    }
    function action_delete() {
        $story = ORM::factory('Story',$this->request->param('id'));
        if ($story->loaded() && ($story->users_id === $this->user->id || $this->user->has('roles',ROLE_ADMIN))) {
            if ($this->isJson()) {
                if (Security::check_token()) {
                    $story->delete();
                    $this->SendJSONData([
                        JSONA_REDIRECT => '/stories'
                    ]);
                } else {
                    $this->SendJSONData([
                        JSONA_ERROR => 'Ошибка ключа безопасности. Повторите попытку.'
                    ]);
                }                
            } else {
                $view = View::factory('pages/stories/delete');
                $view->story = $story;
                $this->template->content = $view;
                $this->template->title = 'Удалить историю №'.$story->id;
            }
        } else {
            throw new HTTP_Exception_404;
        }
    }
    function action_edit() {
        $story = ORM::factory('Story',$this->request->param('id'));
        if ($story->loaded() && ($story->users_id === $this->user->id || $this->user->has('roles',ROLE_ADMIN))) {
            if ($this->isJson()) {
                if (Security::check_token()) {
                    $story->title = Arr::get($_POST,'title');
                    $story->text = Arr::get($_POST,'text');
                    $story->save();
                    $this->SendJSONData([
                        JSONA_REDIRECT => '/stories/show/' . $story->id
                    ]);
                } else {
                    $this->SendJSONData([
                        JSONA_ERROR => 'Ошибка ключа безопасности. Повторите попытку.'
                    ]);
                }                
            } else {
                $view = View::factory('pages/stories/edit');
                $view->story = $story;
                $this->template->content = $view;
                $this->template->title = 'Редактирование истории №'.$story->id;
            }
        } else {
            throw new HTTP_Exception_404;
        }
    }
    function action_like() {
        $story = ORM::factory('Story',$this->request->param('id'));
        if ($story->loaded()) {
            if ($this->auth->logged_in()) {
                if ($story->users_id != $this->user->id) {
                    if ($this->user->has('likes',$story->id)) {
                        $this->user->remove('likes',$story->id);
                        $story->rating = $story->rating - 1;
                    } else {
                        $this->user->add('likes',$story->id);
                        $story->rating = $story->rating + 1;
                    }
                    $story->save();
                    $this->SendJSONData([
                        JSONA_UPDATE_STORY_INFO => [
                            $story->id,
                            View::factory('pages/stories/infobar',['story'=>$story])->render()
                        ]
                    ]);
                } else {
                    $this->SendJSONData([
                        JSONA_ERROR => 'Вы не можете оценить свою собственную историю.',
                        JSONA_UPDATE_STORY_INFO => [
                            $story->id,
                            View::factory('pages/stories/infobar',['story'=>$story])->render()
                        ]
                    ]);
                }
            } else {
                $this->SendJSONData([
                    JSONA_ERROR => 'Только зарегистрированные пользователи имеют право оценивать истории.',
                    JSONA_SHOW_LOGIN_WINDOW => '',
                ]);
            }
        } else {
            $this->SendJSONData([
                JSONA_ERROR => 'Указанная вами история не найдена. Возможно, она была внезапно удалена.'
            ]);
        }
    }
}