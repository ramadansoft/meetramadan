<?

class Controller_Today extends Template {

    function action_index() {
        if ($this->mode() === RAMADAN_ENDED) {
            $this->action_results();
        } elseif($this->mode() === RAMADAN_STARTED) {
            $this->action_show();
        } elseif($this->mode() === RAMADAN_NEAR) {
            $this->redirect('/ramadannear');
        }
    }

    function action_results()
    {
        $users_id = $this->request->param('id');

        if (!empty($users_id))
            $user = ORM::factory('User',$users_id);
        else
            $user = $this->user;

        if (!$this->auth->logged_in())
            throw new HTTP_Exception_404;

        $view = View::factory('pages/today/result');
        $view->suser = $user;
        $view->totalscore = $user->getTotalScore();
        $view->usersscore = Model_User::getAverageUsersScore();

        $this->template->content = $view;
        $this->template->title = 'Итоги ' . $user->username . ', ' . $user->city->city;
    }

    function action_show()
    {
        $this->checkRegister();

        $yesterday = ($this->request->param('id') == 'yesterday') ? TRUE : FALSE;

        $day = $this->day() - (($yesterday) ? 1 : 0);

        $tests = Model_Test::getUserTests($this->user->id, ($this->user->has('roles',ROLE_ADMIN) ? TRUE : FALSE));

        foreach($tests as &$test)
            $test['completed'] = Model_Test::checkCompleted($this->user->id,$test['id'],date('Y'),$day);

        $view = View::factory('pages/today/index');
        $view->tests = $tests;
        $view->day = $day;

        if ($yesterday)
        {
            $view->headtitle1 = 'Вчера';
            $view->headtitle2 = 'Сегодня';
        } else {
            $view->headtitle1 = 'Сегодня';
            $view->headtitle2 = 'Вчера';
        }

        $this->template->content = $view;
        $this->template->title = ($yesterday) ? 'Вчера' : 'Сегодня';
        $this->template->title = $this->template->title . '. День ' . $day . ' из 30';
    }
    
    // Пользователь завершил тест
    function action_completed()
    {
        $this->checkRegister();

        $test = ORM::factory('Test',$this->request->param('id'));

        if (!$test->loaded())
            throw new HTTP_Exception_404;

        $day = Arr::get($_POST,'day');

        // Если человек пытается поставить метку вне рамадана - отменим его желание
        if ($day < 1 || $day > 30 || $day > date('z',time()))
        {
            $this->SendJSONData(array(
                JSONA_WARNING => 'Вы не можете поставить метку вне времени Рамадана',
                JSONA_REFRESHPAGE => ''
            ));
            return;
        }

        // Вдруг не первый раз каким-то ообразом нажимает эту кнопку..
        DB::delete('scores')
                ->where('tests_id','=',$test->id)
                ->where('users_id','=',$this->user->id)
                ->where('day','=',$day)
                ->where('year','=',date('Y'))
                ->execute();

        $score = ORM::factory('Score');
        $score->tests_id = $test->id;
        $score->users_id = $this->user->id;
        $score->day = Arr::get($_POST,'day');
        $score->year = date('Y');
        $score->score = Arr::get($_POST,'score');
        $score->save();

        $this->SendJSONData(array(
            JSONA_NOTHING => '',
        ));
    }

    // Пользователь изменил состояние завершения определенного теста в определенный день
    function action_toggle() {
        $this->checkRegister();

        $test_id = $this->request->param('id');
        $day = Arr::get($_POST, 'day');

        $test = ORM::factory('Test', $test_id);

        if (!$test->loaded())
          throw new HTTP_Exception_404;

        $check = DB::select()->from('scores')
                ->where('day', '=', $day)
                ->where('year','=',date('Y'))
                ->where('users_id', '=', $this->user->id)
                ->where('tests_id', '=', $test_id)->execute()->as_array();

        if ($this->mode() === RAMADAN_STARTED)
        {
            if ($check) {
                $result = DB::delete('scores')
                    ->where('day', '=', $day)
                    ->where('year','=',date('Y'))
                    ->where('users_id', '=', $this->user->id)
                    ->where('tests_id', '=', $test_id)
                    ->execute();
                
                $result = array(
                    'code' => ($result) ? 1 : 0
                );
            } else {
                $result = DB::insert('scores')
                    ->columns(array('day', 'year', 'tests_id', 'users_id', 'score',))
                    ->values(array($day, date('Y'), $test_id, $this->user->id, $test->score,))
                    ->execute();
                $result = array('code' => ($result) ? 2 : 0);
            }
        } else {
            $result = 0;
        }

        $this->auto_render = false;
        $this->response->body(json_encode($result));
    }

    function action_updatecoords()
    {
        $this->checkAdminLogin();

        $tests = (array)Arr::get($_POST,'tests_id');

        foreach($tests as $key => $test) {
            $test = ORM::factory('Test',$test);

            if ($test->loaded())
            {
                $test->x = $_POST['tests_x'][$key];
                $test->y = $_POST['tests_y'][$key];
                $test->save();
            }
        }

        $this->SendJSONData(array(
            JSONA_COMPLETED => 'Координаты тестов обновлены'
        ));
    }

}

?>
