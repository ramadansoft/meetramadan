<?php

class Controller_Auth extends Template
{

	function action_index() {

		$view = View::factory('pages/auth/login')
				->bind('ulogin',$ulogin);

		$ulogin = Ulogin::factory();
        if (!$ulogin->mode()){
        	;
        }
          //  $this->template->ulogin = ;
        else
        {
            try
            {
                $ulogin->login();

            }
            catch(ORM_Validation_Exception $e)
            {
                $this->template->errors = $e->errors('');
            }
        }


		$this->template->content = $view;


	}

	function action_login() {
        $login = Arr::get($_POST, 'login');
        $pass = Arr::get($_POST, 'password');
        if (!empty($login) && !empty($pass)) {
            $success = Auth::instance()->login($login,$pass,true);
            if ($success) {
                $user = ORM::factory('User',['email'=>$login]);
                if ($user->completed_register) {
                    $this->redirect('/auth/completeregister');
                } else {
                    $this->redirect('/today');
                }
            } else {
                $this->redirect('/auth?loginfail');
            }
            
            // Изредка почистим устаревшие токены пользователей
            $num = rand(0,100);
            if ($num < 5) {
                DB::delete('user_tokens')->where('expires','<',time());
            }
        } else {
            $view = View::factory('pages/auth/login');
            $this->template->content = $view;
            $this->template->title = 'Авторизация';
        }
	}

	function action_logout() {
		$this->auth->logout();
		$this->redirect('/');
	}

    function action_resetregister() {
        $this->checkLogin();

        $this->user->completed_register = FALSE;
        $this->user->save();

        $this->SendJSONData(array(
            JSONA_RELOADPAGE => '/auth/completeregister',
        ));
    }

    function action_completeregister() {
        if (!$this->auth->logged_in() || $this->user->completed_register)
            $this->redirect('/');

        $fio = Arr::get($_POST,'fio');
        $city = Arr::get($_POST,'city_id');

        if (!empty($fio) && !empty($city)) {
            $this->user->username = $fio;
            $this->user->cities_id = $city;
            $this->user->completed_register = TRUE;
            $this->user->save();

            $mods = (array)Arr::get($_POST,'mods');

            // Вдруг человек проходит это второй раз. Сотрем на всякий случай все связи модификаторов.
            DB::delete('tests_modificators_has_users')->where('users_id','=',$this->user->id)->execute();

            foreach($mods as $mod)
                $this->user->add('modificators',$mod);

            $this->SendJSONData(array(
                JSONA_RELOADPAGE => '/',
            ));
        } else {
            $view = View::factory('pages/auth/complete_registration');
            $view->modificators = ORM::factory('Tests_Modificator')->find_all();

            $this->template->content = $view;
            $this->template->title = 'Завершение регистрации';
        }
    }

    function action_register() {
        if ($this->isJson()) {
            if (Security::check_token()) {
                $this->auto_render = false;
                $email = Arr::get($_POST,'email');
                $password = Arr::get($_POST,'password');
                $result = $this->register_user($email, $password);

                if ($result[0] === JSONA_ERROR) {
                    $this->SendJSONData([
                        JSONA_ERROR => $result[1]
                    ]);
                } elseif($result[0] === JSONA_COMPLETED) {
                    $this->SendJSONData([
                        //JSONA_COMPLETED => $result[1],
                        JSONA_RELOADPAGE => '/auth/completeregister',
                    ]);
                } else {
                    $this->SendJSONData([
                        $result[0] => $result[1],
                    ]);
                }
            } else {
                $this->SendJSONData([
                    JSONA_ERROR => 'Ошибка ключа безопасности. обновите страницу и повторите попытку.'
                ]);
            }
        } else {
            $view = View::factory('pages/auth/register');
            $this->template->content = $view;
            $this->template->title = 'Регистрация';
        }
    }
    
    // Зарегистрировать пользователя
    static function register_user($email,$password) {
        if (empty($email) || !Valid::email($email) || empty($password)) {
            return [JSONA_ERROR,'Пожалуйста, введите корректные E-mail и пароль'];
        }

        $user = ORM::factory('User',array('email' => $email));
        if ($user->loaded()) {
            if ($this->remindPassword($email)) {
                return [JSONA_NOTICE,'Вы уже зарегистрированы в системе. Новый пароль отправлен на ваш e-mail.'];
            } else {
                return [JSONA_ERROR,'В ходе регистрации произошла ошибка, попробуйте еще раз позднее.'];
            }
        }

        $user = ORM::factory('User');
        $user->password = $password;
        $user->email = $email;
        $user->save();
        $user->add('roles',ROLE_LOGIN);

        // Сразу же залогиним пользователя
        Auth::instance()->force_login($email);

        // Письмо опасной личности
        $Mailer = new Mailer();
        $Mailer->IsHTML(true);
        $Mailer->IsMail();
        $Mailer->AddAddress($email, "");
        $Mailer->FromName = 'team@meetramadan.ru';
        $Mailer->Subject  = 'Регистрация проекте "Встреть Рамадан!"';
        $Mailer->Body  = 'Ас-саляму алейкум уа рахматуллахи уа баракятух!<br/><br/>Спасибо за вашу регистрацию!<br/><br/> Ваш пароль для входа: '.$password.'.<br/><br/>По вопросам и предложениям можете писать <a tagret="_blank" href="http://vk.com/ahmedjahn">Ахмадуллину Роману</a> и <a tagret="_blank" href="http://vk.com/artkhatuyev">Артуру Хатуеву</a>';
        $Mailer->Send();
        
        return [JSONA_COMPLETED,'Пользователь успешно зарегистрирован'];
    }

    function action_preregister() {
        $this->auto_render = false;

        $email = Arr::get($_POST,'email');

        if (!empty($email))
        {

            if (!Valid::email($email))
            {
                $res = array(
                    'head' => 'Это точно ваш email?',
                    'text' => 'Извините, но введенные вами данные ("'.$email.'") не могут быть распознаны как E-Mail.'
                );

                $this->response->body(json_encode($res));

                return;
            }

            $user = ORM::factory('User',array('email' => $email));

            if ($user->loaded())
            {
                $res = array(
                    'head' => 'Вы уже с нами',
                    'text' => 'Ваш E-mail уже зарегистрирован в нашей базе.'
                );

                $this->response->body(json_encode($res));

            } else {

                $password = Text::random('alnum', 12);

                $user->password = $password;
                $user->email = $email;
                $user->save();
                $user->add('roles',ROLE_LOGIN);

                $page = ORM::factory('Page',(array('lat_name' => 'password')));
                $text = str_replace('$password', $password, $page->text);
                $text = str_replace('$email', $email, $text);

                $Mailer = new Mailer();
                $Mailer->IsHTML(true);
                $Mailer->IsMail();
                $Mailer->AddAddress($email, "");
                $Mailer->FromName ='robot@meetramadan.ru';
                $Mailer->Subject  = 'Регистрация в проекте "Встреть Рамадан!"';
                $Mailer->Body  = $text;
                $Mailer->Send();

                $res = array(
                    'head' => 'Спасибо!',
                    'text' => 'Мы выслали к вам на почту ваш логин и пароль..'
                );
            }

            $this->response->body(json_encode($res));
        }
    }

    function action_remindPassword() {
        if ($this->remindPassword(Arr::get($_POST,'email')))
        {
            $this->SendJSONData(array(
                JSONA_REDIRECT => '/auth/login?passwordrestored',
            ));
        } else {
            $this->SendJSONData(array(
                JSONA_ERROR => 'Извините, но такой E-Mail не найден в системе'
            ));
        }
    }

    protected function remindPassword($email)
    {
        $user = ORM::factory('User',array('email' => $email));

        if (!$user->loaded())
            return FALSE;

        $new_password = Text::random('alnum', 8);

        $user->password = $new_password;
        $user->save();

        // Письмо опасной личности
        $Mailer = new Mailer();
        $Mailer->IsHTML(true);
        //$Mailer->IsMail();
        $Mailer->AddAddress($email, "");
        $Mailer->From = 'meetramadan.ru';
        $Mailer->FromName = 'MeetRamadan.ru';
        $Mailer->Subject  = 'Встреть Рамадан! - Напоминание пароля';
        $Mailer->Body  = 'Ас-саляму алейкум уа рахматуллахи уа баракятух!<br/><br/>Ваш новый пароль для входа: ' . $new_password;
        $Mailer->Send();

        return TRUE;

    }


}

?>
