<?

class Controller_Modificators extends Template {

    function before()
    {
        parent::before();

        $this->checkAdminLogin();
    }

	public function action_add()
	{
        if (Security::check_token())
        {
            $modificator = ORM::factory('Tests_Modificator');
            $modificator->name = Arr::get($_POST,'name');
            $modificator->save();

            $tests_ids = (array)Arr::get($_POST,'tests_ids');

            if (!empty($tests_ids))
            {
                foreach($tests_ids as $key => $test_id)
                {
                    $score = ORM::factory('Tests_Score');
                    $score->tests_modificators_id = $modificator->id;
                    $score->tests_id = $test_id;
                    $score->add_score = $_POST['tests_scores'][$key];
                    $score->save();
                }
            }

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Модификатор создан',
                JSONA_REDIRECT => '/modificators',
            ));
        } else {
            $view = View::factory('pages/modificators/add');
            $view->tests = Arr::Make2Array(ORM::factory('Test')->find_all(),'id','name');

            $this->template->content = $view;
            $this->template->title = 'Создать новый модификатор';
        }

	}

	public function action_edit()
	{
        $modificator = ORM::factory('Tests_Modificator',$this->request->param('id'));

        if (!$modificator->loaded())
            throw new HTTP_Exception_404;

        if (Security::check_token())
        {
            $modificator->name = Arr::get($_POST,'name');
            $modificator->save();

            $tests_ids = (array)Arr::get($_POST,'tests_ids');

            DB::delete('tests_scores')->where('tests_modificators_id','=',$modificator->id)->execute();
            if (!empty($tests_ids))
            {
                foreach($tests_ids as $key => $test_id)
                {
                    $score = ORM::factory('Tests_Score');
                    $score->tests_modificators_id = $modificator->id;
                    $score->tests_id = $test_id;
                    $score->add_score = $_POST['tests_scores'][$key];
                    $score->save();
                }
            }

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Модификатор отредактирован',
                JSONA_REDIRECT => '/modificators',
            ));
        } else {
            $view = View::factory('pages/modificators/edit');
            $view->mod = $modificator;
            $view->scores = $modificator->scores->find_all();
            $view->tests = Arr::Make2Array(ORM::factory('Test')->find_all(),'id','name');

            $this->template->content = $view;
            $this->template->title = 'Редактирование модификатора';
        }

	}

    function action_index()
    {
        $view = View::factory('pages/modificators/list');
        $view->modificators = ORM::factory('Tests_Modificator')->find_all();

        $this->template->content = $view;
        $this->template->title = 'Модификаторы задач';
    }


}

?>
