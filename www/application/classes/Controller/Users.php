<?

class Controller_Users extends Main {


	function before()
	{
		parent::before();
		$this->checkAdminLogin();
	}

    protected $_model_name = 'user';
    protected $_add = array(
        'fio' => array(
            'tag' => 'input',
            'label' => 'ФИО',
            'attributes' => array(),
        ),
        'username' => array(
            'tag' => 'input',
            'label' => 'Логин',
            'attributes' => array(),
        ),
        'email' => array(
            'tag' => 'input',
            'label' => 'E-Mail',
            'attributes' => array('data-validation' => 'isemail'),
        ),
        'password' => array(
            'tag' => 'input',
            'label' => 'Пароль',
            'attributes' => array('data-validation' => 'minlength:6'),
        ),
    );

    protected $_edit = array(
        'fio' => array(
            'tag' => 'input',
            'label' => 'ФИО',
            'attributes' => array(),
        ),
        'username' => array(
            'tag' => 'input',
            'label' => 'Логин',
            'attributes' => array(),
        ),
        'email' => array(
            'tag' => 'input',
            'label' => 'E-Mail',
            'attributes' => array('data-validation' => 'isemail'),
        ),
    );

    protected $_messages = array(
        'add' => array(
            'success' => 'Пользователь создан',
            'error' => 'Ошибка создания пользователя',
        ),
        'edit' => array(
            'success' => 'Пользователь отредактрован',
            'error' => 'Ошибка редактрования пользователя',
        ),
        'delete' => array(
            'success' => 'Пользователь удален',
            'error' => 'Ошибка удаления пользователя',
        ),
    );

    /**
     * Возвратить массив строк контроллера
     */
    protected function get_params($model) {
        return array(
            'add' => array(
                'caption' => 'Создание нового пользователя',
                'legend' => '',
                'button' => 'Создать',
            ),
            'edit' => array(
                'caption' => 'Редактирование пользователя "' . (string)$model . '"',
                'legend' => '',
                'button' => 'Применить изменения',
            ),
            'delete' => array(
                'caption' => 'Удаление пользователя "' . (string)$model . '"',
                'answer' => 'Вы в курсе, что вы удаляете пользователя "' . (string)$model . '"?',
                'button' => 'Удалить пользователя',
            )
        );
    }

	function action_add()
	{
		if (!empty($_POST['username']))
		{
			$user = $this->add();
    		if ($user !== FALSE)
			{
				$user->add('roles',ROLE_LOGIN);
			}
		} else {
			$view = View::factory('pages/users/add');
			$view->form = $this->form_add();
			$this->template->content = $view;
		}
	}

	function action_edit()
	{
		if (!empty($_POST['username']))
		{
			$this->edit($this->request->param('id'));
		} else {
			$view = View::factory('pages/users/edit');
			$view->form = $this->form_edit($this->request->param('id'));
			$this->template->content = $view;
		}
	}

	function action_index()
	{
		$view = View::factory('pages/users/list');
		$view->users = ORM::factory('User')->where('is_deleted','=','0')->find_all();

		$this->template->content = $view;
	}

	function action_toggle()
	{
		$user = ORM::factory('User',array('id' => $this->request->param('id'), 'is_deleted' => '0'));

		if ($user->loaded() && $this->user->has('roles',ROLE_ADMIN) && Security::check_token())
		{
			if ($user->has('roles',ROLE_ADMIN))
			{
				if ($user->id !== $this->user->id)
				{
					if ($user->username == 'saggid')
					{
						$this->SendJSONData(array(
							JSONA_COMPLETED => 'Брат, давай не прикалывайся :)',
							JSONA_REFRESHPAGE => '',
						));
					} else {
						$user->remove('roles',ROLE_ADMIN);

						$this->SendJSONData(array(
							JSONA_COMPLETED => (string)$user . ' стал простым смертным',
							JSONA_REFRESHPAGE => '',
						));
					}
				} else {
					$this->SendJSONData(array(
						JSONA_ERROR => 'Зачем самому себе админа убирать?)',
					));
				}
			} else {
				$user->add('roles',ROLE_ADMIN);

				$this->SendJSONData(array(
					JSONA_COMPLETED => (string)$user . ' стал админом',
					JSONA_REFRESHPAGE => '',
				));
			}
		} else {
			$this->SendJSONData(array(
				JSONA_ERROR => 'Недостаточно прав',
			));
		}
	}

	function action_delete()
	{
		$id = $this->request->param('id');
		$user = ORM::factory('User',array('id' => $id, 'is_deleted' => '0'));

		if (!isset($_REQUEST[TOKEN]))
		{
			$this->template->content = $this->form_delete($id);
		} else {
			if ($user->loaded() && $this->user->has('roles',ROLE_ADMIN) && Security::check_token())
			{
				if ($user->id !== $this->user->id)
				{
					if ($user->username == 'saggid')
					{
						$this->SendJSONData(array(
							JSONA_COMPLETED => 'И кто же тогда будет сайт делать? :)',
							JSONA_REFRESHPAGE => '',
						));
					} else {
						$user->is_deleted = TRUE;
						$user->save();

						$this->SendJSONData(array(
							JSONA_COMPLETED => $user . ' удален из системы',
							JSONA_REFRESHPAGE => '',
						));
					}
				} else {
					$this->SendJSONData(array(
						JSONA_ERROR => 'Удалять самого себя?',
					));
				}
			} else {
				$this->SendJSONData(array(
					JSONA_ERROR => 'Недостаточно прав',
				));
			}
		}
	}

    function action_password()
    {
        $user = ORM::factory('User',array('id' => $this->request->param('id'), 'is_deleted' => '0'));

        if ($user->loaded())
        {
            if (Security::check_token())
            {
                $pass = Arr::get($_POST,'pass');
                $retpass = Arr::get($_POST,'retpass');

                if ($pass === $retpass)
                {
                    $user->password = $pass;
                    $user->save();

                    $this->SendJSONData(array(
                        JSONA_COMPLETED => 'Пароль успешно изменен',
                        JSONA_REFRESHPAGE => ''
                    ));
                } else {
                    $this->SendJSONData(array(
                        JSONA_ERROR => 'Введенные пароли не совпадают'
                    ));
                }

            } else {
                $view = View::factory('pages/users/password');
                $view->user = $user;

                $this->template->content = $view;
                $this->template->title = 'Изменени пароля пользователя ' . $user;
            }
        } else {
            throw new HTTP_Exception_404;
        }

    }

}

?>
