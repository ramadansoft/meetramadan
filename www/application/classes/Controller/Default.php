<?
class Controller_Default extends Template {
	function action_index() {
        if ($this->auth->logged_in() && !$this->user->completed_register) {
            $this->redirect('/auth/completeregister');
        } elseif($this->auth->logged_in()) {
            $this->redirect('/today');
        } else {
            $page = ORM::factory('Page',array('lat_name' => 'about_project'));
            $view = View::factory('pages/page/show');
            $view->page = $page;
            $this->template->content = $view;
            $this->template->title = $page->name;
            $this->template->desc = $page->text;
        }
	}
}