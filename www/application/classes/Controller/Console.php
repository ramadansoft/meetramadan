<?

class Controller_Console extends Template {

    private function log($text) {
		if (is_array($text)) {
			foreach ($text as $line) {
				$this->write($line);
			}
		} else {
			fwrite(STDOUT, $text.PHP_EOL);
		}
    }

    public function before() {
        if (isset($_SERVER['SERVER_NAME']))
            die('Run this method only from console');

        parent::before();
        $this->auto_render = false;

        ini_set('memory_limit', -1); // Ставим лимит оперативки в нуль
        gc_enable(); // Включаем сборщик мусора

        $this->log('Консольная утилита Kohana запущена');
    }

    public function action_pagedelivery()
    {

        $page = ORM::factory('Page',(array('lat_name' => $this->request->param('id'))));

        if (!$page->loaded())
        {
            $this->log('Указанная страница ' . $this->request->param('id') . ' не найдена.');
            return;
        }

        $this->log('Запускаем рассылку');

        set_time_limit(0);

        $emails = ORM::factory('User')->where('completed_register','=',1)->find_all();

        foreach($emails as $email)
        {
            $Mailer = new Mailer();
            $Mailer->IsHTML(true);
            $Mailer->IsMail();
            $Mailer->AddAddress($email->email, "");
            $Mailer->FromName ='robot@meetramadan.ru';
            $Mailer->Subject  = $page->name;
            $Mailer->Body  = $page->text;
            $Mailer->Send();

            $this->log('Послали письмо на ' . $email->email);

            sleep(1);
        }

        $this->template->content = 'Письма разосланы';
    }

    public function action_passwordsdelivery()
    {

        $page = ORM::factory('Page',(array('lat_name' => 'password')));

        if (!$page->loaded())
        {
            $this->log('Указанная страница ' . $this->request->param('id') . ' не найдена.');
            return;
        }

        $this->log('Запускаем рассылку');

        set_time_limit(0);

        $emails = ORM::factory('Preregister')->find_all();

        foreach($emails as $email)
        {
            $password = Text::random('alnum', 12);

            $this->log('Обработка: ' . $email->email);

            $user = ORM::factory('User',array('email' => $email->email));

            if (!$user->loaded())
            {
                $user->password = $password;
                $user->email = $email->email;
                $user->save();
                $user->add('roles',ROLE_LOGIN);

                $text = str_replace('$password', $password, $page->text);
                $text = str_replace('$email', $email, $text);

                $Mailer = new Mailer();
                $Mailer->IsHTML(true);
                $Mailer->IsMail();
                $Mailer->AddAddress($email->email, "");
                $Mailer->FromName ='robot@meetramadan.ru';
                $Mailer->Subject  = 'Регистрация в проекте "Встреть Рамадан!"';
                $Mailer->Body  = $text;
                $Mailer->Send();

                $email->delete();

                sleep(1);
            }
        }

        $this->template->content = 'Письма разосланы';


    }
    
    function action_checkcities() {
        $users = ORM::factory('User')
            ->with('city:region:country')
            ->where('cities_id','IS NOT',NULL)
            ->find_all();
        $morecities = [];
        $this->log('Надо обработать пользователей: ' . $users->count());
        foreach($users as $user) {
            $newcities = ORM::factory('Countries_City')
                ->where('city','=',$user->city->name)
                ->join('countries_new')->on('countries_city.country_id','=','countries_new.id')
                ->where('countries_new.name','=',$user->city->region->country->name)
                ->find_all();

            if ($newcities->count() == 1) {
                $user->cities_id = $newcities[0]->id;
                $user->save();
            } elseif ($newcities->count() > 1) {
                $user->cities_id = null;
                $user->save();
                if (!in_array($newcities[0]->id,$morecities)) {
                    array_push($morecities,$newcities[0]->id);
                    $this->log("В новой базе есть больше одного города под названием {$user->city->name} в стране {$user->city->region->country->name}");
                }
            } elseif ($newcities->count() == 0) {
                $user->cities_id = null;
                $user->save();
                $this->log("В новой базе нет ни одного города под названием {$user->city->name} ({$user->cities_id})");
            }
        }
    }

    function action_makeregions() {
        $cities = ORM::factory('Countries_City')->find_all();
        foreach($cities as $city) {
            //$region_name = $city->
        }
    }
    
    // Исправляем координаты в городах
    function action_citiescoords() {
        $this->log('Исправляем координаты городов..');
        $cities = ORM::factory('City')->where('lat','IS',NULL)->find_all();
        foreach ($cities as $key => $city) {
            $location_name = $city->country->name;
            if ($city->region) $location_name .= ", {$city->region}";
            $location_name .= ", {$city->city}";
            $coords = YaMap::getPoint($location_name);
            if (!empty($coords)) {
                $percent = round(($key / $cities->count()) * 100);
                $this->log("[{$percent}/100%] {$location_name}: {$coords->longitude},{$coords->latitude}");
                $city->long = $coords->longitude;
                $city->lat = $coords->latitude;
                $city->save();
            }
        }
    }

    public function after() {
        $this->log('Работа скрипта завершена.');
        echo "\n";
    }

}
