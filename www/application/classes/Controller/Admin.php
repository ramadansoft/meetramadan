    <?

class Controller_Admin extends Template {

	public function before() {
		parent::before();

		$this->checkAdminLogin();
	}

	public function action_index()
	{
		$this->template->content = View::factory('pages/admin/index');
		$this->template->title = 'Панель администрирования системы';
	}

    public function action_recompress()
    {
        $this->template->content = 'Была произведена рекомпрессия скриптов и стилей в один файл.';
    }

    public function action_sendemails()
    {
        set_time_limit(0);

        $emails = ORM::factory('Preregister')->find_all();

        foreach($emails as $email)
        {
            $Mailer = new Mailer();
            $Mailer->IsHTML(true);
            $Mailer->IsMail();
            $Mailer->AddAddress($email->email, "");
            $Mailer->FromName ='team@meetramadan.ru';
            $Mailer->Subject  = 'Ваша заявка в проект "Встреть Рамадан!" принята';
            $Mailer->Body  = 'Ас-саляму алейкум уа рахматуллахи уа баракятух!<br/><br/>Спасибо за предварительную заявку! Как только проект начнет свою работу, мы оповестим вас.<br/><br/>По вопросам и предложениям можете писать <a tagret="_blank" href="http://vk.com/ahmedjahn">Ахмадуллину Роману</a> и <a tagret="_blank" href="http://vk.com/artkhatuyev">Артуру Хатуеву</a>';
            $Mailer->Send();

            sleep(1);
        }

        $this->template->content = 'Письма разосланы';
    }

    // Зарегистрированные пользователи
    public function action_registered()
    {
        $users = ORM::factory('User')->join('cities')->on('user.cities_id','=','cities.id')->where('completed_register','=',1);

        $view = View::factory('pages/admin/registered');
        $view->users = $users;
        $view->logins = DB::select(db::expr('count(*)'))->from('users')->where('last_login','>',time()-(3600*24*1))->execute()->get('count(*)');

        $this->template->content = $view;
        $this->template->title = 'Зарегистрированные пользователи';

    }

}

?>
