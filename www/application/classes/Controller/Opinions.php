<?

class Controller_Opinions extends Template {

    function action_create()
    {
        $this->checkLogin();

        if (isset($_POST['text']))
        {
            if (Security::check_token())
            {
                $latname = Text::RusToLat(Arr::get($_POST,'name'));

                $finded = ORM::factory('Opinion',array('lat_name' => $latname));
                if ($finded->loaded())
                    $latname .= '-' . date('d-m-Y', $date);

                $opinion = ORM::factory('Opinion');
                $opinion->name = Arr::get($_POST,'name');
                $opinion->text = Arr::get($_POST,'text');
                $opinion->lat_name = $latname;
                $opinion->users_id = $this->user->id;
                $opinion->date = Arr::get($_POST,'unix-date');
                $opinion->keywords = Arr::get($_POST,'keywords');
                $opinion->avatar_link = Arr::get($_POST,'avatar_link');
                $opinion->age = Arr::get($_POST,'age');
                $opinion->vk = Arr::get($_POST,'vk');
                $opinion->show = isset($_POST['show']) ? TRUE : FALSE;
                $opinion->save();

                $this->SendJSONData(array(
                    JSONA_REDIRECT => '/opinion/' . $opinion->lat_name,
                    JSONA_COMPLETED => 'Отзыв успешно создан. Пока что его видят только админы.',
                ));
            } else {
                $this->SendJSONData(array(
                    JSONA_ERROR => 'Неверный ключ безопасности',
                ));
            }
        } else {
            $this->template->content = View::factory('pages/opinions/create');
            $this->template->title = 'Новый отзыв';
        }

    }

    function action_index()
    {
        $opinion = ORM::factory('Opinion',array('lat_name' => $this->request->param('name')));

        if ($opinion->loaded())
        {
            $view = View::factory('pages/opinions/show');
            $view->opinion = $opinion;

            $this->template->content = $view;
            $this->template->title = $opinion->name;
            $this->template->desc = $opinion->text;
            $this->template->keywords = $opinion->keywords;
        } else {
            $opinions = ORM::factory('Opinion');

            if (!$this->auth->logged_in())
                $opinions->where('show','=','1');
                
            $opinions->order_by('date','desc');

            $view = View::factory('pages/opinions/list');
            $view->opinions = $opinions->pagination();
            $view->pager = $opinions->generate_pagination();

            $this->template->content = $view;
            $this->template->title = 'Отзывы';
        }
    }

    function action_edit()
    {
        $this->checkLogin();

        $opinion = ORM::factory('Opinion',$this->request->param('id'));

        if (!$opinion->loaded())
            throw new HTTP_Exception_404;

        if (isset($_POST['text']))
        {
            if (Security::check_token())
            {
                $latname = Text::RusToLat(Arr::get($_POST,'name'));

                $finded = ORM::factory('Opinion')->where('lat_name','=',$latname)->and_where('id','<>',$opinion->id);
                if ($finded->loaded())
                    $latname .= '-' . date('d-m-Y', $date);

                $opinion->name = Arr::get($_POST,'name');
                $opinion->text = Arr::get($_POST,'text');
                $opinion->lat_name = $latname;
                $opinion->users_id = $this->user->id;
                $opinion->date = Arr::get($_POST,'unix-date');
                $opinion->keywords = Arr::get($_POST,'keywords');
                $opinion->avatar_link = Arr::get($_POST,'avatar_link');
                $opinion->age = Arr::get($_POST,'age');
                $opinion->vk = Arr::get($_POST,'vk');
                $opinion->show = isset($_POST['show']) ? TRUE : FALSE;
                $opinion->save();

                $this->SendJSONData(array(
                    JSONA_REDIRECT => '/opinion/' . $opinion->lat_name,
                    JSONA_COMPLETED => 'Отзыв отредактирован.',
                ));
            } else {
                $this->SendJSONData(array(
                    JSONA_ERROR => 'Неверный ключ безопасности',
                ));
            }
        } else {
            $view = View::factory('pages/opinions/edit');
            $view->opinion = $opinion;

            $this->template->content = $view;
            $this->template->title = 'Редактирование отзыва';
        }
    }

    function action_toggleshow()
    {
        $this->checkLogin();

        $opinion = ORM::factory('Opinion',$this->request->param('id'));

        if (!$opinion->loaded() || !Security::check_token())
            throw new HTTP_Exception_404;

        if ($opinion->show)
            $opinion->show = FALSE;
        else
            $opinion->show = TRUE;

        $opinion->save();

        $this->SendJSONData(array(
            JSONA_COMPLETED => 'Положение отзыва изменено',
            JSONA_REFRESHPAGE => ''
        ));

    }

    function action_delete()
    {
        $this->checkLogin();

        $opinion = ORM::factory('Opinion',$this->request->param('id'));

        if (!$opinion->loaded() || !Security::check_token())
            throw new HTTP_Exception_404;

        $opinion->delete();

        $this->SendJSONData(array(
            JSONA_COMPLETED => 'Отзыв удален',
            JSONA_REDIRECT => '/opinions'
        ));

    }

}

?>
