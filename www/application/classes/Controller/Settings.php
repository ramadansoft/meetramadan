<?

class Controller_Settings extends Template {

    function action_index()
    {
        $this->checkLogin();

        if (!(bool)$this->user->completed_register)
            $this->redirect('/');

        $view = View::factory('pages/settings/index');

        $this->template->content = $view;
        $this->template->title = 'Параметры';

    }

}

?>