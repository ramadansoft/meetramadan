<?
class Controller_Videos extends Template {

    function before()
    {
        parent::before();
    }

    function action_index()
    {
        $view = View::factory('pages/videos/list');
        $view->videos = ORM::factory('Video')->order_by('date','desc')->find_all();
        $this->template->content = $view;
        $this->template->title = 'Видео священного Корана';
    }

    function action_add()
    {
        $this->checkAdminLogin();

        if (Security::check_token())
        {
            $video = ORM::factory('Video');
            $video->name = Arr::get($_POST,'name');
            $video->date = time();
            $video->url = Text::youtube(Arr::get($_POST,'url'));
            $video->save();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Видео успешно добавлено',
                JSONA_REDIRECT => '/videos',
            ));

        } else {
            $view = View::factory('pages/videos/add');

            $this->template->content = $view;
            $this->template->title = 'Видео';
        }
    }

    function action_edit()
    {
        $this->checkAdminLogin();

        $video = ORM::factory('Video',$this->request->param('id'));

        if (!$video->loaded())
            throw new HTTP_Exception_404;

        if (!empty($_POST))
        {
            $video->name = Arr::get($_POST,'name');
            $video->url = Text::youtube(Arr::get($_POST,'url'));
            $video->save();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Видео успешно отредактировано',
                JSONA_REDIRECT => '/videos',
            ));
        } else {
            $view = View::factory('pages/videos/edit');
            $view->video = $video;

            $this->template->content = $view;
            $this->template->title = 'Редактирование видео';
        }
    }

    function action_delete()
    {
        $this->checkAdminLogin();

        $videos = ORM::factory('Video',$this->request->param('id'));

        if (!$videos->loaded())
            throw new HTTP_Exception_404;

        $videos->delete();

        $this->SendJSONData(array(
            JSONA_COMPLETED => 'Видео удалено',
            JSONA_REFRESHPAGE => '',
        ));

    }
    
    function action_random() {
        $video = Model_Video::getRandom();
        $this->auto_render = false;
        $this->response->body(json_encode([
            'name' => Text::limit_chars($video->name,70),
            'url' => $video->url,
        ]));
    }
}
?>
