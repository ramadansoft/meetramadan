<?
class Controller_Statistics extends Template {
    function action_index() {
        $this->checkRegister();
        $tests = Model_Test::getUserTests($this->user->id, ($this->user->has('roles',ROLE_ADMIN) ? TRUE : FALSE));

        $view = View::factory('pages/statistics/index');
        $view->tests = $tests;
        $view->day = $this->day();
        $view->ramadan = $this->ramadan();
        $view->part = ($this->request->param('id') == 'second') ? 'second' : 'first';
        $this->template->content = $view;
        $this->template->title = 'Статистика Рамадана';
    }
    function action_global() {
        $view = View::factory('pages/statistics/global');
        $view->cities = json_encode(Model_City::getCities());
        $this->template->content = $view;
        $this->template->title = 'Глобальная статистика';
    }
    function action_cities() {
        $city = ORM::factory('City',$this->request->param('id'));
        if ($city->loaded()) {
            $view = View::factory('pages/statistics/cities_users');
            $view->city = $city;
            $view->users = Model_User::getUsersByCity($city->id);

            $this->template->content = $view;
            $this->template->title = 'Статистика - ' . $city->city;
        } else {
            $view = View::factory('pages/statistics/cities');
            $view->cities = Model_City::getCitiesFull();

            $this->template->content = $view;
            $this->template->title = 'Статистика - Города';
        }
    }
}