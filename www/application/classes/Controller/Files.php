<?

class Controller_Files extends Template {

    function action_uploadimg()
    {
        $this->auto_render = false;

        $array = Validation::factory($_FILES);
        $array->rule('file', 'Upload::not_empty')
                ->rule('file', 'Upload::type', array(':value', array('jpg','jpeg', 'png', 'gif')));

        if ($array->check())
        {
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            $filename = date('Y-m-d-i-') . (string)rand(1000,9999999999) . '.' . $ext;

            Upload::save($_FILES['file'], $filename , 'upload', 0777);
            
            $image = Image::factory(DOCROOT . '/upload/' . $filename,'Imagick');
            
            // Если фотография шире 1024 пикселей - уменьшаем ее
            if ($image->width > 1024)
                $image->resize(1024,768, Image::WIDTH)->save(null,85);

            // Создадим превьюшку
            $thumbnail_filename = DOCROOT . Controller_Files::thumbName($filename);
            $image->resize(150, 150, Image::WIDTH)->save($thumbnail_filename, 90);
            
            // отображаем файл
            if (Arr::get($_REQUEST,'flashupload') == 'true')
            {
                $this->response->body('/upload/' . $filename);
            } else {
                $this->response->body('<img src="/upload/'.$filename.'" />');
            }

        }

    }
    
    // Вернуть название превьюшки из имени файла
    static function thumbName($filename)
    {
        return '/upload/' . pathinfo(DOCROOT . $filename, PATHINFO_FILENAME) . '-thumb.' . pathinfo(DOCROOT . $filename, PATHINFO_EXTENSION);
    }
}

?>
