<?
class Controller_Page extends Template {
    
    function action_show()
    {
        $page = ORM::factory('Page',array('lat_name' => $this->request->param('page')));
        
        if (!$page->loaded())
            throw new HTTP_Exception_404;
        
        $view = View::factory('pages/page/show');
        $view->page = $page;
        
        $this->template->content = $view;
        $this->template->title = $page->name;
        $this->template->desc = $page->text;
    }

	public function action_admin()
	{
		$this->checkAdminLogin();

        $view = View::factory('pages/page/list');
        $view->pages = ORM::factory('Page')->find_all();

		$this->template->content = $view;
		$this->template->title = 'Страницы';
	}

    function action_add()
    {
        $this->checkAdminLogin();

        if (Security::check_token())
        {
            $page = ORM::factory('Page');
            $page->name = Arr::get($_POST,'name');
            $page->lat_name = Text::RusToLat(Arr::get($_POST,'lat_name'));
            $page->text = Arr::get($_POST,'text');
            $page->save();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Страница создана',
                JSONA_REDIRECT => '/' . $page->lat_name,
            ));
        } else {
            $view = View::factory('pages/page/add');

            $this->template->content = $view;
            $this->template->title = 'page';
            //$this->template->keywords = $page->keywords;
        }
    }

    function action_edit()
    {
        $this->checkLogin();
        
        $page = ORM::factory('Page',$this->request->param('id'));
        
        if (!$page->loaded())
            throw new HTTP_Exception_404;
            
        if (Security::check_token())
        {
            $page->name = Arr::get($_POST,'name');
            $page->text = Arr::get($_POST,'text');
            //$page->keywords = Arr::get($_POST,'keywords');
            $page->save();
            
            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Страница обновлена',
                JSONA_REDIRECT => '/' . $page->lat_name,
            ));
        } else {
            $view = View::factory('pages/page/edit');
            $view->page = $page;
            
            $this->template->content = $view;
            $this->template->title = 'page';
        }
    }
    
    function action_delete()
    {
        $this->checkLogin();
        
        $page = ORM::factory('Page',$this->request->param('id'));
        
        if (!$page->loaded())
            throw new HTTP_Exception_404;
        
        $page->delete();
        
        $this->redirect('/');
        
    }
}