<? class Controller_Tests extends Template {
    public function action_info() {
        $test = ORM::factory('Test',$this->request->param('id'));

        if (!$test->loaded())
            throw new HTTP_Exception_404;

        $view = View::factory('pages/tests/info');
        $view->test = $test;

        $this->template->content = $view;
        $this->template->title = $test->name;

    }
	public function action_admin() {
		$this->checkAdminLogin();

        $view = View::factory('pages/tests/admin/list');
        $view->tests = ORM::factory('Test')->find_all();

		$this->template->content = $view;
		$this->template->title = 'Задачи';
	}
	public function action_add() {
		$this->checkAdminLogin();

        if (Security::check_token())
        {
            $test = ORM::factory('Test');
            $test->name = Arr::get($_POST,'name');
            $test->score = Arr::get($_POST,'score');
            $test->desc = Arr::get($_POST,'desc');
            $test->need_modificator = isset($_POST['need_modificator']) ? true : false;
            $test->save();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Задача создана',
                JSONA_REDIRECT => '/tests/admin',
            ));
        } else {
            $view = View::factory('pages/tests/admin/add');
            $this->template->content = $view;
            $this->template->title = 'Создать новую задачу';
        }

	}
	public function action_edit() {
		$this->checkAdminLogin();

        $test = ORM::factory('Test', $this->request->param('id'));

        if (!$test->loaded())
            throw new HTTP_Exception_404;

        if (Security::check_token())
        {
            $test->name = Arr::get($_POST,'name');
            $test->score = Arr::get($_POST,'score');
            $test->desc = Arr::get($_POST,'desc');
            $test->need_modificator = isset($_POST['need_modificator']) ? true : false;
            $test->save();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Задача отредактирована',
                JSONA_REDIRECT => '/tests/admin',
            ));
        } else {
            $view = View::factory('pages/tests/admin/edit');
            $view->test = $test;

            $this->template->content = $view;
            $this->template->title = 'Редактирование задачи';
        }

	}
	public function action_delete() {
		$this->checkAdminLogin();

        $test = ORM::factory('Test', $this->request->param('id'));

        if (!$test->loaded())
            throw new HTTP_Exception_404;

        if (Security::check_token()) {
            $test->delete();

            $this->SendJSONData(array(
                JSONA_COMPLETED => 'Задача удалена',
                JSONA_REFRESHPAGE => '',
            ));
        } else {
            $view = View::factory('pages/tests/admin/delete');
            $view->test = $test;

            $this->template->content = $view;
            $this->template->title = 'Удаление задачи';
        }
	}
}