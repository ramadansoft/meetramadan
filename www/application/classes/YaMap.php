<? 
/**
 * Класс для получения координат с помощью API Яндекс-карт
 */
class YaMap {
    /**
     * Пытается получить координаты указанного адреса на карте
     * @param string $text Адрес
     * @return null | object
     */
    static function getPoint($text) {
        $params = [
            'geocode' => $text,     // Адрес
            'format'  => 'json',    // Формат ответа
            'results' => 1,         // Количество выводимых результатов
        ];
        $response = json_decode(file_get_contents('http://geocode-maps.yandex.ru/1.x/?' . http_build_query($params, '', '&')));
        if ($response->response->GeoObjectCollection->metaDataProperty->GeocoderResponseMetaData->found > 0) {
            $coords = explode(' ',$response->response->GeoObjectCollection->featureMember[0]->GeoObject->Point->pos);
            return (object) [
                'долгота'   => $coords[0],
                'longitude' => $coords[0],
                'широта'    => $coords[1],
                'latitude'  => $coords[1],
            ];
        } else {
            return null;
        }
    }
}