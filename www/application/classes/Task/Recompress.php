<?
/**
 * Произвести рекомпрессию JS и CSS файлов
 */
class Task_Recompress extends Minion_Task
{
    protected function _execute(array $params)
    {
        $themes = Kohana::$config->load('asset-merger.themes');
        foreach($themes as $theme => $items) {
            $doc = Document::instance($theme);
            foreach($items['js'] as $item) {
                $doc->addScript($item);
            }
            foreach($items['css'] as $item) {
                $doc->addStyleSheet($item);
            }
            $doc->recompress();
        }
    }
}