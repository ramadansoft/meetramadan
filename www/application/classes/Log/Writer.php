<?

abstract class Log_Writer extends Kohana_Log_Writer
{

    /**
     * Formats a log entry.
     *
     * @param   array $message
     * @param   string $format
     * @return  string
     */
    public function format_message(array $message, $format = "time --- level: body in file:line")
    {
        $message['time'] = Date::formatted_time(
            '@' . $message['time'],
            Log_Writer::$timestamp,
            Log_Writer::$timezone,
            true
        );
        $message['level'] = $this->_log_levels[$message['level']];

        unset($message['trace']);
        if (isset($message['additional']['exception'])) {
            $exception = $message['additional']['exception'];
            unset($message['additional']);
        }

        foreach ($message as $key => $m) {
            if (empty($m)) {
                unset($message[$key]);
            }
        }

        $string = strtr($format, $message);
        if (isset($exception)) {
            // Re-use as much as possible, just resetting the body to the trace
            $message['body'] = $exception->getTraceAsString();
            $message['level'] = $this->_log_levels[Log_Writer::$strace_level];

            $string .= PHP_EOL . strtr($format, $message);
        }

        return $string;
    }


}

?>
