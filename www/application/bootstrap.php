<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/Kohana/Core'.EXT;

if (is_file(APPPATH.'classes/Kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/Kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/Kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Asia/Yekaterinburg');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'ru_RU.utf-8');


/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('ru-ru');

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
//if (isset($_SERVER['KOHANA_ENV']))
//{
//    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
//}

Kohana::$environment = (isset($_SERVER['SERVER_NAME']) && $_SERVER['SERVER_NAME'] == 'meetramadan') ? Kohana::DEVELOPMENT : Kohana::PRODUCTION;
//Kohana::$environment = Kohana::PRODUCTION;

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => FALSE,
    'profile' => Kohana::$environment !== Kohana::PRODUCTION,
    'errors' => Kohana::$environment !== Kohana::PRODUCTION,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules([
    'system'    => MODPATH . 'system',
    'auth'      => MODPATH.'auth',       // Basic authentication
    'database'  => MODPATH . 'database', // Database access
    'image'     => MODPATH . 'image', // Image manipulation
    'orm'       => MODPATH . 'orm', // Object Relationship Mapping
    'assets'    => MODPATH . 'assets',
    'cache'     => MODPATH.'cache',
    'ulogin'    => MODPATH.'ulogin',
    'minion'    => MODPATH.'minion',
]);

Cookie::$salt = "sdaksdald32";
Cookie::$httponly = FALSE;

if (class_exists('Cache'))
{
    Cache::$default = 'apc';
}

include(APPPATH . 'functions' . EXT);
include(APPPATH . 'routes' . EXT);
include(APPPATH . 'constants' . EXT);

const INIT_COMPLETED = true;
