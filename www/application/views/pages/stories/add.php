<h1>Добавить историю благого поступка</h1>

<div class="stories-add">
    <? if (! $auth->logged_in()) { ?>
        <p>При добавлении истории требуется регистрация. Это сделано не для того, чтобы узнать, кто совершил историю, а для того, чтобы затянуть вас в наш проект, обезопасить сервис от дурных историй и, при необходимости, быстро удалить их из системы.</p>
        <p>Ваше имя и информация о вас нигде не будет отображена.</p>
        
        <table class="light-table">
            <col width="130">
            <tr>
                <td>E-Mail:</td>
                <td>
                    <input type="text" name="email" data-validation="isemail;notempty" />
                    <?=Form::validation('email')?>
                </td>
            </tr>
            <tr>
                <td>Пароль:</td>
                <td><input type="text" name="password" data-validation="notempty" /></td>
            </tr>
            <tr>
                <td>Название истории:</td>
                <td><input type="text" name="title" data-validation="notempty" /></td>
            </tr>
            <tr>
                <td>Текст:</td>
                <td><textarea data-validation="notempty;" style="height:200px;" name="text"></textarea></td>
            </tr>
        </table>
    <? } else { ?>
        <table class="light-table">
            <col width="130">
            <tr>
                <td>Название истории:</td>
                <td><input type="text" name="title" data-validation="notempty" /></td>
            </tr>
            <tr>
                <td>Текст:</td>
                <td><textarea style="height:200px;" name="text"></textarea></td>
            </tr>
        </table>
    <? } ?>
        
    <div data-input=".stories-add" data-link="/stories/add" class="normal-button senddata-token">Добавить историю</div>
</div>