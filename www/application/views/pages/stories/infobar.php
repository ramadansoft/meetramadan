<div class="story-info" id="story-info-<?=$story->id?>">
    <span class="story-info-date"><?=Text::humanDate($story->date)?></span>
    <span class="story-likes">
        <? if ($story->rating > 0) { ?>Оценили: <?=Text::chislitelnie($story->rating,['человек','человека','людей'])?><? } else { ?>Пока никто не оценил<? } ?>
    </span>
    <? if (in_array(Request::current()->action(),['show','like']) && !($auth->logged_in() && $user->id == $story->users_id)) { ?>
    <span class="my-like">
        <input type="checkbox" onclick="likestory('<?=$story->id?>')" <? if ($auth->logged_in() && $user->has('likes',$story->id)) { ?>checked<? } ?> /> Мне понравилось
    </span>
    <? } ?>
    <div class="clear"></div>
</div>