<? if ($type == null || true) { ?>
    <p>Дорогие братья и сестры, мы с радостью представляем вам новый раздел, на котором каждый может анонимно рассказать о совершенном им своем благом поступке. Знайте, что ваш благой поступок может замотивировать других людей совершить что-то подобное, что приведет только к благу для всех людей на этой планете.</p>
    <p>Передается, что Посланник Аллаха, да благословит его Аллах и приветствует, сказал: <i><b>"Кто укажет на благое, тому награда подобна награде совершившего его!"</i></b> (Муслим, Ахмад, ат-Тирмизий, абу Дауд и др.)</p>
    <hr/>
<? } ?>
<div class="clear"></div>
<a class="mb15px normal-button fl_r" href="/stories/add">Добавить историю</a>
<? if (!($type == null && $stories->count() < 4)) { ?>
    <div class="stories-filter">
        <a <?if($type == null) {?>class="active"<?}?> href="/stories">Новые</a>
        <a <?if($type == 'best') {?>class="active"<?}?> href="/stories/list/best">Лучшие</a>
        <a <?if($type == 'random') {?>class="active"<?}?> href="/stories/list/random">Случайная</a>
        <div class="clear"></div>
    </div>
<? } ?>
<div class="stories">
    <? foreach($stories as $story) { ?>
        <div class="story-preview">
            <a class="story-preview-title" href="/stories/show/<?=$story->id?>"><?=$story->title?></a>
            <div class="story-preview-text"><?=Text::limit_chars($story->text, 300,'...')?></div>
            <div class="story-preview-info">
                <span class="story-preview-info-date"><?=Text::humanDate($story->date)?></span>
                <span class="story-preview-info-rating">
                    <? if ($story->rating > 0) { ?>Оценили: <?=Text::chislitelnie($story->rating,['человек','человека','людей'])?><? } else { ?>Пока никто не оценил<? } ?>
                </span>
            </div>
        </div>
    <? } ?>
    <? if ($stories->count() == 0) { ?>
        <p>На сайте еще не выложено ни одной истории. Будьте первым :)</p>
    <? } ?>
    <?=$pager?>
</div>