<h1>Редактирование истории "<?=$story->title?>"</h1>
<div class="stories-add">
    <table class="light-table">
        <col width="130">
        <tr>
            <td>Название истории:</td>
            <td><input type="text" name="title" data-validation="notempty" value="<?=$story->title?>" /></td>
        </tr>
        <tr>
            <td>Текст:</td>
            <td><textarea style="height:200px;" name="text"><?=$story->text?></textarea></td>
        </tr>
    </table>
        
    <div data-input=".stories-add" data-link="/stories/edit/<?=$story->id?>" class="normal-button senddata-token">Сохранить изменения</div>
</div>