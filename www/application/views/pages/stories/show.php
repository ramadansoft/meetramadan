<div class="story-page" id="story-<?=$story->id?>">
    <a href="/stories">← читать остальные истории</a>
    <div class="clear"></div>
    <? if ($auth->logged_in() && $user->id === $story->users_id || $auth->logged_in() && $user->has('roles',ROLE_ADMIN)) { ?>
        <a href="/stories/edit/<?=$story->id?>" class="fl_l normal-button mr10px">Редактировать</a>
        <a href="/stories/delete/<?=$story->id?>" class="fl_l normal-button">Удалить</a>
        <div class="clear"></div>
    <? } ?>
    <div class="story-text">
        <?=nl2br($story->text)?>
        <div class="clear"></div>

        <? if (Arr::get($_GET,'pluso') == 'on') { ?>
        
        <span class="story-services">
            <script type="text/javascript">(function() {
                      //if (window.pluso)if (typeof window.pluso.start == "function") return;
                      var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                      s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                      s.class = 'plusoscript';
                      s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                      var h=d[g]('head')[0] || d[g]('body')[0];
                      h.appendChild(s);
                      })();</script>
            <div class="pluso" data-options="small,round,line,horizontal,nocounter,theme=01" data-services="vkontakte,odnoklassniki,twitter,facebook" data-background="transparent"></div>
        </span>
        <? } else { ?>
        <div style="display:block;margin-top:10px" onclick="Navigation.reloadFullPage('/stories/show/<?=$story->id?>?pluso=on');" class="mini-button">Поделиться историей в социальных сетях</div>
        <div class="clear"></div>
        <? } ?>
    </div>        
    <?=View::factory('pages/stories/infobar',['story'=>$story])?>
</div>