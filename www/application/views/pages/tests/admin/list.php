<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/tests/admin">Задачи</a>
</div>

<a class="normal-button" href="/tests/add">Создать новую</a>
<a class="ml15px normal-button" href="/modificators">Модификаторы</a>

<table class="tablesorter">
    <thead>
        <tr>
            <th>Название</th>
            <th>Баллы</th>
            <th>Управление</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($tests as $test) { ?>
        <tr>
            <td>
                <? if ((bool)$test->need_modificator) { ?><div class="fl_r">модификатор</div><? } ?>
                <?=$test->name?>
            </td>
            <td><?=$test->score?></td>
            <td>
                <a class="normal-button" href="/tests/edit/<?=$test->id?>">Изменить</a>
                <div class="normal-button showmessage ml10px" data-link="/tests/delete/<?=$test->id?>" >Удалить</div>
            </td>
        </tr>
        <? } ?>
    </tbody>
</table>

