<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/tests/admin">Тесты</a> -> Создать задачу
</div>

<table class="light-table" id="create-test">
    <tr>
        <td>Название</td>
        <td><input type="text" name="name" data-validation="notempty;" /></td>
    </tr>
    <tr>
        <td>Награда</td>
        <td><input type="text" name="score" data-validation="notempty;" /></td>
    </tr>
    <tr>
        <td>Необходим модификатор</td>
        <td><input type="checkbox" name="need_modificator" value="1" /></td>
    </tr>
    <tr>
        <td>Описание задачи</td>
        <td><textarea name="desc" style="height: 150px"></textarea></td>
    </tr>
    <tr>
        <td colspan="2"><div class="normal-button senddata-token" data-input="#create-test">Создать задачу</div></td>
    </tr>
</table>

<script>
    Core.editor('#create-test textarea','malik');
</script>