<h2><a href="/globalstatistics">Результаты</a> -> Статистика по городам</h2>

<table class="tablesorter">
    <thead>
        <tr>
            <th>Город</th>
            <th>Пользователей</th>
            <th>Среднее количество очков на человека</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($cities as $city) { ?>
        <tr>
            <td><a href="/statistics/cities/<?=$city->id?>"><?=$city->title?></a></td>
            <td><?=$city->users_count?></td>
            <td><?=$city->worships?></td>
        </tr>
        <? } ?>
    </tbody>
</table>