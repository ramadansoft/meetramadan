<h2><a href="/globalstatistics">Результаты</a> -> <a href="/statistics/cities">Статистика по городам</a> -> <?=$city->city?></h2>

<table class="tablesorter">
    <thead>
        <tr>
            <th>Пользователь</th>
            <th>Набрано очков</th>
            <th>Любимый вид поклонения</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($users as $user) { ?>
        <tr>
            <td><a href="/today/results/<?=$user->id?>"><?=$user->username?></a></td>
            <td><?=$user->worships?></td>
            <td><?=$user->lovelyworship?></td>
        </tr>
        <? } ?>
    </tbody>
</table>