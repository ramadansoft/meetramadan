<h2>Статистика по городам</h2>

<table class="light-table">
    <thead>
        <tr>
            <th>Город</th>
            <th>Пользователей</th>
            <th>Поклонений</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($cities as $city) { ?>
        <tr>
            <td><a href="/statistics/cities/<?=$city->id?>"><?=$city->title?></a></td>
            <td><?=$city->users_count?></td>
            <td><?=$city->worships?></td>
        </tr>
        <? } ?>
    </tbody>
</table>