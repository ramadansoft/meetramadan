<p>
    <a href="/statistics" class="tip-blue" <?if($part =='first'){?>style="text-decoration: underline;;"<?}?>>Первая половина Рамадана</a>
    &nbsp;&nbsp;&nbsp;<a href="/statistics/index/second" class="tip-blue" <?if($part =='second'){?>style="text-decoration: underline;;"<?}?>>Вторая половина Рамадана</a>
</p>
<?
    if ($part == 'first') {
        $begin_day = 1;
        $end_day = 15;
    } else {
        $begin_day = 16;
        $end_day = 30;
    }
?>
<table class="statistics">
    <thead>
        <tr class="statistics-days">
            <th class="tip-red">День №</th>
            <? for($x = $begin_day; $x <= $end_day; $x++) { ?>
                <th class="tip-blue <? if ($x == $day) echo 'active'?>"><?=$x?></th>
            <? } ?>
        </tr>
        <tr class="statistics-days">
            <th class="tip-red">День недели</th>
            <? $weekdays = ['вс','пн','вт','ср','чт','пт','сб'] ?>
            <? for($x = $begin_day; $x <= $end_day; $x++) { ?>
                <? $date = DateTime::createFromFormat('z',$ramadan-1+$x); ?>
                <th><?=$weekdays[$date->format('w')];?></th>
            <? } ?>
        </tr>
        <tr class="statistics-days">
            <th class="tip-red">Набрано баллов</th>
            <? for($x = $begin_day; $x <= $end_day; $x++) { ?>
                <th style="text-align: center"><?= Model_Test::getScore($user->id,date('Y'),$x) ?></th>
            <? } ?>
        </tr>
    </thead>
    <tbody style="max-height: 200px; overflow: auto;">
        <? foreach($tests as $test) { ?>
            <tr>
                <td class="statistics-title"><a href="/tests/info/<?=$test['id']?>"><?=$test['name']?></a></td>
                <? for($x = $begin_day; $x <= $end_day; $x++) { ?>
                <td>
                    <div onclick="toggleTest('<?=$x?>','<?=$test['id']?>',this)" class="today-checkbox <? if (Model_Test::checkCompleted($user->id,$test['id'],date('Y'),$x)) echo 'checked' ?>"></div>
                </td>
                <? } ?>
            </tr>
        <? } ?>
    </tbody>
</table>