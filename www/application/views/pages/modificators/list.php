<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/tests/admin">Задачи</a> -> <a href="/modificators">Модификаторы</a>
</div>

<a class="normal-button" href="/modificators/add">Создать нового</a>
<table class="tablesorter">
    <thead>
        <tr>
            <th>Название</th>
            <th>Тесты</th>
            <th>Управление</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($modificators as $mod) { ?>
        <tr>
            <td><?=$mod->name?></td>
            <td><?=Arr::ListArray(Arr::Make1Array($mod->tests(),'name'),'Отсутствуют')?></td>
            <td>
                <a class="normal-button" href="/modificators/edit/<?=$mod->id?>">Изменить</a>
                <div class="normal-button showmessage ml10px" data-link="/modificators/delete/<?=$mod->id?>">Удалить</div>
            </td>
        </tr>
        <? } ?>
    </tbody>
</table>

