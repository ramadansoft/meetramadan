<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/tests/admin">Задачи</a> -> <a href="/modificators">Модификаторы</a> -> Создать модификатор
</div>

<table class="light-table" id="create-test">
    <tr>
        <td>Название</td>
        <td><input type="text" name="name" data-validation="notempty;" /></td>
    </tr>
    <tr>
        <td>Тесты и дополнительные награды за них</td>
        <td>
            <div id="test-list"></div>
            <div class="normal-button" onclick="$('#test-list').append($.tmpl($('#t-test')))">Добавить тест</div>
        </td>
    </tr>
    <tr>
        <td colspan="2"><div class="normal-button senddata-token" data-input="#create-test">Создать задачу</div></td>
    </tr>
</table>

<script>
</script>

<!-- Шаблон теста -->
<script id="t-test" type="text/x-jquery-tmpl">
    <table class="light-table">
        <tr>
            <td>Тест</td>
            <td><?=Form::select('tests_ids[]',$tests)?></td>
        </tr>
        <tr>
            <td>Дополнительная награда</td>
            <td>
                <input type="text" name="tests_scores[]" data-validation="notempty;" />
            </td>
        </tr>
        <tr>
            <td colspan="2"><div class="normal-button" onclick="$(this).parent().parent().parent().remove();">Удалить</div></td>
        </tr>
    </table>
</script>

