<div id="upload-image"></div>
<div class="mini-button showmessage fl_r" data-link="/gallery/delete/<?=$theme->id?>">Удалить</div>
<a class="mr10px mini-button fl_r" href="/gallery/edit/<?=$theme->id?>">Редактировать</a>
<div class="mr10px mini-button fl_r senddata-token" data-link="/gallery/toggleshow/<?=$theme->id?>">Переключить видимость</div>
<? if (!$theme->show) { ?><h3>Галерея скрыта</h3><? } ?>
<div class="clear"></div>

<div id="photos">
    <? foreach($photos as $photo) { ?>
        <div data-path="<?=$photo->path?>" class="photo" style="background-image: url(<?=$photo->thumb?>)">
            <div class="setmain" onclick="Gallery.setMainPhoto($(this).parent())"></div>
            <div class="close" onclick="Gallery.removePhoto(this);"></div>
        </div>
    <? } ?>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    
    Gallery.id = '<?=$theme->id?>';
    
    Upload.init('upload-image','<?= Security::token()?>',function(serverData) {

        var cont = $('#photos');
        $('.clear',cont).remove();
        $(cont).append('<div data-path="'+serverData+'" class="photo" style="background-image: url('+serverData+')"> <div class="setmain" onclick="Gallery.setMainPhoto($(this).parent())"></div> <div class="close" onclick="Gallery.removePhoto(this);"></div> </div>');
        $(cont).append('<div class="clear"></div>');

    },'*.jpg;*.jpeg;*.png','Изображения','normal', function() {
        // Когда все фотографии загрузятся на сервер, послать их на сервер
        Notify.message('Все фотографии загружены на сервер', 'completed');
        Gallery.savePhotos();
    });
    
    $('#photos').sortable({
        update: function(event, ui) {
            // Когда произошла пересортировка, послать на сервер новый массив с данными
            Gallery.savePhotos();
        }
    });
</script>