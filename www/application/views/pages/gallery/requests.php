<h3>Заявки на тренинг <?=$training?></h3>
<table class="light-table">
    <? foreach($requests as $request) { ?>
    <tr<? if ($request->checked) { ?> style="background: #ccc"<? } ?>>
        <td><?=$request->fio?></td>
        <td><?=$request->email?></td>
        <td><?=$request->phone?></td>
        <td><a class="senddata-token" href="/trainings/togglerequest/<?=$request->id?>"><?= ((bool)$request->checked) ? 'Обработана' : 'Новая'?></a></td>
        <td><div class="senddata-token mini-button" href="/trainings/deleterequest/<?=$request->id?>">Удалить</div></td>
    </tr>
    <? } ?>
</table>