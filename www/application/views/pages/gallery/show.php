<? if ($auth->logged_in()) { ?>

    <div data-link="/trainings/delete/<?=$training->id?>" class="mini-button fl_r senddata-token">Удалить</div>
    <a href="/trainings/edit/<?=$training->id?>" class="mr5px mini-button fl_r">Редактировать</a>
    <div data-link="/trainings/toggleshow/<?=$training->id?>" class="mr5px mini-button fl_r senddata-token">Скрыть/Отобразить</div>
    <a class="mini-button mr5px fl_r" href="/trainings/requests/<?=$training->id?>">Заявки (<?=$training->requests->get_model_count()?>)</a>


<? } ?>

<h1>Тренинг "<?=$training->name?>"</h1>
<? if (!$training->show) { ?><h2>тренинг скрыт</h2><? } ?>
<? if (!empty($training->avatar_link)) { ?>
<div class="avatar">
    <img style="width: 230px;" src="<?=$training->avatar_link?>" alt="<?=$training->name?>" />
</div>
<? } ?>
<div class="fnt-normal">
    <?=$training->text?>
</div>

<table class="light-table">
    <tr>
        <td>Дата проведения мероприятия</td>
        <td><?=Text::humanDate($training->date)?></td>
    </tr>
</table>

<h3>Сделайте заявку на участие в тренинге!</h3>
<table class="light-table" id="training-request">
    <tr>
        <td>Ваше имя:</td>
        <td>
            <input type="text" name="fio" data-validation="notempty"/>
            <?=Form::validation('fio')?>
        </td>
    </tr>
    <tr>
        <td>E-Mail:</td>
        <td>
            <input type="text" name="email" data-validation="notempty;isemail"/>
            <?=Form::validation('email')?>
        </td>
    </tr>
    <tr>
        <td>Контактный телефон:</td>
        <td>
            <input type="text" name="phone" data-validation="notempty;isphone"/>
            <?=Form::validation('phone')?>
        </td>
    </tr>
    <tr>
        <td colspan="2"><div class="normal-button senddata-token fl_r" data-link="/trainings/createrequest/<?=$training->id?>" data-input="#training-request">Записаться на тренинг</div></td>
    </tr>
</table>

