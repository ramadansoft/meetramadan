<h1>Галерея</h1>

<? if ($auth->logged_in()) { ?>
    <a class="normal-button" href="/gallery/create">Создать новый альбом</a>
    <div class="clear"></div>
<? } ?>


<? foreach($themes as $theme) { ?>
    <div id="galleries">
        <div class="gallery<? if (!$theme->show) { ?> notshow<? } ?>">
            <? if ($auth->logged_in()) { ?>
                <a class="mini-button fl_r" href="/gallery/editphotos/<?=$theme->id?>">Управление</a>
            <? } ?>
            <div class="avatar" onclick="Gallery.show('<?=$theme->id?>')" style="background-image: url(<?=$theme->avatar_link?>)"></div>
            <div class="text">
                <h3><?=$theme->name?></h3>
                <p><?=nl2br($theme->desc)?></p>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
<? } ?>

<? if ($themes->count() == 0) { ?>
    <p>Извините, но в данный момент у нас нет ни одного альбома с фотографиями.</p>
<? } ?>