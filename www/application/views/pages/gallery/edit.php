<div id="edit-gallery">
    <h2>Редактирование альбома "<?=$theme->name?>"</h2>
    <table class="light-table">
        <thead>
            <tr>
                <th style="width: 150px;"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Отображение альбома</td>
                <td>
                    <p><?=Form::checkbox('show','1',(bool)$theme->show)?> Альбом виден посетителям</p>
                    <p class="fnt-small">Если желаете сначала удостовериться в том, что все правильно отформатировано, не ставьте эту галочку до того момента, пока все не отладите.</p>
                </td>
            </tr>
            <tr>
                <td>Название</td>
                <td>
                    <?=Form::input('name',$theme->name,array('data-validation' => 'notempty'))?>
                    <?=Form::validation('name')?>
                </td>
            </tr>
            <tr>
                <td>Описание</td>
                <td>
                    <?=Form::textarea('desc',$theme->desc,array('data-validation' => 'notempty'))?>
                    <?=Form::validation('desc')?>
                </td>
            </tr>
            <tr>
                <td>Дата</td>
                <td>
                    <?
                    echo Form::input('date', date('d.m.Y',$theme->date), array('data-validation'=>'notempty;isdate'));
                    echo Form::hidden('unix-date', $theme->date);
                    echo Form::validation('date');
                    ?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="mt5px mb10px ml10px normal-button senddata-token" data-input="#edit-gallery" data-link="/gallery/edit/<?=$theme->id?>">Применить изменения</div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    Core.datepicker('date');
</script>