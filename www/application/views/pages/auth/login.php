<div class="inputs">
    <div class="close" onclick="Auth.closeLogin()"></div>
    <? if (isset($_GET['loginfail'])) { ?><p id="login-error">Неверная связка email + пароль</p><? } ?>
    <? if (isset($_GET['passwordrestored'])) { ?><p class="tip-blue">Система сгенерировала вам новый пароль и отправила его на вашу почту.</p><? } ?>
    <form action="/auth/login" method="POST">
        
        <table class="registration-selects" style="margin-bottom: 40px;">
            <tr>
                <td class="title">E-mail</td>
                <td class="select"><input type="text" name="login" data-validation="notempty" /></td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td class="title">Пароль</td>
                <td class="select"><input type="password" name="password" data-validation="notempty" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="submit">
                        <input type="submit" class="normal-button" name="btn-login" value="Войти" />
                    </span>
                </td>
            </tr>
        </table>
        
        <a class="mini-button" href="/auth/register" id="not-have-account">У меня еще нет аккаунта</a>
        <div class="mini-button ml15px" onclick="Auth.remindPassword();" id="remind-password">Напомнить пароль</div>

        <div class="clear"></div>
    </form>
</div>
