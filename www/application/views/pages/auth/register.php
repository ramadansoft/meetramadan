<table id="registration" class="registration-selects" style="margin-bottom: 40px;">
    <tr>
        <td class="title">E-mail</td>
        <td class="select"><input type="text" name="email" data-validation="notempty" /></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
        <td class="title">Пароль</td>
        <td class="select"><input type="text" name="password" data-validation="notempty" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <span class="submit">
                <button style="width: 200px;" class="submit senddata-token normal-button" data-input="#registration" data-link="/auth/register">Зарегистрироваться</button>
            </span>
        </td>
    </tr>
</table>
