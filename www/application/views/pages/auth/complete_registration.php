<div id="register-steps">
    <div class="step" id="step-1">
        <h3>шаг 1 из 3</h3>
        <p>
            Ас-саляму алейкум уа рахматуллахи уа баракятух!
            <br/><br/>
            Вы практически завершили регистрацию в нашем проекте. Осталось узнать о вас немного подробностей, чтобы наша система смогла сгенерировать для вас список тех дел, которые вам необходимо будет выполнять каждый день Рамадана.
            <br/><br/>
            Аллах создал людей разными, и у каждого человека свои слабости в этом мире. Кто-то не может бросить курить, кто-то слишком увлекся музыкой, забыв про священный Коран, кто-то не может перебороть себя в страсти к женщинам, и так далее.
            <br/><br/>
            Вам необходимо ответить на несколько вопросов, чтобы система проставила вам правильные испытания и правильные награды за них. В первую очередь это необходимо прежде всего вам, а информация, которая будет передана на сайт, ИншаАлла, будет надежно закрыта.
        </p>
        <p style="font-weight: bold">Внимание! Сегодня мы наконецто загрузили в систему большую базу (больше милллиона городов) населенных пунктов всего мира, и теперь вы можете выбрать свой населенный пункт. По этой причине все регистрации пользователей были сбиты, чтобы каждый указал свой реальный город.</p>
        <div class="normal-button fl_r" onclick="Auth.changeStep(2)">Все понятно, задавайте вопросы</div>
        <div class="clear"></div>
    </div>
    <div class="step" id="step-2" style="display: none;">
        <h3>шаг 2 из 3</h3>
        <p class="tip-blue">Пожалуйста, заполните следующие поля:</p>
        <table class="registration-selects">
            <tr>
                <td class="title">Имя</td>
                <td class="select">
                   <input type="text" name="fio" data-validation="notempty" value="<?=$user->username?>"/>
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td class="title">Город</td>
                <td class="select">
                    <?=Form::search('city_id', array('model' => 'city'), 'Введите название вашего города')?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="margin-top: 50px; width: 80px;" class="normal-button" onclick="if (Auth.checkSecondStep()) Auth.changeStep(3)">Далее</div>
                </td>
            </tr>
        </table>
    </div>

    <div class="step" id="step-3" style="display: none;">
        <h3>шаг 3 из 3</h3>

        <p>
            Пожалуйста, отметьте галочками те пункты, которые характерны для вас. Это позволит построить систему баллов для вас таким образом, чтобы уровень награды за определенные действия был приблизительно соответствующим для вас лично.
            <br/><br/>
            Система запомнит эти данные, и на их основе построит для вас таблицу наград.
        </p>

        <? foreach($modificators as $mod) { ?>
            <p style="margin: 5px 0"><input name="mods[]" value="<?=$mod->id?>" type="checkbox" <? if($user->has('modificators',$mod->id)) { ?> checked<? } ?> /> <?=$mod->name?></p>
        <? } ?>

        <div class="mt10px normal-button fl_r senddata-token" data-link="/auth/completeregister" data-input="#register-steps" >Завершить регистрацию</div>
        <div class="clear"></div>
    </div>
</div>