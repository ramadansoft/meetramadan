<p><span class="tip-red"><?=Text::humanDate(time())?></span> <span class="tip-blue">Пожалуйста, отметьте галочками те дела, которые вы сделали сегодня.</span></p>
<a style="text-decoration: underline" class="tip-blue mb15px" href="<? if ($headtitle2 == 'Вчера') echo '/today/show/yesterday'; else echo '/today';?>">Переключиться на <?=$headtitle2?></a>
<div class="clear"></div>
<div class="today-inputs" style="margin-top: 15px;">
    <? foreach($tests as $test) { ?>
        <div class="today-input" id="today-test-<?=$test['id']?>">
            <div onclick="toggleTest('<?=$day?>','<?=$test['id']?>',this)" class="today-checkbox <? if ($test['completed']) {?> checked<? } ?>"></div>
            <div class="today-input-desc"><span class="today-input-desc-inner"><a href="/tests/info/<?=$test['id']?>"><?=$test['name']?></a></span></div>
        </div>
    <? } ?>
    <div class="clear"></div>
</div>
