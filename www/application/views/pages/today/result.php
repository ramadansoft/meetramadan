<a class="normal-button" href="/today/results/<?=$suser->id?>">Ссылка на результат</a>
<a class="ml15px normal-button" href="/statistics/cities/<?=$suser->cities_id?>">Результаты других пользователей города <?=$suser->city->city?></a>
<div class="clear"></div>
<div id="results">
    <? if ($auth->logged_in() && $user->id === $suser->id) { ?>
        <div class="title">СРЕДНИЙ РЕЗУЛЬТАТ ПОЛЬЗОВАТЕЛЕЙ</div>
        <div id="usersscore">0</div>
    <? } ?>
        <div class="title"><? if ($auth->logged_in() && $user->id === $suser->id) { ?> ВАШ РЕЗУЛЬТАТ <? } else { ?> <?=$suser->username?>, <?=$suser->city->city?> <? } ?></div>
    <div id="totalscore">0</div>
    <div id="status" style="display: none"></div>
    <div id="desc" style="display: none"></div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('#yesterday-title').hide();
        $('#today-title').html('Итоги');

        Total.init(<?=$totalscore?>,<?=$usersscore?>);
    });
</script>