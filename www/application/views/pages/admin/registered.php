<table class="light-table">
    <tr>
        <td><a href="/admin/registered">Обновить</a></td>
        <td>Всего пользователей: <?=ORM::factory('User')->get_model_count()?></td>
        <td>Зарегистрировалось: <?=$users->get_model_count()?></td>
        <td>Из них недавно были на сайте: <?=$logins?></td>
    </tr>
</table>

<?
    $susers = $users->pagination(100);
    $pager = $users->pagination_html(100);
?>

<?=$pager?>
<table class="tablesorter">
    <thead>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>Город</th>
            <th>Кол-во логинов</th>
            <th>E-Mail</th>
        </tr>
    </thead>
    <? foreach($susers as $user) { ?>
    <tr>
        <td><?=$user->id?></td>
        <td><?=$user->username?></td>
        <td><?=$user->city->city?></td>
        <td><?=$user->logins?></td>
        <td><?=$user->email?></td>
    </tr>
    <? } ?>
</table>
<?=$pager?>
