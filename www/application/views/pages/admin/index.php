<h2>Добро пожаловать, <?= $user ?>!</h2>
<a class="senddata" href="/auth/logout">Выйти из системы</a>

<ul>
    <? if ($user->has('roles',ROLE_ADMIN)) { ?>
        <li><a href="/page/admin">Страницы</a></li>
        <li><a href="/users">Пользователи</a> (<a href="/admin/registered">статистика</a>)</li>
        <li><a href="/tests/admin">Задачи</a></li>
    <? } ?>
</ul>

<a class="normal-button" href="/admin/recompress" target="_blank">Пересжать JS и CSS-файлы</a>
<div class="clear"></div>
