<? if ($auth->logged_in() && $user->has('roles',ROLE_ADMIN)) { ?>
    <a class="normal-button" href="/videos/add">Создать новое видео</a>
    <div class="clear"></div>
<? } ?>
<div id="videos">
<? foreach($videos as $video) { ?>
    <div class="video">
        <? if ($auth->logged_in() && $user->has('roles',ROLE_ADMIN)) { ?>
            <a class="mini-button manage" href="/videos/edit/<?=$video->id?>">Управление</a>
            <a class="really mr5px mini-button manage senddata-token" href="/videos/delete/<?=$video->id?>">Удалить</a>
            <div class="clear"></div>
        <? } ?>
        <div class="avatar" onclick="Video.show('<?=$video->url?>')" style="background-image: url(http://i.ytimg.com/vi/<?=$video->url?>/0.jpg)"></div>
        <div class="text"><?=Text::limit_chars($video->name,70)?></div>
        <div class="clear"></div>
    </div>
<? } ?>
    <div class="clear"></div>
</div>

<? if ($videos->count() == 0) { ?>
    <p>Извините, но в данный момент у нас нет ни одного видеоролика.</p>
<? } ?>




