<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/users">Пользователи</a>
</div>

<a class="normal-button" href="/users/add">Создать</a>

<table class="tablesorter">
	<thead>
		<tr>
			<th>№</th>
			<th>ФИО</th>
			<th>Логин</th>
			<th>Email</th>
			<th>Последний вход</th>
			<th>Админ</th>
			<th style="width: 50px">Управление</th>
		</tr>
	</thead>
	<tbody>
		<? foreach($users as $usr) { ?>
		<tr>
			<td><?=$usr->id?></td>
			<td><?=$usr?></td>
			<td><?=$usr->username?></td>
			<td><?=$usr->email?></td>
			<td><?=Text::humanDateTime($usr->last_login)?></td>
			<td><a class="senddata-token" href="/users/toggle/<?=$usr->id?>"><?=$usr->has('roles',ROLE_ADMIN) ? 'Да' : 'Нет'?></a></td>
			<td>
                <a class="mini-button" href="/users/edit/<?=$usr->id?>">Редактировать</a>
                <div class="showmessage mini-button" data-link="/users/password/<?=$usr->id?>">Пароль</div>
                <div class="showmessage mini-button" data-link="/users/delete/<?=$usr->id?>">Удалить</div>
            </td>
		</tr>
		<? } ?>
	</tbody>
</table>