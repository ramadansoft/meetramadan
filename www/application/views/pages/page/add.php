<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/page/admin">Страницы</a> -> Создать страницу
</div>

<table id="edit-page" class="light-table">
    <tr>
        <td>Название</td>
        <td><input data-validation="notempty" type="text" name="name" /></td>
    </tr>
    <tr>
        <td>Название латиницей</td>
        <td><input data-validation="notempty" type="text" name="lat_name" /></td>
    </tr>
    <tr>
        <td>Текст страницы</td>
        <td><textarea style="height: 250px;" id="text" name="text"></textarea></td>
    </tr>
    <tr>
        <td colspan="2"><div data-input="#edit-page" class="normal-button senddata-token">Создать страницу</div></td>
    </tr>
</table>

<script type="text/javascript">
    Core.editor('#text','malik');
</script>