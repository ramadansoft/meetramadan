<div id="menu_tree">
	<a href="/admin">Главное меню</a> -> <a href="/page/admin">Страницы</a>
</div>

<a class="normal-button" href="/page/add">Создать</a>

<table class="tablesorter">
    <thead>
        <tr>
            <th>Название</th>
            <th>Управление</th>
        </tr>
    </thead>
    <tbody>
        <? foreach($pages as $page) { ?>
        <tr>
            <td><?=$page->name?></td>
            <td>
                <a class="normal-button" href="/page/edit/<?=$page->id?>">Изменить</a>
                <div class="normal-button showmessage ml10px" data-link="/page/delete/<?=$page->id?>">Удалить</div>
            </td>
        </tr>
        <? } ?>
    </tbody>
</table>

