<h3>Редактирование страницы "<?=$page->name?>"</h3>

<table id="edit-page" class="light-table">
    <tr>
        <td>Название</td>
        <td><input data-validation="notempty" type="text" name="name" value="<?=$page->name?>" /></td>
    </tr>
    <tr>
        <td>Текст страницы</td>
        <td><textarea style="height: 250px;" id="text" name="text"><?=$page->text?></textarea></td>
    </tr>
    <tr>
        <td colspan="2"><div data-input="#edit-page" class="normal-button senddata-token">Применить изменения</div></td>
    </tr>
</table>

<script type="text/javascript">
    Core.editor('#text','malik');
</script>