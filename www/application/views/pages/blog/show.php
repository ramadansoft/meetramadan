<? if (Request::current()->controller() == 'blog') { ?><div id="bloghead"></div><? } ?>

<? if ($auth->logged_in()) { ?>
    <a class="mini-button manage fl_r" href="<?=$main_link?>/edit/<?=$blog->id?>">Управление</a>
    <a class="mr5px mini-button really manage fl_r" href="<?=$main_link?>/delete/<?=$blog->id?>">Удалить</a>
    <div data-link="<?=$main_link?>/toggleshow/<?=$blog->id?>" class="mr5px mini-button fl_r senddata-token">Скрыть/Отобразить</div>

    <? if (!$blog->show) { ?>
    <h2>Запись скрыта от посетителей</h2>
    <? } ?>
<? } ?>

<? if (Request::current()->controller() != 'blog') { ?>
    <h1><a href="<?=$main_link?>"><?=$blog_name?></a></h1>
<? } ?>

<div class="fl_r"><?=Text::humanDate($blog->date)?></div>
<h2><?=$blog->name?></h2>
<? if (!empty($blog->avatar_link)) { ?>
<div class="avatar">
    <img style="width: 230px;" src="<?=$blog->avatar_link?>" alt="<?=$blog->name?>" />
</div>
<? } ?>
<div class="mt15px fnt-normal">
    <?=nl2br($blog->text)?>
</div>
<div class="clear"></div>
