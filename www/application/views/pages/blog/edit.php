<div id="create-training">
    <h2>Редактировать запись</h2>
    <table class="light-table">
        <thead>
            <tr>
                <th style="width: 150px;"></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Отображение записи</td>
                <td>
                    <p><?=Form::checkbox('show','1',(bool)$blog->show)?> Запись видна посетителям</p>
                    <p class="fnt-small">Если желаете сначала удостовериться в том, что все правильно отформатировано, не ставьте эту галочку до того момента, пока все не отладите.</p>
                </td>
            </tr>
            <tr>
                <td>Название</td>
                <td><?=Form::input('name',$blog->name)?></td>
            </tr>
            <tr>
                <td>Дата</td>
                <td>
                    <?
                    echo Form::input('date', date('d.m.Y',$blog->date), array('data-validation'=>'notempty;isdate'));
                    echo Form::hidden('unix-date',$blog->date);
                    echo Form::validation('date');
                    ?>
                </td>
            </tr>
            <tr>
                <td>Текст</td>
                <td>
                    <?=Form::textarea('text',$blog->text,array('id'=>'text','style' => 'height: 250px;'))?>
                    <?=Form::validation('text')?>
                </td>
            </tr>
            <tr>
                <td>SEO - Ключевые слова</td>
                <td><?=Form::input('keywords',$blog->keywords)?></td>
            </tr>
            <tr>
                <td>Картинка на заголовок (рекомендуемый размер: 230х140)</td>
                <td id="avatar_link">
                    <div class="mini-button fl_r" onclick="$('#image-preview').html(''); $('input[name=avatar_link]').val('');">без изображения</div>
                    <div id="upload-image"></div>
                    <p>Выбранный файл:</p>
                    <div id="image-preview">
                        <? if (empty($blog->avatar_link)) { ?>отсутствует<? } else { ?>
                        <a target="_blank" href="<?=$blog->avatar_link?>"><img style="width: 150px" src="<?=$blog->avatar_link?>"/></a>
                        <? } ?>
                    </div>
                    <?=Form::hidden('avatar_link',$blog->avatar_link)?>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="mt5px mb10px ml10px normal-button senddata-token" data-input="#create-training">Применить изменения</div>
    <div class="clear"></div>
</div>

<script type="text/javascript">
    Core.editor('#text','malik');
    Core.datepicker('date');
    
    Upload.init('upload-image','<?= Security::token()?>',function(serverData) {
        $('#image-preview').html('<a target="_blank" href="'+serverData+'"><img style="width: 150px" src="'+serverData+'"/></a>');
        $('#avatar_link input').val(serverData);
    },'*.jpg;*.jpeg;*.png','Изображения','normal');
</script>