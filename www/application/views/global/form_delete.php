<? // echo Form::open($controller_link.'delete/'.$global_item->id)?>
<?=Form::open(Request::current()->uri(),array('onsubmit'=>'return false;'));?>
	<fieldset>
		<legend><?=$global_params['delete']['caption']?></legend>
		<p><?=$global_params['delete']['answer']?></p>
        <?=Form::hidden('confirm', 'yes');?>
	<div
		class="senddata-token normal-button mr15px"
		data-link="/<?=Request::current()->uri()?>"
		data-input='#message-text form'
	><?=$global_params['delete']['button']?></div>

	<div class="closemessage normal-button">Отмена</div>
	<div class="clear"></div>
	</fieldset>
<?=Form::close()?>
