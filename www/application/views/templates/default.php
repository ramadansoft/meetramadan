<!DOCTYPE html>
<html>
    <head>
        <title><?= $title ?></title>
        <meta name="description" content="<?=Text::limit_chars(Text::desc($desc),160,'...')?>" />
        <meta name="keywords" content="<?=$keywords?>" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <?= $header ?>
        
        <script src="http://api-maps.yandex.ru/2.0/?load=package.full&mode=debug&lang=ru-RU" type="text/javascript"></script>
        <? if (Kohana::$environment == Kohana::PRODUCTION) { ?>
            <?= View::factory('components/analitycs'); ?>
        <? } ?>        
        
    </head>
    <body>
        <div id="content-wrap">
            <? if ($MODE == RAMADAN_NEAR) { ?>
                <div id="countdown-container">
                    <div id="countdown"></div>
                    <div id="countdown-names">
                        <div class="name">дней</div>
                        <div class="name">часов</div>
                        <div class="name">минут</div>
                        <div class="name">секунд</div>
                    </div>
                    <div id="countdown-foot">... осталось до начала</div>
                </div>
                <script type="text/javascript">
                    <?
                        $ramadan = ORM::factory('Ramadan',['year' => date('Y')]);
                        $date = DateTime::createFromFormat('Y-z', date('Y') . '-' . $ramadan->startday);
                    ?>
                    $(function() {
                        ts = new Date(<?=$date->format('Y')?>, <?=$date->format('n')-1?>, <?=$date->format('j')?>);
                        $('#countdown').countdown({
                            timestamp	: ts,
                            callback	: function(days, hours, minutes, seconds) {
                                var message = "";
                                message += "Дней: " + days +", ";
                                message += "часов: " + hours + ", ";
                                message += "минут: " + minutes + " и ";
                                message += "секунд: " + seconds + " <br />";
                            }
                        });
                    });
                </script>
            <? } ?>
            <div id="leftpanel">
                <a class="logo-link" href="/"></a>

                <? /*
                <div class="quran-random">
                    <? $video = Model_Video::getRandom(); ?>
                    <div class="avatar avatar-1" style="display:none"></div>
                    <div class="avatar avatar-2" onclick="Video.show('<?=$video->url?>')" style="background-image: url(http://i.ytimg.com/vi/<?=$video->url?>/0.jpg)"></div>
                    <div class="text"><?=Text::limit_chars($video->name,70)?></div>
                    <div class="refresh-video" onclick="canNext = false; Core.loadRandomVideo();" title="Загрузить другое случайное видео"></div>
                    <div class="clear"></div>
                    <script type="text/javascript">
                        canNext = true;
                        setInterval(function(){
                            if (canNext) {
                                Core.loadRandomVideo();
                            } else {
                                canNext = true;
                            }
                        },20000);
                    </script>
                </div>
                */ ?>
                <?=View::factory('components/vk-group')?>
            </div>
            <div id="toppanel">
                <? if ($auth->logged_in()) { ?>
                    <div id="toppanel-logout" onclick="Auth.logout();">Выход</div>
                    <a id="toppanel-params" href="/settings">Параметры</a>
                <? } else { ?>
                    <a id="toppanel-register" href="/auth/register">Регистрация</a>
                    <div id="toppanel-login" onclick="Navigation.reloadFullPage('/auth/login')">Вход</div>
                <? } ?>
            </div>
            <div class="content-container">
                <h1 class="page-title"><?=$title?></h1>
                <? if (! ($auth->logged_in() && $user->completed_register == FALSE)) { ?>
                <div id="buttons">
                    <? if ($MODE == RAMADAN_STARTED) { ?>
                        <a class="button-link button-today" href="/today" <? if (!$auth->logged_in() || !(bool)$user->completed_register) { ?>style="display:none" <? } ?>>Сегодня</a>
                        <a class="button-link button-statistics" href="/statistics<?if($CURRENT_DAY > 15) echo '/index/second'?>" <? if (!$auth->logged_in() || !(bool)$user->completed_register) { ?>style="display:none" <? } ?>>Статистика</a>
                    <? } ?>
                    <? if ($MODE == RAMADAN_ENDED) { ?>
                        <a class="button-link button-results" href="/globalstatistics">Результаты</a>
                    <? } ?>
                    <? if (! $auth->logged_in()) { ?>
                        <a class="button-link button-about_project" href="/about_project">О проекте</a>
                    <? } ?>
                    <a class="button-link button-about_ramadan" href="/about_ramadan">О Рамадане</a>
                    <a class="button-link button-stories" href="/stories">Истории</a>
                    <? if ($auth->logged_in() && $user->has('roles',ROLE_ADMIN)) { ?>
                        <a class="button-link button-admin" href="/admin">Админка</a>
                    <? } ?>
                </div>
                <? } ?>
                <div id="content">
                    <?= $content ?>
                </div>
            </div>
        </div>
        
		<?= View::factory('message-container'); ?>
		<?= View::factory('components/box'); ?>
		<? //echo View::factory('components/reformal'); ?>
		<?= View::factory('components/notifies'); ?>
        <?= View::factory('components/metrika'); ?>
    </body>

	<script type="text/javascript">
		CORE = {
			token: '<?=Security::token()?>',
            day: '<?=date('z',time())?>'
		}
	</script>
    <?= $footer ?>
</html>