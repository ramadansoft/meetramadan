<? if (Kohana::$environment === Kohana::PRODUCTION) { ?>

    <? foreach ($scripts as $file) { ?>
        <script type="text/javascript" src="/themes/<?= $file ?>"></script>
    <? } ?>
    <? foreach ($styles as $file => $type) { ?>
        <link type="text/css" href="/themes/<?= $file ?>" media="<?= $type ?>" rel="stylesheet"/>
    <? } ?>
<? } else { ?>
    <? foreach ($scripts as $file) { ?>
        <? $filemtime = filemtime(DOCROOT . '/themes/js/' . $file); ?>
        <script type="text/javascript" src="/themes/js/<?= $file ?>?<?= $filemtime ?>"></script>
    <? } ?>
    <? foreach ($styles as $file => $type) { ?>
        <? $filemtime = filemtime(DOCROOT . '/themes/' . $file); ?>
        <link type="text/css" href="/themes/<?= $file ?>?<?= $filemtime ?>" media="<?= $type ?>" rel="stylesheet"/>
    <? } ?>
<? } ?>

<? foreach ($custom as $file) {
    echo $file;
} ?>