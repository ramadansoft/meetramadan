<?php


// Статические страницы
Route::set('stories', 'stories/list/<type>')
  ->defaults(array(
    'controller' => 'stories',
    'action' => 'index',
    'type' => '(best|random)',
  ));

// Статические страницы
Route::set('static', '<page>',[
    'page' => etc::generatePages()
  ])
  ->defaults([
    'controller' => 'Page',
    'action' => 'show',
  ]);

// Глобальная статистика
Route::set('globalstatistics', 'globalstatistics')
	->defaults(array(
		'controller' => 'statistics',
		'action'     => 'global',
	));

// Отзывы
Route::set('showopinion', 'opinion(/<name>)')
	->defaults(array(
		'controller' => 'opinions',
		'action'     => 'index',
	));

// Тренинги
Route::set('showtraining', 'events(/<name>)')
	->defaults(array(
		'controller' => 'trainings',
		'action'     => 'show',
	));

// Страница "О нас"
Route::set('about', 'about')
	->defaults(array(
		'controller' => 'pages',
		'action'     => 'view',
		'name'     => 'about',
	));

Route::set('default', '(<controller>(/<action>(/<id>(/<stuff>))))', array('stuff' => '.*'))
	->defaults(array(
		'controller' => 'default',
		'action'     => 'index',
	));

?>
