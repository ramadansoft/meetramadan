<?

/**
 * Функция сначала ищет значение в переданном массиве $values
 * Если не находит, возвращает текущее значение модели $item
 * @param string $name Название параметра
 * @param Model $item Объект модели
 * @param array $values Ассоциативный массив со значениями
 * @return type
 */
function get_value($name, $item, $values) {
    if (!empty($values[$name])) {
        return $values[$name];
    } else {
        return $item->$name;
    }
}

/**
 * Специальная функция для получения значения из объекта на основе указанного объекта информации
 */
function get_item_value($item,$value = NULL) {
    // Если для значений был передан массив, обрабатываем его особым образом
    // Если в значениях массива встретится массив, значит это функция..
    // Если в значениях встретится простая строка, то передаем её как строку
    if (is_array($value)) {
        $item_value = '';
        foreach ($value as $value_param) {
            if (is_array($value_param)) {
                $item_method = $value_param[0];
                if (isset($value_param[1])) {
                    $item_value .= $item->$item_method($value_param[1]);
                } else {
                    $item_value .= $item->$item_method();
                }
            } else {
                $item_value .= $value_param;
            }
        }
        
        return $item_value;
    } elseif (empty($value)) {
        return (string)$item;
    } elseif (mb_substr($value, 0, 7) === '_text->') {
        $_value = mb_substr($value, 7);
        return $item->_text->$_value;
    } else {
        return method_exists($item, $value) ? $item->$value() : $item->$value;
    }
}

/**
 * Возвращает уникальное значение URL для определенной таблицы
 * @param string $model_name название модели
 * @param string $url_column_name название колонки для проверки
 * @param string $title текст для преобразования в URL
 */
function get_unique_url($model_name, $url_column_name, $title, $id = null) {
    $url = mb_strtolower(Text::RusToLat($title));

    $model = ORM::factory($model_name)->where($url_column_name, '=', $url);
    if (!empty($id)) {
        $model->where('id', '!=', $id);
    }
    $model = $model->find();

    if ($model->loaded()) {
        $n = 0;
        while ($model->loaded()) {
            $n++;
            $model = ORM::factory($model_name)->where($url_column_name, '=', $url . $n);
            if (!empty($id)) {
                $model->where('id', '!=', $id);
            }
            $model = $model->find();
        }
        $url .= $n;
    }
    
    return $url;
}

function mb_ucfirst($string) {
    $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
    return $string;
}