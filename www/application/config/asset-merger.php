<?php defined('SYSPATH') OR die('No direct script access.');

return array(
    'merge' => Kohana::PRODUCTION,
    'folder' => 'themes/packed',
    'load_paths' => [
        Assets::JAVASCRIPT => DOCROOT . 'themes' . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR,
        Assets::STYLESHEET => DOCROOT . 'themes' . DIRECTORY_SEPARATOR,
    ],
    'processor' => [
        Assets::JAVASCRIPT => 'jsmin',
        //Assets::JAVASCRIPT => 'jshrink',
        Assets::STYLESHEET => 'csscompressor',
    ],
    'themes' => [
        'main-header' => [
            'js' => [
                'jquery/jquery-1.7.2.js',
                'core.js'
            ],
            'css' => [
                'search.css',
                'js/datepicker/datepicker.css',
                'normalize.css',
                'style.css',
                'auth.css',
                'complete_register.css',
                'box.css',
                'table.css',
                'notifies.css',
                'buttons.css',
                'loading.css',
                'message.css',
                'blog.css',
                'pagination.css',
                'leftmenu.css',
                'today.css',
                'statistics.css',
                'registration.css',
                'results.css',
                'counter.css',
                'stories.css',
                'videos.css',
            ]
        ],
        'main-footer' => [
            'js' => [
                'jquery/jquery.easing.js',
                'jquery/jquery.tmpl.min.js',
                'jquery/jquery.json-2.2.js',
                'jquery/jquery.cookie.js',
                'jquery/jquery.scrollto.js',
                'class/eventEmitter.js',
                'swfupload/swfupload.js',
                'swfupload/swfupload.queue.js',
                'class/upload.js',
                'class/auth.js',
                'class/box.js',
                'class/table.js',
                'class/navigation.js',
                'class/message.js',
                'class/notify.js',
                'class/validation.js',
                //'class/today.js',
                'class/statistics.js',
                'class/maps.js',
                'class/total.js',
                'class/counter.js',
                'class/search.js',
                'datepicker/datepicker.js',
                'class/videos.js',
                'onfullload.js',
            ],
            'css' => [
            ]
        ]
    ]
);
