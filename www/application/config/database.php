<?php defined('SYSPATH') or die('No direct access allowed.');

return [
    'default' => [
        'type' => 'PDO',
        'connection' => array(
            'dsn' => 'mysql:dbname=meetramadan;unix_socket=/var/run/mysqld/mysqld.sock',
            'port' => '3306',
            'hostname' => 'localhost',
            'database' => 'meetramadan',
            'username' => 'root',
            'password' => '123456',
            'persistent' => false,
        ),
        'table_prefix' => '',
        'charset' => 'utf8',
        'caching' => false,
        'profiling' => true,
    ],
];
