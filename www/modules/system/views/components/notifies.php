<? /* Система оповещений о событиях */ ?>
<div id="notifies"></div>
<?='<script id="t-notify" type="text/x-jquery-tmpl">'?>
    <div class="notify" id="notify-${id}" style="background-image: url(/themes/images/components/notify/${type.icon});">
        <div class="notify-status">
            <div class="notify-title">Системное сообщение</div>
            <div class="notify-close" onclick="Notify.closeMessage(${id});">Закрыть</div>
        </div>
        <div class="notify-head">
            <div class="notify-text">{{html text}}</div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
<?='</script>'?>
