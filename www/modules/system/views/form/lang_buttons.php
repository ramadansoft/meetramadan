<? foreach (ORM::factory('Language')->where('active', '=', true)->find_all() as $lang) {
    $text_entered = (bool)$item->texts
        ->where('languages_id', '=', $lang->id)
        ->get_model_count();

    if ($text_entered) {
        $img = 'tick';
    } else {
        $img = 'minus';
    }
    ?>
    <div data-link="/admin/<?= mb_strtolower(
        Request::current()->controller()
    ) ?>/savelang/<?= $lang->id ?>/<?= $item->id ?>" class="btn loadcontent">
        <img src="/themes/images/admin/<?= $img ?>.png">&nbsp<?= $lang->code ?>
    </div>
<?
}
