<?
$value = ORM::factory(
    $field['model'],
    array($field['model_key'] => (isset($item->$name)) ? $item->$name : null)
)->$field['model_name'];
echo Form::search(
    $input_name,
    $field,
    'начните набирать название..',
    (isset($item->$name)) ? array($item->$name => $value) : null,
    false
);
