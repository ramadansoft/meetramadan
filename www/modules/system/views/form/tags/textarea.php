<?

if (!isset($field['attributes']['class'])) {
    $field['attributes']['class'] = 'ckeditor';
} else {
    $field['attributes']['class'] = $field['attributes']['class'] . ' ckeditor';
}

$field['attributes']['id'] = $input_name;

$javascript .= "Core.editor('{$input_name}');";

echo Form::textarea($input_name, get_value($name, $item, $values), $field['attributes']);
