<?
if (isset($field['options'])) {
    $options = $field['options'];
} elseif (isset($field['model'])) {
    $model = ORM::factory($field['model']);

    // Отфильтруем данные, если есть необходимость
    if (!empty($field['model_wheres'])) {
        foreach ($field['model_wheres'] as $where) {
            $model->where($where[0], $where[1], $where[2]);
        }
    }

    // Отсортируем данные, если есть необходимость
    if (!empty($field['model_orders'])) {
        $model->order_by($field['model_orders']);
    }

    $options = Arr::Make2Array($model->find_all(), $field['model_key'], $field['model_name']);
    if (!empty($field['placeholder'])) {
        $options = ['' => $field['placeholder']] + $options;
    }
}

$javascript .= "$('select[name=\"$name\"]').chosen({disable_search_threshold: 15});";
$field['attributes']['data-placeholder'] = $label . '..';

$selected = get_value($name, $item, $values);

if ($field['tag'] === 'select-multi') {
    $field['attributes']['multiple'] = '.';
    if ($item->loaded() && empty($values[$name])) {
        $selected = Arr::Make1Array($item->$name->find_all(), $field['model_key']);
    }
}

if (empty($options)) {
    $options = ['' => 'список пуст'];
}

echo Form::select($input_name, $options, $selected, $field['attributes']);