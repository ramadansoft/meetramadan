<label class="checkbox">
    <?= Form::checkbox(
        $input_name,
        1,
        (bool)get_value($name, $item, $values),
        $field['attributes']
    ) . '&nbsp;' . Arr::get($field, 'text'); ?>
</label>