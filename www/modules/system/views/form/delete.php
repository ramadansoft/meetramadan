<div id="delete-form">
    <fieldset>
        <h2><?= $strings['delete']['caption'] ?></h2>

        <p><?= $strings['delete']['answer'] ?></p>
        <?= Form::hidden('confirm', 'yes') ?>
        <div
            class="senddata-token btn btn-primary mr15px"
            data-link="<?= Request::current()->url() ?>"
            data-input='#delete-form'><?= $strings['delete']['button'] ?></div>

        <? if ($LOADTYPE == LT_SHOWMESSAGE) { ?>
            <div class="closemessage btn">Отмена</div>
        <? } elseif ($LOADTYPE == LT_LOADCONTENT) { ?>
            <div class="closecontent btn" style="position: static">Отмена</div>
        <? } ?>
        <div class="clear"></div>
    </fieldset>
</div>