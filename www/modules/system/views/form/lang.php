<?
/**
 * Вьюшка используется для рендера формы редактирования локализации
 */
?>
<h3>
    <?= $current_item->_text->name ?>
    &rarr;
    <?= $current_lang->_text->name ?>
    &nbsp;язык
</h3>

<p>Подсказка: для SEO-полей используйте специальные вставки {key:name}, {key:name_rp},{key:name_pp}</p>
<p>Подсказка: если это пункт меню, и он является потомком пункта меню с подключенным адаптером, то вы можете использовать
дополнительные ключи в текстовых полях, через которые можно добраться до значений текущего элемента адаптера.
Использовать можно следующие ключи:  {item:id} {item:name} {item:name_rp} {item:name_pp} {item:url} {item:seo_title} {item:seo_desc} {item:seo_keywords} {item:seo_h1} {item:seo_text1} {item:seo_text2}
</p>
<table id="redakt" class="redakt" border="1" cellspacing="0" cellpadding="0" bordercolor="#ccc">
    <?= $inputs ?>
</table>

<div class="senddata-token btn btn-primary mt15px"
     id="btn-save"
     data-link="<?= Request::current()->url() ?>"
     data-input="#redakt">Сохранить
</div>
<div class="clear"></div>

