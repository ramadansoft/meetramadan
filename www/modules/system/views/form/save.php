<div id="save-form">
    <h2><?= $strings[$mode]['caption'] ?></h2>
    <table class="redakt" border="1" cellspacing="0" cellpadding="0" bordercolor="#ccc">
        <?= View::factory('form/inputs', ['fields' => $fields, 'item' => $item, 'values' => $values])->render() ?>
        <? if (isset($lang_inputs)) { ?>
            <tr>
                <td colspan="99">
                    <h3>Текстовые данные</h3>

                    <p>Подсказка: для SEO-полей используйте специальные вставки {key:name}, {key:name_rp}, {key:name_pp}</p>
                    <p>Подсказка: если это пункт меню, и он является потомком пункта меню с подключенным адаптером, то вы можете использовать
                    дополнительные ключи в текстовых полях, через которые можно дообраться до значений текущего элемента адаптера.
                    Использовать можно следующие ключи:  {item:id} {item:name} {item:name_rp} {item:name_pp} {item:url} {item:seo_title} {item:seo_desc} {item:seo_keywords} {item:seo_h1} {item:seo_text1} {item:seo_text2}
                    </p>
                </td>
            </tr>
            <?= $lang_inputs ?>
        <? } ?>
        <? if (isset($lang_buttons)) { ?>
            <tr>
                <td colspan="99"><?= $lang_buttons ?></td>
            </tr>
        <? } ?>
    </table>
</div>

<div class="senddata-token btn btn-primary mt15px"
     data-link="<?= Request::current()->url() ?>"
     id="btn-save"
     data-input="#save-form">
    <?= $strings[$mode]['button'] ?>
</div>

<div class="clear"></div>