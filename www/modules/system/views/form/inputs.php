<?
/**
 * Генерация полей для добавления/редактирования записи
 */
$javascript = '';

if (!isset($prefix)) {
    $prefix = '';
}

if (!isset($values)) {
    $values = null;
}

// В некоторых исключительных ситуациях бывает необходимо вытащить номер текущего редактируемого объекта
echo Form::hidden('data_item_id', $item->id);

foreach ($fields as $name => $field) {
    if (!isset($field['attributes'])) {
        $field['attributes'] = [];
    }

    $input_name = $prefix . $name;
    $label = Arr::get($field, 'label');

    if ($field['tag'] == 'hidden') {
        echo Form::hidden($input_name, get_value($name, $item, $values), $field['attributes']);
    } else {

        echo "<tr><td class='td7'>$label</td>";

        
        echo '<td>';
        include("tags/{$field['tag']}.php");
        if (isset($default_item)) {
            echo '<div class="seo-text-content" style="margin-top:5px">' . $default_item->$name . '</div>';
        }
        echo Form::validation($input_name);
        echo '</td></tr>';
    }
}

if (!empty($javascript)) {
    echo '<script type="text/javascript">Core.onFullLoad(function(){ setTimeout(function(){ ' . $javascript . ' },50); });</script>';
}
