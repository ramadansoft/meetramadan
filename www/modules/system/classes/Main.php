<?php

defined('SYSPATH') or die('No direct script access.');

abstract class Main extends Template
{

    /**
     * Массив дополнительных параметров.  На данный момент может содержать три параметра: lang, order, gorder
     * lang - ключает функционал для редактирования текстов на разных языках
     * order - включает обработку порядка записей
     * gorder - включает обработку порядка сгруппированных записей. Для работы также требуется определить параметр $_gorder_group_field с названием поля группировки записей
     * @var array
     */
    protected $_params = [];

    /**
     * Поле для группировки сортировки записей, используется в режиме gorder
     * @var string
     */
    protected $_gorder_group_field = null;

    /**
     * Поднять определенную запись вверх в списке
     * @param int $id Номер записи в таблице
     * @return boolean
     */
    protected function up($id)
    {
        if (Security::check_token()) {
            $model = ORM::factory($this->_model_name, $id);
            if (!$model->loaded()) {
                return false;
            }

            if (in_array('gorder', $this->_params)) {
                $order_field = $this->_gorder_group_field;
                $gorder = new Gorder($model->get_table_name(), $order_field);
                $gorder->set_group($model->$order_field);
                $result = $gorder->up($model->id);
            } else {
                $order = new Order($model->get_table_name());
                $result = $order->up($model->id);
            }

            if ($result) {
                $this->SendJSONData([JSONA_REFRESHPAGE => '']);
                return true;
            } else {
                $this->SendJSONData([JSONA_ERROR => 'Невозможно поднять запись']);
                return false;
            }
        } else {
            $this->security_error();
            return false;
        }
    }

    /**
     * Опустить определенную запись вверх в списке
     * @param int $id Номер записи в таблице
     * @param bool $is_gorder Если установлено в TRUE (по-умолчанию), то запись будет обрабатываться как принадлежащая определенной группе записей в таблице.
     * @return boolean
     */
    protected function down($id)
    {
        if (Security::check_token()) {
            $model = ORM::factory($this->_model_name, $id);
            if (!$model->loaded()) {
                return false;
            }

            if (in_array('gorder', $this->_params)) {
                $order_field = $this->_gorder_group_field;
                $gorder = new Gorder($model->get_table_name(), $order_field);
                $gorder->set_group($model->$order_field);
                $result = $gorder->down($model->id);
            } else {
                $order = new Order($model->get_table_name());
                $result = $order->down($model->id);
            }

            if ($result) {
                $this->SendJSONData([JSONA_REFRESHPAGE => '']);
                return true;
            } else {
                $this->SendJSONData([JSONA_ERROR => 'Невозможно опустить запись']);
                return false;
            }
        } else {
            $this->security_error();
            return false;
        }
    }

    /**
     * Добавляет/изменяет запись в таблице. При желании, можно указать дополнительные значения в массиве $values, за основу же берутся данные из $_POST
     * @param int $id Номер записи в таблице
     * @param array $values Массив значений
     * @param array $params Дополнительные параметры метода
     * @return boolean
     */
    protected function save($id = null, $values = null)
    {
        if (Security::check_token()) {
            $model = ORM::factory($this->_model_name, $id);
            $mode = $model->loaded() ? 'edit' : 'add';
            $controller_strings = $this->get_strings($model);

            $values = array_merge($_POST, is_null($values) ? [] : $values);

            // При создании новой записи и при включенном режиме order, вставляем нужный порядковый номер для сортировки
            if (in_array('order', $this->_params) && $mode === 'add') {
                $order = new Order(ORM::factory($this->_model_name)->get_table_name());
                $values['order'] = $order->get_insert_order();
            }

            if (in_array('gorder', $this->_params)) {
                $gorder_group_field = $this->_gorder_group_field;
                $gorder = new Gorder($model->get_table_name(), $gorder_group_field);

                if ($mode === 'edit') {
                    $old_order_value = $model->order;
                    $old_group_id = $model->$gorder_group_field;
                    // Если это редактирование записи, то обновляем поле order только в том случае, когда была изменена группа записи
                    if ($values[$gorder_group_field] != $old_group_id) {
                        $gorder->set_group($values[$gorder_group_field]);
                        $values['order'] = $gorder->get_insert_order();
                    }
                } else {
                    $gorder->set_group($values[$gorder_group_field]);
                    $values['order'] = $gorder->get_insert_order();
                }
            }

            if ($model->save_item($values)) {

                // Если включен режим gorder, то проверим, не изменилась ли группа у записи
                if ($mode === 'edit' && in_array('gorder', $this->_params)) {
                    if ($model->$gorder_group_field != $old_group_id) {
                        // Вернёмся к старой группе и сдвинем все записи чтобы закрыть пустое место
                        $gorder->set_group($old_group_id);
                        $gorder->update_after_del($old_order_value);
                    }
                }

                // Если включен режим текстов на разных языках, то также сохраним тексты на текущем языке
                if ($mode === 'add' && in_array('lang', $this->_params)) {
                    $this->save_lang($model->id, LANGUAGE);
                }

                $this->SendJSONData(
                    [
                        JSONA_REFRESHPAGE => '',
                        JSONA_COMPLETED => $controller_strings[$mode]['success']
                    ]
                );

                return $model;
            } else {
                $this->SendJSONData(
                    [
                        JSONA_ERROR => $controller_strings[$mode]['error']
                    ]
                );
                return false;
            }
        } else {
            $this->security_error();
            return false;
        }
    }

    /**
     * Удаление записи из таблицы
     * @param type $id
     * @return boolean
     */
    protected function delete($id)
    {
        if (Security::check_token()) {
            $model = ORM::factory($this->_model_name, $id);
            if (!$model->loaded()) {
                return false;
            }

            if (in_array('order', $this->_params)) {
                $old_order = $model->order;
            }

            if (in_array('gorder', $this->_params)) {
                $old_order = $model->order;
                $gorder_group_field = $this->_gorder_group_field;
                $gorder = new Gorder($model->get_table_name(), $gorder_group_field);
                $gorder->set_group($model->$gorder_group_field);
            }

            $model->delete();

            if (in_array('order', $this->_params)) {
                $order = new Order(ORM::factory($this->_model_name)->get_table_name());
                $order->update_after_del($old_order);
            }

            if (in_array('gorder', $this->_params)) {
                $gorder->update_after_del($old_order);
            }

            $this->SendJSONData(
                [
                    JSONA_REFRESHPAGE => ''
                ]
            );

            return true;
        } else {
            $this->security_error();
            return false;
        }
    }

    /**
     * Генерирует форму для создания/редактирования записи
     */
    protected function form_save($id = null, $values = null)
    {
        $model = ORM::factory($this->_model_name, $id);
        $mode = $model->loaded() ? 'edit' : 'add';

        if ($mode === 'edit' && isset($this->_edit)) {
            $fields = $this->_edit;
        } else {
            $fields = $this->_add;
        }

        if ($mode === 'edit' && isset($model->_edit)) {
            $model_fields = $model->_edit_fields;
        } else {
            $model_fields = $model->_add_fields;
        }

        if (count($model_fields) !== count($fields)) {
            throw HTTP_Exception::factory(
                404,
                'количество колонок изменения в модели не совпадает с количеством колонок в контроллере'
            );
        }

        $view = View::factory('form/save');
        $view->fields = $fields;
        $view->mode = $mode;
        $view->strings = $this->get_strings($model);
        $view->params = $this->_params;
        if ($mode === 'add' && in_array('lang', $this->_params)) {
            $view->lang_inputs = $this->form_lang_inputs();
        } elseif ($mode === 'edit' && in_array('lang', $this->_params)) {
            $view->lang_buttons = View::factory('form/lang_buttons', ['item' => $model])->render();
        }
        $view->item = $model;
        $view->values = $values;

        return $view;
    }

    /**
     * Генерирует форму удаления записи
     * @param $id
     * @return View
     * @throws HTTP_Exception
     */
    protected function form_delete($id)
    {
        $model = ORM::factory($this->_model_name, $id);

        if (!$model->loaded()) {
            throw HTTP_Exception::factory(404, 'model ' . $this->_model_name . ' not loaded');
        }

        return View::factory(
            'form/delete',
            [
                'strings' => $this->get_strings($model),
            ]
        );
    }

    /**
     * Генерирует форму для редактирования текста для определенного объекта на определенном языке
     * @param int $item_id ID объекта
     * @param int $lang_id ID языка
     */
    protected function form_lang($item_id, $lang_id)
    {
        $view = View::factory('form/lang');
        $view->model = $this->_model_name;
        $view->fields = $this->_language_table['fields'];
        $view->current_item = ORM::factory($this->_model_name, Request::current()->param('stuff'));
        $view->current_lang = ORM::factory('Language', Request::current()->param('id'));
        $view->inputs = $this->form_lang_inputs($lang_id, $item_id);
        $this->template->content = $view;
        $this->template->title = $view->current_item->_text->name . ' / ' . $view->current_lang->_text->name;
    }

    /**
     * Генерирует форму с полями ввода для текстовых данных объекта на определенном языке
     * @param int $lang_id ID языка
     * @param int $item_id ID объекта
     * @return html
     */
    protected function form_lang_inputs($lang_id = null, $item_id = null)
    {
        $text_item = ORM::factory($this->_language_table['model']);

        if (!(is_null($lang_id) && is_null($item_id))) {
            $text_item->where($this->_language_table['language_key'], '=', $lang_id)
                ->where($this->_language_table['primary_key'], '=', $item_id)
                ->find();
        }

        $view = View::factory(
            'form/inputs',
            [
                'fields' => $this->_language_table['fields'],
                'item' => $text_item,
                'prefix' => 'lang_data_'
            ]
        );

        if (!is_null($lang_id) && $lang_id !== LANGUAGE) {
            $view->default_item = ORM::factory(
                $this->_language_table['model'],
                [
                    $this->_language_table['language_key'] => LANGUAGE,
                    $this->_language_table['primary_key'] => $item_id
                ]
            );
        }

        return $view->render();
    }

    /**
     * Сохраняет текст для определенного объекта на определенном языке
     * @param int $item_id ID объекта
     * @param int $lang_id ID языка
     */
    protected function save_lang($item_id, $lang_id)
    {
        if (Security::check_token()) {
            $text_item = ORM::factory(
                $this->_language_table['model'],
                [
                    $this->_language_table['language_key'] => $lang_id,
                    $this->_language_table['primary_key'] => $item_id
                ]
            );

            $edit = false;
            if ($text_item->loaded()) {
                $edit = true;
            }

            $data = [
                $this->_language_table['language_key'] => $lang_id,
                $this->_language_table['primary_key'] => $item_id
            ];

            foreach ($_POST as $key => $value) {
                if (mb_substr($key, 0, 10) === 'lang_data_') {
                    $data[mb_substr($key, 10)] = $value;
                }
            }

            $text_item->values($data)->save();

            $this->SendJSONData(
                [
                    JSONA_COMPLETED => $edit ? 'Текст обновлен' : 'Текст создан',
                    JSONA_REFRESHPAGE => '',
                ]
            );
        } else {
            $this->security_error();
        }
    }


    protected function controller_lang()
    {
        if ($this->isJson()) {
            $this->save_lang($this->request->param('stuff'), $this->request->param('id'));
        } else {
            $this->form_lang($this->request->param('stuff'), $this->request->param('id'));
        }
    }

    /**
     * Стандартный метод для создания/редактирования записи
     * При обычном обращении генерирует форму, которая отправляет данные на тот же URL
     * Когда приходят данные, обрабатывает их.
     * @param type $values
     */
    protected function controller_save($values = null)
    {
        if ($this->isJson()) {
            $this->save($this->request->param('id'));
        } else {
            $this->template->content = $this->form_save($this->request->param('id'), $values);
        }
    }

    /**
     * Стандартный метод для удаления записи.
     * Работает также, как controller_save()
     */
    protected function controller_delete()
    {
        $id = $this->request->param('id');
        if ($this->isJson()) {
            if (Arr::get($_POST, 'confirm') === 'yes') {
                $this->delete($id);
            } else {
                $this->SendJSONData([JSONA_REFRESHPAGE => '']);
            }
        } else {
            $this->template->content = $this->form_delete($id);
        }
    }

}
