<?php defined('SYSPATH') OR die('No direct script access.');

abstract class Session extends Kohana_Session
{

    public static function instance($type = null, $id = null)
    {
        // Попытаемся вытащить ID сессии из get/post параметра
        if ($id === null) {
            $id = Arr::get_empty($_REQUEST, 'session_id');
        }

        return parent::instance($type, $id);
    }

}
