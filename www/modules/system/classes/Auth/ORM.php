<?php

class Auth_ORM extends Kohana_Auth_ORM
{

    /**
     * Logs a user in.
     *
     * @param   string   username
     * @param   string   password
     * @param   boolean  enable autologin
     * @return  boolean
     */
    protected function _login($user, $password, $remember)
    {
        if (!is_object($user)) {
            $username = $user;

            // Load the user
            $user = ORM::factory('User');
            $user->where('email', '=', $username)->find();
        }
        
        if (is_string($password)) {
            // Create a hashed password
            $password = $this->hash($password);
        }
        
        // If the passwords match, perform a login
        if ($user->has('roles', ROLE_LOGIN) AND $user->password === $password) {
            if ($remember === true) {
                // Token data
                $data = array(
                    'user_id' => $user->id,
                    'expires' => time() + $this->_config['lifetime'],
                    'user_agent' => sha1(Request::$user_agent),
                );

                // Create a new autologin token
                $token = ORM::factory('User_Token')
                    ->values($data)
                    ->create();

                // Set the autologin cookie
                Cookie::set('authautologin', $token->token, $this->_config['lifetime']);
            }

            // Finish the login
            $this->complete_login($user);

            return true;
        }

        // Login failed
        return false;
    }

    /**
     * Logs a user in, based on the authautologin cookie.
     *
     * @return  mixed
     */
    public function auto_login()
    {
        if (Kohana::$environment === Kohana::DEVELOPMENT && isset($_COOKIE['unittest_login'])) {
            return $this->force_login(Arr::get($_COOKIE,'unittest_login'));
        } else {
            return parent::auto_login();
        }
    }
    

}