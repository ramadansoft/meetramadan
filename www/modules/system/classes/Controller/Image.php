<?php

class Controller_Image extends Template {

    /*
     * Обрезка фотографии
     */
    protected function crop($file, $x, $y, $w, $h, $quality = 100)
    {
        $img = Image::factory($file);
        $_x = round($img->width * (float)$x / 100);
        $_y = round($img->height * (float)$y / 100);
        $_w = round($img->width * ((float)$w - (float)$x) / 100);
        $_h = round($img->height * ((float)$h - (float)$y) / 100);
        $img->crop($_w, $_h, $_x, $_y)->save($file, $quality);
    }

    /**
     * Обрезать и сохранить изображение
     */
    function action_save() {
        if (!$this->checkLogin()) return;

        $x = Arr::get($_POST, 'cropX');
        $y = Arr::get($_POST, 'cropY');
        $w = Arr::get($_POST, 'cropW');
        $h = Arr::get($_POST, 'cropH');

        $file = ORM::factory('File', Arr::get($_POST, 'file'));

        if ($file->loaded() && $w > $x && $h > $y) {
            $file->public = 1;
            $file->save();

            $this->crop($file->path(), $x, $y, $w, $h);
            Model_File::makeThumbnails($file,true);

            $this->SendJSONData([
                //JSONA_COMPLETED => $positions,
                //JSONA_COMPLETED => $iscompany ? 'Аватар компании установлен' : 'Аватар пользователя установлен',
                JSONA_SAVE_PHOTO => '',
            ]);
        } else {
            $this->SendJSONData([
                JSONA_ERROR => 'Не удалось сохранить файл. Неверные параметры.',
            ]);
        }
    }
}