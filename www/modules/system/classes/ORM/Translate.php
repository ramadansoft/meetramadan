<?

/**
 * Специальный класс, благодаря которому мы можем легко вытаскивать текст разных
 * таблиц на том языке, на котором работает в данный момент наш сайт
 */
class ORM_Translate extends ORM
{

    /**
     * Returns an ORM model for the given one-one related alias
     *
     * @param  string $alias Alias name
     * @return ORM
     */
    protected function _related($alias)
    {
        if ($alias === '_text') {
            return $this->_related[$alias] = ORM::factory($this->_has_many['texts']['model']);
        } else {
            return parent::_related($alias);
        }
    }

    public function get($column)
    {
        // По свойству "_text" вытаскиваем данные из переводов
        if ($column === '_text' || $column === '_country_text') {
            if (isset($this->_related[$column])) {
                // Return related model that has already been fetched
                return $this->_related[$column];
            } else {
                if ($column === '_text') {
                    // Сперва попробуем найти некст на текущем активном языке
                    $text = $this->texts->where('languages_id', '=', LANGUAGE)->limit(1)->find();

                    // Если текст на текущем активном языке не был найдем,
                    // попробуем найти его в тексте страны и зависимой страны
                    if (!$text->loaded() && !empty(Params::instance()->location->country)) {
                        $text = $this->get_country_text();
                    }

                    // Если в БД нет записи с текущим языком, попытаемся вытащить запись с дефолтным языком
                    if (!$text->loaded() && LANGUAGE !== DEFAULT_LANGUAGE) {
                        $text = $this->texts->where('languages_id', '=', DEFAULT_LANGUAGE)->find();
                    }

                    // Если в БД нет записи ни с текущим, ни с дефолнтым языком.. попытаемся вытащить хоть какой-то текст..
                    if (!$text->loaded()) {
                        $text = $this->texts->limit(1)->find();
                    }
                } elseif ($column === '_country_text') {
                    // В этом случае текст ищется исключительно на языке текущей и зависимой стран,
                    // независимо от того, какой язык установлен пользователем в данный момент в качестве главного.
                    // Это бывает необходимо в некоторых ситуациях, например, в генерации ссылок на пункты меню.
                    // Ссылки не должны меняться вместе с изменением языка.
                    $text = $this->get_country_text();
                }

                return $this->_related[$column] = $text;
            }
        } elseif (Params::instance()->_template === 'default' AND mb_substr($column, 0, 4) === 'seo_') {
            $column_content = parent::get($column);
            if (is_string($column_content)) {
                // Подставим вместо значений {key:name}, {key:name_rp} и {key:name_pp}
                // названия текущей локации в нужном падеже
                $location = Params::instance()->location;
                $keys = array('{key:name}', '{key:name_rp}', '{key:name_pp}');
                $replace = array($location->name, $location->name_rp, $location->name_pp);
                return str_replace($keys, $replace, $column_content);
            } else {
                return $column_content;
            }
        } else {
            return parent::get($column);
        }
    }

    public function get_country_text($country = NULL)
    {
        if (empty($country)) {
            $country = Params::instance()->location->country;
        }
        $text = $this->texts->where('languages_id', '=', $country->languages_id)->find();

        // Если текст на текущем активном языке не был найдем, попробуем найти его в тексте зависимой страны
        if (!$text->loaded() && $country->depend->loaded() && $country->languages_id !== $country->depend->languages_id
        ) {
            $text = $this->texts->where('languages_id', '=', $country->depend->languages_id)->find();
        }

        return $text;
    }

    /**
     * Binds another one-to-one object to this model.  One-to-one objects
     * can be nested using 'object1:object2' syntax
     *
     * @param  string $target_path Target model to bind to
     * @return ORM
     */
    public function with($target_path)
    {
        if ($target_path === '_text') {
            if (isset($this->_with_applied[$target_path])) {
                // Don't join anything already joined
                return $this;
            }

            $parent = $this;
            $target = $this->_related($target_path);
            $parent_path = $this->_object_name;

            // Add to with_applied to prevent duplicate joins
            $this->_with_applied[$target_path] = true;

            // Use the keys of the empty object to determine the columns
            foreach (array_keys($target->_object) as $column) {
                $name = $target_path . '.' . $column;
                $alias = $target_path . ':' . $column;

                // Add the prefix so that load_result can determine the relationship
                $this->select([$name, $alias]);
            }

            $this
                ->join([$target->get_table_name(), $target_path], 'LEFT')
                ->on(
                    $parent_path . '.' . $parent->_primary_key,
                    '=',
                    $target_path . '.' . $parent->_has_many['texts']['foreign_key']
                )
                ->on($target_path . '.languages_id', '=', DB::expr(LANGUAGE));

            return $this;
        } else {
            return parent::with($target_path);
        }
    }
}
