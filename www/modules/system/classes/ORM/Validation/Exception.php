<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * ORM Validation exceptions.
 *
 * @package    Kohana/ORM
 * @author     Kohana Team
 * @copyright  (c) 2008-2009 Kohana Team
 * @license    http://kohanaphp.com/license
 */
class ORM_Validation_Exception extends Kohana_ORM_Validation_Exception
{

    /**
     * Constructs a new exception for the specified model
     *
     * @param  string $alias The alias to use when looking for error messages
     * @param  Validation $object The Validation object of the model
     * @param  string $message The error message
     * @param  array $values The array of values for the error message
     * @param  integer $code The error code for the exception
     * @return void
     */
    public function __construct(
        $alias,
        Validation $object,
        $message = 'Failed to validate array',
        array $values = null,
        $code = 0,
        Exception $previous = null
    ) {
        $this->_alias = $alias;
        $this->_objects['_object'] = $object;
        $this->_objects['_has_many'] = false;

        if (isset($_REQUEST['json']) && $_REQUEST['json'] === 'true') {
            $jsondata = etc::makeJsonData(
                [
                    JSONA_VALIDATION_ERRORS => $this->generateValidationErrors(),
                ]
            );

            die($jsondata);
        } else {

            if (PHP_SAPI === 'cli') {
                foreach ($this->errors('validation') as $error) {
                    Minion_CLI::write($error);
                }
            }

            parent::__construct($alias, $object, $message, $values, $code, $previous);
        }
    }

    /**
     * Сгенерировать одномерный массив ошибок валидации из исключения
     * @return array массив ошибок
     */
    private function generateValidationErrors()
    {
        $_errors = [];
        foreach ($this->errors('validation') as $error) {
            array_push($_errors, $error);
        }
        return $_errors;
    }


}