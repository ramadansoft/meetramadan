<?

class Model_Error extends ORM
{
    protected $_table_name = 'errors';
    protected $_belongs_to = [
        'user' => [
            'model' => 'User',
            'foreign_key' => 'users_id',
        ],
    ];

    /**
     * Подсчитать количество непрочитанных ошибок
     */
    static function notification()
    {
        return DB::query(Database::SELECT, "SELECT count(*) as count FROM `errors` WHERE `readed` = FALSE")
            ->execute()
            ->get('count');
    }

}