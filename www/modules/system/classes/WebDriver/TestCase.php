<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Класс, упрощающий взаимодействие с WebDriver'ом Selenium'а
 */

require_once MODPATH . '/webdriver/classes/__init__.php';
abstract class WebDriver_TestCase extends Kohana_Unittest_TestCase {
    
    protected $main_url = 'http://dev.36n6.ru';
    protected $browser = WebDriverBrowserType::FIREFOX;

    /** @var RemoteWebDriver $driver */
    protected $driver;
    
    function setUp()
    {
        $this->driver = RemoteWebDriver::create(
            'http://localhost:4444/wd/hub', [
                WebDriverCapabilityType::BROWSER_NAME => $this->browser,
            ]
        );
    }
    
    function tearDown()
    {
        $this->driver->quit();
    }
    
    /**
     * Get the URL of the test html.
     */
    protected function getTestPath($path)
    {
        return $this->main_url . $path;
    }
    
    /**
     * Выбрать определенное значение из Select'а в указанном элементе
     * @param RemoteWebElement $el SELECT-Элемент
     * @param string $val Значение для выбора
     */
    function selectOption(RemoteWebElement $el,$val) {
        $el->click();
        $this->click($this->css('option[value="'.$val.'"]',$el));
        
    }
    
    /**
     * Найти элемент по CSS-селектору
     * @param string $selector CSS-селектор
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function css($selector, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::cssSelector($selector));
        } else {
            return $this->driver->findElement(WebDriverBy::cssSelector($selector));
        }
    }

    /**
     * Найти инпут по его имени
     * @param string $name Название инпута
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function name($name, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::name($name));
        } else {
            return $this->driver->findElement(WebDriverBy::name($name));
        }
    }
    
    /**
     * Найти элемент по названию его тэга
     * @param string $name Название тэга
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function tagname($name, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::tagName($name));
        } else {
            return $this->driver->findElement(WebDriverBy::tagName($name));
        }
    }
    
    /**
     * Найти элемент по пути xpath
     * @param string $path Путь
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function xpath($path, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::xpath($path));
        } else {
            return $this->driver->findElement(WebDriverBy::xpath($path));
        }
    }

    /**
     * Найти элемент-ссылку по его тексту, находящемся внутри
     * @param string $text Текст внутри ссылки
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function link($text, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::linkText($text));
        } else {
            return $this->driver->findElement(WebDriverBy::linkText($text));
        }
    }
    
    /**
     * Найти элемент по его ID
     * @param string $name ID элемента
     * @param RemoteWebElement $el (необязательно) элемент, внутри которого производим поиск
     * @return RemoteWebElement
     */
    function id($name, RemoteWebElement $el = NULL) {
        if ($el) {
            return $el->findElement(WebDriverBy::id($name));
        } else {
            return $this->driver->findElement(WebDriverBy::id($name));
        }
    }
    
    /**
     * Произведем клик по указанному элементу
     * @param RemoteWebElement $el
     */
    function click(RemoteWebElement $el) {
        if (PHP_OS === 'Linux') {
            $coords = $el->getCoordinates();
            $this->driver->getMouse()->mouseDown($coords)->mouseUp($coords);
        } else {
            $el->click();
        }
    }
    
    /**
     * Подождать, пока не будет обнаружен на странице определенный объект
     * @param string $selector CSS-селектор
     */
    function waitForCss($selector, $timeout = 30, $text = NULL) {
        $this->driver->wait($timeout)->until(function (RemoteWebDriver $driver) use ($selector,$text) {
            $el = $driver->findElement(WebDriverBy::cssSelector($selector));
            if (is_null($text)) {
                return $el->isDisplayed();
            } else {
                return ($el->isDisplayed() && $el->getText() == $text);
            }
        });
    }
    
    /**
     * Открыть в браузере определенную страницу
     * @param string $link путь
     */
    function open($link) {
       $this->driver->get($this->getTestPath($link));
    }
    
    /**
     * Переместить мышь на указанный элемент
     * @param RemoteWebElement $element
     */
    function mouseOver(RemoteWebElement $element) {
        $this->driver->getMouse()->mouseMove($element->getCoordinates());
    }

    /**
     * Авторизоваться под указанным email'ом
     * @param string $email
     */
    function autologin($email) {
        $this->driver->manage()->addCookie([
            'name'      => 'unittest_login',
            'value'     => $email,
        ]);
    }

}
