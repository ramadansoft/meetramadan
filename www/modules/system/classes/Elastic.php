<?
/**
 * Класс для удобной работы с поисковым движком ElasticSearch
 */
class Elastic
{
    private static $inst = NULL;
    /**
     * 
     * @return Elasticsearch\Client
     */
    static function instance() {
        if (is_null(self::$inst)) {
            self::$inst = new Elasticsearch\Client([
                'hosts' => ['127.0.0.1']
            ]);
        }
        return self::$inst;
    }
    

    /**
     * Создать индекс, если это необходимо
     */
    static function createIndex() {
        $client = self::instance();
        $client->indices()->create([
            'index' => ELASTIC_INDEX,
            'ignore' => 400
        ]);
    }
    
    /**
     * Полностью очистить определенный тип данных
     * @param string $type имя типа данных
     */
    static function clearType($type) {
        self::instance()->deleteByQuery([
            'index' => ELASTIC_INDEX,
            'type'  => $type,
            'body'  => [
                'query' => [
                    'match_all' => []
                ]
            ]
        ]);
    }

    /**
     * Сохранить объект в поисковом индексе
     * @param string $type Тип документа
     * @param int $id Номер документа
     * @param array $data Данные документа
     */
    private static function save($type,$id,$data) {
        self::instance()->index([
            'index' => ELASTIC_INDEX,
            'type'  => $type,
            'id'    => $id,
            'body'  => $data,
        ]);
    }
    
    private static function delete($type,$id) {
        self::instance()->delete([
            'index' => ELASTIC_INDEX,
            'type'  => $type,
            'id'    => $id,
        ]);
    }
    
    /**
     * Добавить пользователя в индекс
     * @param type $user
     */
    static function saveUser($user) {
        self::save(ELASTIC_TYPE_USERS, $user->id, [
            'cities_id'     => (int)$user->cities_id,
            'avatar_id'     => (int)$user->avatar_id,
            'email'         => $user->email,
            'username'      => $user->username,
            'first_name'    => $user->first_name,
            'last_name'     => $user->last_name,
            'patronymic'    => $user->patronymic,
            'sex'           => (bool)$user->sex,
            'is_doctor'     => (bool)$user->is_doctor,
            'specialties'   => Arr::Make1Array($user->specialties->find_all(),'id')
        ]);
    }
    
    static function deleteUser($user) {
        self::delete(ELASTIC_TYPE_USERS, $user->id);
    }
    
    /**
     * Искать объекты по указанным параметрам
     * @param string $type Тип данных
     * @param string $search Поисковый запрос
     * @param array $fields Поля, по которым искать
     * @param array $exists Ассоциативный массив данных, для фильтрации по определенным параметрам
     * @return array
     */
    private static function search($type,$search,$fields = NULL,$exists = NULL) {
        $client = self::instance();
        
        $_search = [];
        foreach(explode(' ',$search) as $search_word) {
            $_search[] = "*{$search_word}*";
        }
        $_search = implode(' ',$_search);
        
        $query = [
            'query_string' => [
                'query' => $_search,
            ],
//            'multi_match' => [
//                'query' => 'Паша',
//                'fields' => ['username','first_name','last_name','patronymic'],
//            ]
//            'match_all' => [] // Найти все объекты
        ];
        if ($fields) {
            $query['query_string']['fields'] = $fields;
        }
        
        if (!empty($exists)) {
            $body = [];
            $body['query']['filtered'] = [
                'query' => $query,
                'filter' => [
                    'term' => $exists
                ],
            ];
        } else {
            $body = [];
            $body['query'] = $query;
        }
        
        $result = $client->search([
            'index' => ELASTIC_INDEX,
            'type'  => $type,
            'body'  => $body
        ]);
        return Arr::Make1Array($result['hits']['hits'],'_id');
    }
    
    /**
     * Найти пользователей по указанному запросу
     * @param string $search Текст запроса
     * @param int $specialties_id Номер специальности пользователя
     * @param bool $only_doctors Искать только среди докторов
     * @return array
     */
    static function searchUsers($search,$specialties_id = NULL, $only_doctors = FALSE) {
        $fields = ['username','first_name','last_name','patronymic'];
        if (!empty($specialties_id)) {
            return self::search(ELASTIC_TYPE_USERS, $search, $fields,[
                'specialties' => $specialties_id,
            ]);
        } else {
            if ($only_doctors) {
                return self::search(ELASTIC_TYPE_USERS, $search, $fields,[
                    'is_doctor' => TRUE
                ]);
            } else {
                return self::search(ELASTIC_TYPE_USERS, $search, $fields);
            }
            
        }
    }
    
}
