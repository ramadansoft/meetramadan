<?

/**
 * Класс для подготовки и отсылки сообщений по протоколу LongPolling
 */
class ClientPooler
{
    private static $url = 'http://127.0.0.1/publish?cid=';
    private static $messages = [];

    static function add($chanel, $message)
    {
        self::$messages[$chanel][] = etc::makeJsonData($message);
    }

    /**
     * Посылает сообщения на указанные каналы и очищает пул сообщений
     * @return boolean|error
     */
    static function send()
    {
        $errors = false;
        $errors_curl = [];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);

        foreach (self::$messages as $channel => $messages) {
            foreach ($messages as $message) {
                curl_setopt($curl, CURLOPT_URL, self::$url . $channel);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $message);
                //curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                $r = curl_exec($curl);

                if (!$r) {
                    $errors = true;
                    $errors_curl[] = curl_error($curl);
                }
            }
        }

        curl_close($curl);

        self::$messages = [];

        if ($errors) {
            foreach ($errors_curl as $error_curl) {
                Kohana::$log->add(Log::ERROR, $error_curl);
            }
            return $errors_curl;
        } else {
            return true;
        }
    }

    /**
     * Отправить определенную команду одному пользователю
     * @param Model_User $users Модель пользователя
     * @param array $message
     */
    static function sendMessageForUser($users, $message)
    {
        if (!is_array($users)) {
            $users = [$users];
        }
        foreach ($users as $user) {
            if (!is_object($user)) {
                $user = ORM::Factory('User', $user);
            }
            self::add($user->channel(), $message);
        }
        self::send();
    }

    /**
     * Отправить определенную команду всем пользователям, которые в данный момент находятся в управлении определенной компанией
     * @param Model_Company $company
     * @param array $message
     */
    static function sendMessageForCompanyUsers($companies, $message)
    {
        if (!is_array($companies)) {
            $companies = [$companies];
        }
        foreach ($companies as $company) {
            foreach ($company->getAllUsersIds() as $user) {
                $channel = hashids::instance()->encrypt((int)$user);
                self::add($channel, $message);
            }
        }
        self::send();
    }
}
