<?

class HTTP_Exception extends Kohana_HTTP_Exception
{
    public function get_response()
    {
        header('Content-Type: text/html; charset=utf-8');
        $response = Response::factory();
        $response->body(Request::factory('/throwMessage/error/' . $this->_code)->execute());
        $response->headers('Access-Control-Allow-Origin', '*');
        $response->status($this->_code);
        return $response;
    }
}