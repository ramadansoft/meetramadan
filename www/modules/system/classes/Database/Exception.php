<?
class Database_Exception extends Kohana_Database_Exception
{
    function __construct($message = "", array $variables = null, $code = 0, \Exception $previous = null)
    {
        if (Database::$transaction_started) {
            // Если была начата транзакция, откатим изменения
            Database::instance()->rollback();
        }
        parent::__construct($message, $variables, $code, $previous);
    }
}