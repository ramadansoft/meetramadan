Total = new function() {

    this.init = function(count,usersscore) {

        var status = 'Ленивец';
        var desc = 'Неужели тебе было настолько не интересено кликать по кружочкам?';

        if (count > 50 && count <= 100) {
            status = 'Джахиль';
            desc = 'Ваш Рамадан прошел мимо вас.. Пусть Аллах позволит вам достичь следующего Рамадана и исправить свое положение!';
        }

        if (count > 100 && count <= 200) {
            status = 'Обладатель слабого имана';
            desc = 'Врятли вам есть чем похвастаться..';
        }

        if (count > 200 && count <= 300) {
            status = 'Слишком близко к дунье';
            desc = 'Награда Аллаха велика, и Он награждает за каждую мелочь, но, к сожалению, вы редко пользовались этим.';
        }

        if (count > 300 && count <= 500) {
            status = 'Вдали от Садов Рая';
            desc = 'Ваш результат лучше, чем у многих, но все же он слишком низкий.';
        }

        if (count > 500 && count <= 700) {
            status = 'Среднестатичный мусульманин';
            desc = 'Поздравляем, вы оказались примерно таким же мусульманином, как большинство мусульман в русскоязычном сегменте.';
        }

        if (count > 700 && count <= 1000) {
            status = 'Образец усердия';
            desc = 'МашаАллах! Ваш результат впечатляет: вы обошли большинство своих братьев и сестер в стремлении довольства Аллаха!';
        }

        if (count > 1000 && count <= 1500) {
            status = 'Идущий прямым путем';
            desc = 'АльхамдулиЛлях, что у нас есть такие братья и сестры, как ты!';
        }

        if (count > 1500 && count <= 2000) {
            status = 'Стремящийся к довольству Аллаха';
            desc = 'Да будет Аллах доволен тобою за твое усердие на Его пути!';
        }

        if (count > 2000 && count <= 3000) {
            status = 'Быстрый как ветер';
            desc = 'Пусть Аллах укрепит и возвысит тебя на твоем пути!';
        }

        if (count > 3000 && count <= 4000) {
            status = 'Мукмин';
            desc = 'МашаАллах! Ты проявил настоящее усердие на пути Аллаха! Но не останавливайся на достигнутом, ведь есть еще много дел, которые ты не успел сделать.';
        }

        if (count > 4000 && count <= 5000) {
            status = 'Летящий как стрела';
            desc = 'Пусть Аллах воздаст тебе благом за твои старания!';
        }

        if (count > 5000 && count <= 6000) {
            status = 'Уали Аллаха';
            desc = 'Поистине, ты являешься одним из приближенных к Аллаху! МашаАллах!';
        }

        if (count > 6000 && count <= 7000) {
            status = 'Недосягаемый';
            desc = 'СубханАллах, сколько же усердия нужно проявить для такого результата? Пусть Аллах воздаст тебе благом за твои усердия!';
        }

        if (count > 7000) {
            status = 'Чуждый';
            desc = 'Земная жизнь ничего не значит для тебя. Пусть Аллах укрепит тебя на этом пути и даст умереть на нем!';
        }

        var animate2 = function() {

            $('#totalscore').css({top: 0}).animate({top:count},{
                duration: 5000,
                easing: 'easeInOutQuint',
                step: function(now, fx) {
                    $(this).html(Math.round(now));
                },
                complete: function() {

                    $('#status,#desc').css({opacity: 0, display : 'block'});
                    $('#status').html(status);
                    $('#desc').html(desc);

                    $('#status').animate({opacity:1},1000,'linear', function() {
                        $('#desc').animate({opacity:1},500,'linear');
                    });
                }
            });

        }

        var animate = function() {

            $('#usersscore').css({top: 0}).animate({top: usersscore},{
                duration: 5000,
                easing: 'easeInOutQuint',
                step: function(now, fx) {
                    $(this).html(Math.round(now));
                },
                complete: function() {
                    setTimeout(animate2, 500);
                }
            });

        }

        if ($('#usersscore').length > 0)
        {
            setTimeout(animate, 500);
        } else {
            setTimeout(animate2, 1000);
        }

    }

};