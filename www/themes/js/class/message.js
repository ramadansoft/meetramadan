Message				= function()							{
    return Message.implement.init();
}

Message.show		= function(url,mess)				{
    Message.implement.show(url,mess);
}
Message.refresh		= function()							{
    Message.implement.refresh();
}
Message.close		= function()							{
    Message.implement.close();
}
Message.sendData	= function(url,mass)		{
    Message.implement.sendData(url,mass);
}
Message.onPageChanged	= function()		{
    Message.implement.onPageChanged();
}
Message.implement = {

    container: '#message-container',
    text: '#message-text',

    imgLoading: '<img src="/themes/images/ajax-loader.gif" alt="Загрузка..." />',
    imgCenterLoading: '<table width="100%" height="100%"><tr><td style="vertical-align: middle; text-align: center;"><img src="/themes/images/ajax-long-loader.gif" /></td></tr></table>',
    old_url: '',	// Последний урл, который загружался
    old_mess: '',	// Последние POST-параметры, которые шли вместе с урлом
    ee:EventEmitter.getInstance(),
    onPageChanged:function(){
    },
    // Загружает и отображает контент в специальной дивке
    show: function(url,mess) {
        var $this = this;
        url = url + '?showmessage=true';

        this.old_url  = url;
        this.old_mess = mess;
		if(Navigation.implement.activePreview!=null)
            Navigation.implement.activePreview.hide();
        this.text.html(this.imgCenterLoading);
        this.container.css({
            opacity:'1',
            display:'block'
        });

        $.ajax({
            type: "POST",
            url: url,
            data: mess,
            dataType: 'html',
            success: function(data) {
                $this.text.html(data);
                $this.ee.emit('AjaxOpComplete');
            },
            error:   function(data, status, e) {
                $this.ee.emit('AjaxOpFailed',e);
                $this.text.html('Ошибка загрузки');
            }
        });
    },

    // Обновить сообщение
    refresh: function() {
        this.show(this.old_url,this.old_mess);
    },

    close: function() {
        this.container.css({
            opacity:0,
            display:'none'
        });
        this.text.html('');
    },

    handleMessage:function(msg) {
        switch(msg.type) {
            case 'nothing':
                this.close();
                this.ee.emit('AjaxOpComplete');
            break;
            case 'error':
                this.ee.emit('AjaxOpFailed',{'data':msg.data, 'status': ''});
                this.ee.emit('OperationError',msg.data);
                //$(this.text).html(msg.data);
            break;
            case 'refreshpage':
                this.ee.emit('AjaxOpComplete');
                this.close();
                Navigation.refreshPage();
            break;
            case 'reloadpage':
                this.ee.emit('AjaxOpComplete');
                Navigation.reloadFullPage(msg.data);
            break;
            case 'redirect':
                this.ee.emit('AjaxOpComplete');
                this.close();
                Navigation.loadPage('http://' + document.domain + msg.data, {});
            break;
            case 'showmessage':
                this.ee.emit('AjaxOpComplete');
                this.show(msg.data, {});
            break;
            case 'completed':
                this.ee.emit('OperationCompleted',msg.data);
            break;
            case 'notice':
                this.ee.emit('OperationNotice',msg.data);
            break;
            case 'warning':
                this.ee.emit('OperationWarning',msg.data);
            break;
            case 'showleftuttons':
                $('#panel-left a').css('display','block');
            break;
            case 'setadminmenu':
                $('#admin_menuitem').css('display',(msg.data == 'on') ? 'block' : 'none');
            break;
            case 'set_system_case':
                // Обновить токен данных
                CORE.token = msg.data;
            break;
            case 'updateStoryInfo':
                $('#story-info-'+msg.data[0]).remove();
                $('#story-'+msg.data[0]).append(msg.data[1]);
            break;
            case 'showLoginWindow':
                Auth.showLogin();
            break;
            case 'closecall': // Закрыть окошко заяки на звонок
                Banner.closeCall();
            break;
            default:
                throw 'No handler for: '+msg.type;
            break;
        }
    },
    sendData: function(url,mass) {

        if (Core.debug == true)
            this.ee.emit('OperationNotice','Посылаем данные на: <a href="' + url + '">'+ url +'</a>');

        this.ee.emit('AjaxOpStart');
        var $this = this;
        if (mass == undefined) mass = {};

        mass.json = 'true';

        // Отошлем массив и посмотрим ответ - нормально все или нет.
        $.ajax({
            type: "POST",
            url: url,
            data: mass,
            dataType: 'json',
            success: function(data) {
                data.map(function(elem){
                    $this.handleMessage.call($this,elem);
                });
                $this.ee.emit('AjaxOpComplete');
            },
            error: function (data, status, e)
            {
				if (data.status == '500')
				{
					Notify.message('Произошла ошибка при выполнении запроса.', 'error');
					//Navigation.reloadFullPage();
				}

                $this.ee.emit('AjaxOpFailed',{
                    data:data,
                    status:status
                });

            }
        });
    },

    init: function () {
        var self = this;
        return self;
    }
};

$(document).ready(function()
{
    Message.implement.container = jQuery(Message.implement.container);
    Message.implement.text = jQuery(Message.implement.text);

    var ee = EventEmitter.getInstance();

    ee.on('PageChanged',function(){
        Message.onPageChanged();
    });


    /*
    ee.on('setLoaderImage',function(el){
        alert('setLoaderImage');
        var html=$(el).html();
        ee.on('AjaxOpComplete',function(){
            $(el).html(html);
        });
        ee.on('AjaxOpFailed',function(data){
            $(el).html(data.status);
        });
        $(el).html(Message.implement.imgLoading);
    });
    */

    // Авторесайзинг окна сообщения
    $(window).resize(function() {
        $('#message-container-2').css('max-height',$(window).height()-100);
    });

    $('#message-container-2').css('max-height',$(window).height()-100);

    /* Создаем методы-хуки для отслеживания нажимаемых кнопок в интерфейсе */
    $('.showmessage, .senddata, .senddata-token').live('click',function() {
        var $this = $(this);

        if ($this.hasClass('really') && !confirm('Действительно выполнить действие?'))
            return false;

        // Предотвращаем выполнение функции, если есть некоторые обстоятельства.
        //if ($this.hasClass('save_button_container-noactive'))
        //    return; Сейчас это уже не используется

        var link = $this.attr('href');
        if (link == undefined || link == '#') link = $this.attr('data-link');
        if (link == undefined) link = document.location.href;

        var input = $this.attr('data-input');

        //Есть событие-при-загрузке Посылаем его всем слушателям.
        var event=$this.attr('data-event');

        if(event)
            ee.emit(event);
        if ($this.hasClass('senddata') || $this.hasClass('senddata-token')) {

            if(input) {
                Core.getInputData(input, function(data){

                    if ($this.hasClass('senddata-token'))
                        data.system_case = CORE.token;

                    Message.sendData(link, data);
                });
            } else {
                var mess = {};

                if ($this.hasClass('senddata-token'))
                    mess.system_case = CORE.token;

                Message.sendData(link, mess);
            }
        } else {
            Message.show(link);
        }

        return false;
    });

    $('.showimage').live('click',function() {
        var $this = $(this);
        var link = $this.attr('href');
        if (link == undefined || link == '#') link = $this.attr('data-link');
        if (link != undefined)
            Message.show('/image/?src='+link);

        return false;
    });

    $('.closemessage').live('click',function() {
        Message.close();
        return false;
    });

    $('.refreshmessage').live('click',function() {
        Message.refresh();
        return false;
    });

    // Закрытие оповещений
    $('.message-error').live('click',function() {
        var $this = $(this);

        $this.css({
            'height':$this.css('height'),
            'min-height':'0px'
        });
        $this.animate({
            height: 0,
            opacity: 0
        },1000,function() {
            $this.remove();
        });
    });

});


