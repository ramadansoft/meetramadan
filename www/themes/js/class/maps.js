Maps = new function(){

    this.init = function(points, element)
    {
        ymaps.ready(function () {

            var map = new ymaps.Map(element, {
                // Центр карты
                center: Maps._getCenter(points),
                // Коэффициент масштабирования
                zoom: 7,
                // Тип карты
                type: "yandex#map",

                behaviors: ['drag','scrollZoom']
            });

            Maps.map = map;

            map.controls
                .add("zoomControl")
                .add("mapTools")
                .add(new ymaps.control.TypeSelector(["yandex#map", "yandex#satellite", "yandex#hybrid", "yandex#publicMap"]));


            var x = 0;
            points.map(function(point){
                x += 1;
                // Создание метки
                var placemark = new ymaps.Placemark(
                    // Координаты метки
                    [point.x,point.y], {
                        hintContent: "",
                        //iconContent: point.title.substr(0,1),
                        iconContent: point.users_count,
                        balloonContent: '<a href="/statistics/cities/'+point.id+'">' + point.title + '</a><p style="margin:0;">Пользователей в городе: ' + point.users_count + '</p>'
                    }, {
                        draggable: false,
                        hideIconOnBallon: false
                    }
                );

                // Добавление метки на карту
                map.geoObjects.add(placemark);

            });

            map.setBounds(Maps._getBounds(points));

        });
    }

    this._getCenter = function(points)
    {
        if (points.length == 0)
        {
            // По умолчанию установим центром Уфу
            return ['54.73453296473352', '55.97780695072327'];
        } else {
            var min_x = 999999999999999;
            var max_x = 0;
            var min_y = 999999999999999;
            var max_y = 0;

            points.map(function(point) {
                point.x = parseFloat(point.x);
                point.y = parseFloat(point.y);

                if (min_x > point.x) min_x = point.x;
                if (min_y > point.y) min_y = point.y;

                if (max_x < point.x) max_x = point.x;
                if (max_y < point.y) max_y = point.y;
            });

            var mid_x = min_x + ((max_x - min_x) / 2);
            var mid_y = min_y + ((max_y - min_y) / 2);

            return [mid_x, mid_y];
        }
    }

    this._getBounds = function(points)
    {
        if (points.length == 0)
        {
            // По умолчанию установим центром Уфу
            return [[54.67657625412595, 55.82953846435543], [54.815603893851296, 56.162561535644485]];
        } else {
            var min_x = 999999999999999;
            var max_x = 0;
            var min_y = 999999999999999;
            var max_y = 0;

            points.map(function(point) {
                point.x = parseFloat(point.x);
                point.y = parseFloat(point.y);

                if (min_x > point.x) min_x = point.x;
                if (min_y > point.y) min_y = point.y;

                if (max_x < point.x) max_x = point.x;
                if (max_y < point.y) max_y = point.y;
            });

            return [ [min_x, min_y], [max_x, max_y]];
        }
    }

    this.map = null;                // Объект карты
    this._placemarks = [];          // Массив меток на карте
    this._pointsContainer = null;   // Контейнер для управляющих меткой элементов

    // Установить текст метки
    this.setMarkText = function(text,num)
    {
        Maps._placemarks[num].properties.set('balloonContent',nl2br(text))
    }


    // Добавить на карту новую метку
    this.addPlacemark = function(point)
    {
        if (typeof(point) == 'undefined')
        {
            var center = Maps.map.getCenter();
            point = {
                x: center[0],
                y: center[1],
                text: 'Новая метка',
                addr: ''
            };
        }

        var num = this._placemarks.length;

        // Создадим инпуты для управления меткой
        this._pointsContainer.append($.tmpl($('#tmpl-coord'),{point : point, num : num}));

        var placemark = new ymaps.Placemark(
            // Координаты метки
            [point.x,point.y], {
                hintContent: "",
                iconContent: num+1,
                balloonContent: nl2br(point.text)
            }, {
                draggable: true,
                hideIconOnBallon: false
            }
        );

        // Добавление метки на карту
        Maps.map.geoObjects.add(placemark);

        placemark.events.add('dragend',function(e) {
            var geoPoint = e.get('target').geometry.getCoordinates();
            //var zoom = e.originalEvent.target.getMap()._zoom;
            Maps.setPlacementString(geoPoint,num);
        });

        // Добавим метку в массив меток
        Maps._placemarks.push(placemark);

    }

};