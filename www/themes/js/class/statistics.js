$(document).ready(function(){
    $('.state-block').live('click', function() {
        var $this = $(this);
        var day = $this.attr('data-day');
        var testId = $this.attr('data-test');
        $.ajax({
            url: '/today/toggle/' + testId,
            data: { day: day },
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.code == '1')
                {    
                    $this.removeClass('checked');
                }
                else if (data.code == '2')
                {
                    $this.addClass('checked');
                }
            },
            error: function (data, status, e)
            {
            }
        });
    });
});