Auth = new function() {
    this.login_container = '#login-form';
    this.reg_container = '#register-form';

    this.showLogin = function() {
        this.closeRegister();
        this.login_container.css('display','block');
    }

    this.closeLogin = function() {
        this.login_container.css('display','none');
    }

    this.showRegister = function() {
        this.closeLogin();
        this.reg_container.css('display','block');
    }

    this.closeRegister = function() {
        this.reg_container.css('display','none');
    }

    this.logout = function() {
        document.location.href = '/auth/logout';
    }

    this.remindPassword = function() {

        var login = $('input[name="login"]').val();

        if (login.length == 0)
        {
            alert('Пожалуйста, укажите ваш e-mail в поле логина');
            return;
        }

        Message.sendData('/auth/remindpassword', {
            email: login
        });
    }

    // Завершение регистрации
    this.checkSecondStep = function() {
        if (! ($('#register-steps #step-2 input[name="fio"]').val()) || (! $('input[name="city_id"]').val()))
        {
            alert('Пожалуйста, укажите свое имя и город');

            return false;
        }
        return true;
    }

    this.changeStep = function(num) {
        $('#register-steps .step').css('display','none');
        $('#register-steps #step-' + num).css('display','block');
     }

}

$(document).ready(function() {
    Auth.login_container = $(Auth.login_container);
    Auth.reg_container = $(Auth.reg_container);
});