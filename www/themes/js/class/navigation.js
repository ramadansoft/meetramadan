Navigation = new function () {
    this.ajaxObj = '';			// Объект ajax-загрузки
    this.content = '#content';	// Объект, содержащий в себе весь HTML-контент страницы

    this.ee = EventEmitter.getInstance(); // EventEmitter Object
    this.pages = {}; // Кеш загруженных страниц

    // Картинки :)
    this.imgLoading = '<img class="loading-process" src="/themes/images/ajax-loader.gif" alt="Загрузка..." />';
    this.loading = $('<div id="preview-loading"></div>');
    this.imgCenterLoading = '<table style="width:100%;height:100%;"><tr><td style="vertical-align: middle; text-align: center;"><img src="/themes/images/ajax-long-loader.gif" /></td></tr></table>';

    this.history = {};

    this.reloadFullPage = function (link) {
        if (link == undefined || link == '') link = document.location.href;
        document.location.href = link;
    };

    this.refreshPage = function () {
        this.loadPage(document.location.href);
    };

    // Подсветим нужные ссылки и уберем подсветку с ненужных в тот момент
    this.activateButtons = function(url) {
        var checkurl = checkUrl(url,{
            'today'         : ['/today'],
            'results'       : ['/globalstatistics'],
            'statistics'    : ['/statistics'],
            'about_project' : ['/about_project'],
            'about_ramadan' : ['/about_ramadan'],
            'settings'      : ['/settings'],
            'videos'        : ['/videos'],
            'stories'       : ['/stories'],
            'admin'         : ['/admin'],
            'other'         : ['/']
        });

        $('#buttons a').removeClass('active');
        if (checkurl !== null) {
            $('#buttons .button-'+checkurl).addClass('active');
        }
    },

    // Установить новый контент на странице
    this.setContent = function (content) {
        this.ee.emit('beforeSetContent');
        this.content.html(content);
        this.ee.emit('afterSetContent');
    };

    // Установим новую страницу
    this.setPage = function (link, html_data, old_history_page) {

        // Расскажем Гуглу о том, что мы загрузили новую страницу
        if (typeof(_gaq) != 'undefined') {
            _gaq.push(['_trackPageview', link]);
            _gaq.push(['_trackEvent', 'Forms', 'AJAX Form']);
        }

        // Расскажем Яндекс-Метрике о том, что мы загрузили новую страницу
        if (typeof(yaCounter15212854) != 'undefined') {
            yaCounter15212854.hit(link);
        }

        // Обновим счетчик ЛайвИнтернет
        //updateLiveInternetCounter();

        // Если браузером поддерживается хистори, используем это
        if (supports_history_api()) {
            if (typeof(window.history.state) == 'number' && old_history_page !== true) {
                this.history[window.history.state].scroll = document.documentElement.scrollTop;
            }
            
            var time = new Date().getTime();
            this.history[time] = {
                html    : html_data,
                scroll  : 0
            };
            
            if (old_history_page !== true) {
                history.pushState(time, null, link);
            } else {
                history.replaceState(time, null, document.location.href);
            }
        }
        
        this.setContent(html_data);
        
        this.activateButtons(link);

        // Оповестим всех об изменении страницы
        this.ee.emit('PageChanged', link);

        // Если cсылка содержит еще и якорь, то направим страницу туда
        if (link.lastIndexOf('#') != -1) {
            this.scrollToAnkor(link);
        } else {
            //$(document).stop().scrollTo(100,200,'linear');
        }
    };

    /*
     * Функция загружает контент с сервера.
     * str      куда отправлсять запрос
     * mess     данные, которые следует передать серверу. Необязательно.
     * */
    this.loadPage = function (http_link, old_history_page) {
        var that = this;

        // Если уже был отправлен какой-либо запрос, отменим его
        if (typeof(this.ajaxObj) === 'object') this.ajaxObj.abort();

        // Оповестим всех о начале загрузки страницы
        this.ee.emit('PageLoadingStart', http_link);
        
        this.ajaxObj = jQuery.ajax({
            type: "GET",
            url: http_link,
            data: {
                onlycontent : 'true' // Попросим наш сервер отдать только контентную часть, без основного шаблона
            },
            dataType: 'html',
            success: function (data) {
                that.ee.emit('PageLoadingSuccess', http_link);
                that.setPage(http_link, data, old_history_page);
                $(document).stop().scrollTo(0,200,'linear');
            },
            error: function (data, status, e) {
                if (e != 'abort') {
                    that.ee.emit('PageLoadingError', http_link);
                    var errm = '<h3>Ошибка загрузки страницы</h3><p>Причина: ' + e + '</p><p>Попробуйте <a href="#" onclick="Navigation.refreshPage(); return false;">обновить страницу</a></p>';
                    that.setPage(http_link, errm);
                } else {
                    that.ee.emit('PageLoadingAbort', http_link);
                }
            }
        });

        if (http_link.lastIndexOf('#') == -1)
            return true;
        else
            return false;
    };

    this.setTitle = function (name) {
        document.title = name;
        $('.page-title').text(name);
    };

    this.scrollToAnkor = function (str) {
        var aname = str.substring(str.lastIndexOf('#') + 1);
        var link = jQuery('a[name=' + aname + ']').get(0);
        if (typeof link == 'object') {
            var linkcoords = getElementPosition(link);
            $.scrollTo(linkcoords.top, 300);
        }
    };

    this.scrollToElement = function (el, speed) {
        el = $(el).get(0);
        if (typeof(el) !== 'undefined') {
            var linkcoords = getElementPosition(el);
            if (typeof(speed) === 'undefined') {
                speed = 300;
            }
            
            $.scrollTo(parseInt(linkcoords.top - 20), {
                duration: speed,
                easing : 'linear',
                axis: 'y'
            });
        }
    };

    this.loadContent = function (url) {
        var container = $('#loadcontent-container');
        $.ajax({
            type: "GET",
            url: url,
            data: { loadcontent: 'true', onlycontent : 'true' },
            dataType: 'html',
            success: function (data) {
                container.html(data);
                container.css('display', 'block');
            },
            error: function (data, status, e) {
                if (e != 'abort') {
                    var errm = '<h3>Ошибка загрузки объекта</h3><p>Причина: ' + e + '</p><p>Попробуйте <a href="#" onclick="Navigation.reloadFullPage(); return false;">обновить страницу</a></p>';
                    container.html(errm);
                }
            }
        });
    };

    /**
     * Сменить домен на другой
     */
    this.changeDomain = function (domain) {
        Message.sendData('/etc/changedomain', {
            domain: domain,
            url: location.pathname
        });
        return false;
    };

};

Core.onFullLoad(function(){
    Navigation.content = jQuery(Navigation.content);

    // Нажатие на ссылки
    $('body').on('click', 'a', function () {
        var $this = jQuery(this);
        var cleared_href = this.href.substring(0, this.href.lastIndexOf('#'));
        var cleared_lasturl = document.location.href.substring(0, document.location.href.lastIndexOf('#'));

        // Если в ссылке есть якорь, и ссылка является точно такой же, как текущая страница, то просто переместим страницу на нужный якорь
        if ((this.href.indexOf('#') !== -1) && (cleared_lasturl === cleared_href)) {
            Navigation.scrollToAnkor(this.href);
            return false;
        } else {
            if (($this.attr('target') !== '_blank')
                && (this.href[this.href.length - 1] !== '#')
                && (!$this.hasClass('showmessage'))
                && (!$this.hasClass('senddata'))
                && (!$this.hasClass('senddata-token'))
                && (this.getAttribute('data-navigation') !== 'base')
                && (!$this.parent().hasClass('cke_button'))) {
            
                    if ($this.attr('onclick')) {
                        return false;
                    } else {
                        //Message.close();
                        Navigation.loadPage(this.href);
                        return false;
                    }
            }

            if (this.href[this.href.length - 1] === '#') {
                return false;
            }

            return true;
        }
    });

    $('body').on('click', '.loadcontent', function () {
        Navigation.loadContent($(this).attr('data-link'));
    });

    var ee = EventEmitter.getInstance();
    ee.on('NeedRefreshPage', function () {
        Navigation.refreshPage();
    });

    if (supports_history_api()) {
        var time = new Date().getTime();
        history.replaceState(time, null, document.location.href);
        Navigation.history[time] = {
            html    : $('#content').html(),
            scroll  : 0
        };
    }
    
    // Оповестим всех о том, что страница загружена
    ee.emit('PageChanged', document.location.href);
});

function supports_history_api() {
    return !!(window.history && history.pushState);
};

if (supports_history_api()) {
    window.addEventListener('popstate', function (e) {
        var time = window.history.state;
        if (typeof(history.state) === 'number' && typeof(Navigation.history[time]) === 'object') {
            
            var html =  Navigation.history[time].html;
            var scroll = Navigation.history[time].scroll;

            Navigation.setContent(html);
            window.scrollTo(0, scroll);
            Navigation.ee.emit('PageChanged', document.location.href);
        } else {
            Navigation.loadPage(document.location.href,true);
        }
        
    }, false);
}