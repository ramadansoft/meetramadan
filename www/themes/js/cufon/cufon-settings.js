function replaceFonts() {
    //Cufon.replace('#content h1, #content h2', {fontFamily: 'Myriad Pro', hover: true});
	Cufon.replace('#content', {fontFamily: 'Myriad Pro light', hover: true});
}

// Когда изменяется страница, заменять на ней шрифты
EventEmitter.getInstance().on('PageChanged',function(){
    replaceFonts();
});

replaceFonts();