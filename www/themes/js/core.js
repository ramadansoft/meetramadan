Core = new function() {

    this.getInputData=function(element,success,fail) {
        if (element != undefined && element != ''){
            var mass = {},
            els=$('input[type="text"], input[type="password"], input[type="hidden"], input[type="checkbox"], select, textarea',element);
            for(var i=0;i<=els.length;i++){
                var $this = $(els[i]),
                    val = undefined;

                // Имя есть - обрабатываем.
                if ($this.attr('name') != undefined)
                {
                    var isValid = Validation.getInstance().validate($this);
                    if (isValid != 'ok') {
                        if(fail)
                            fail(isValid);
                        return false;
                    }

                    // Если имя имеет [] то шлём серверу массив.
                    if($this.attr('name').indexOf('[]') != -1) {
                        //Если массив для хранения выбранных елементов не создан создадим его.
                        if(mass[$this.attr('name')]==undefined) {
                            mass[$this.attr('name')]=[];
                        }
                    }

                    if($this.attr('type') == 'checkbox' && $this.is(':checked')) {
                        val = $this.val();
                    } else if($this.attr('type') != 'checkbox') {
                        val = $this.val();
                    }

                    // Если это textarea и к ней прикреплен редактор - получим его код
                    if ($this.get(0).tagName == 'TEXTAREA')
                    {
                        var red = $this.data('redactor');
                        if (red != undefined)
                        {
                            val = red.getCode();
                        }
                    }

                    if(typeof(mass[$this.attr('name')]) == 'object' && val != undefined) {
                        mass[$this.attr('name')].push(val);
                    }else if(val!=undefined) {
                        mass[$this.attr('name')]=val;
                    }
                }
            }
            success(mass);
        }
    };

    /* Сделать инпут датепикером */
    this.datepicker = function(inputname) {
        var element = $('input[name="'+inputname+'"]');
        var unix_element = $('input[name="unix-'+inputname+'"]');

        element.DatePicker({
            format: 'd.m.Y',
            date: element.val(),
            current: element.val(),
            starts: 1,
            position: 'r',
            onBeforeShow: function(){
                element.DatePickerSetDate(element.val(), true);
            },
            onChange: function(formated, dates){
                unix_element.val(dates.getTime()/1000);
                element.val(formated);
                element.DatePickerHide();
            }
        });
    };

    /* Создать редактор на элементе */
    this.editor = function(element,toolbar) {
        if (toolbar == undefined) toolbar = 'default';
        $(element).redactor({
            'resize' : true,
            'toolbar' : toolbar,
            'lang': 'ru',
            'imageUpload': '/files/uploadimg',
            'path': '/themes/js/redactor/',
            'convertDivs': false,
            'removeClasses': false,
            'cleanUp': false
        });
    };
    
    this.loadRandomVideo = function() {
        $.ajax({
            url: '/videos/random',
            dataType: 'json',
            success: function(data) {
                $('.quran-random .avatar-1').attr({
                    'onclick' : "Video.show('"+data.url+"')",
                    'style'   : "background-image: url(http://i.ytimg.com/vi/"+data.url+"/0.jpg); opacity: 0; display: block;"
                });
                $('.quran-random .avatar-1').animate({opacity:1},1000,function(){
                    $('.quran-random .avatar-1').hide();
                    $('.quran-random .avatar-2').attr({
                        'onclick' : "Video.show('"+data.url+"')",
                        'style'   : "background-image: url(http://i.ytimg.com/vi/"+data.url+"/0.jpg); opacity: 1; display: block;"
                    });
                });
                $('.quran-random .avatar-2').animate({opacity:0},900,function(){});
                $('.quran-random .text').animate({opacity:0},300,function(){
                    $('.quran-random .text').html(data.name).animate({opacity:1},200);
                });
            }
        });
    };

    /**
     * Возвращает true, когда страница будет полностью загружена
     * @returns {Boolean}
     */
    this.onFullLoad = function(func) {
        if (this.onFullLoadInited) {
            func();
        } else {
            this.onFullLoadFuncs.push(func);
        }
    };
    this.onFullLoadFuncs = [];
    this.onFullLoadInited = false;
    
    this.fullLoad = function() {
        this.onFullLoadFuncs.map(function(el){
            el();
        });
        this.onFullLoadInited = true;
    };

    this.debug = false;
};

$(document).ready(function()
{
    $.extend({
        clearTable:function(table){
            $(table).find('tbody tr').remove();
        }
    });

    $('.really').live('click',function(){
        if ($(this).hasClass('senddata') || $(this).hasClass('senddata-token'))
            return;

        if (!confirm('Действительно выполнить действие?'))
            return false;
    });

    Navigation.activateButtons(document.location.href);
});

// Вывести информацию
function log(text)
{
    console.log(text);
    if (Core.debug)
    {
        Notify.message('Сообщение: "' + text + '"', 'notice');
    }
}

function onEnter(ev, handler)
{
    ev = ev || window.event;

    if (ev.keyCode == 13) {
        if (typeof(handler) !== 'function')
            return true;

        handler();
    }

    return false;
}

function onCtrlEnter(ev, handler)
{
    ev = ev || window.event;

    if (ev.keyCode == 10 || ev.ctrlKey && ev.keyCode == 13) {
        if (typeof(handler) !== 'function')
            return true;

        handler();
    }

    return false;
}

function getRandomInt(min, max)
{
    // использование Math.round() даст неравномерное распределение!
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Узнать координаты элемента
function getElementPosition(elem)
{
    //var elem = document.getElementById(elemId);

    var w = elem.offsetWidth;
    var h = elem.offsetHeight;

    var l = 0;
    var t = 0;

    while (elem)
    {
        l += elem.offsetLeft;
        t += elem.offsetTop;
        elem = elem.offsetParent;
    }

    return {
        "left":l,
        "top":t,
        "width": w,
        "height":h
    };
}

// Сперли функцию из Вконтакте, спасибо им :)
function onCtrlEnter(ev, handler)
{
    ev = ev || window.event;
    if (ev.keyCode == 10 || ev.ctrlKey && ev.keyCode == 13) {
        handler();
    }
}

// Функция получает ассоциативный массив и превращает его в get-сообщение
function ArrayToGet(mass)
{
    var s = '?';
    var x = false;

    for (var item in mass) {
        if (x == false) {
            s = s + item + '=' + mass[item];
            x = true;
        } else {
            s = s + '&' + item + '=' + mass[item];
        }
    }

    return s;
}

/* Функция проверяет массив урлов на соответствие, если найдет соответствие - то возвращает этот урл
* Используется в классе навигации для определения текущей ссылки.
*/
function checkUrl(url,strs)
{
    for (var key in strs)
    {
        for (var url_key in strs[key])
        {
            if (!!(url.indexOf('http://' + document.domain + strs[key][url_key]) + 1))
                return key;
        }
    }

    return null;
}
//Check for missing functions at first. DONT move this to doc.ready!
//Stupid IE 8-9 HAS NO Array.map (Like PS3 gamers HAS NO GAMES).
if (!Array.prototype.map)
{
    Array.prototype.map = function(fun /*, thisp*/)
    {
        var len = this.length;
        if (typeof fun != "function")
            throw new TypeError();

        var res = new Array(len);
        var thisp = arguments[1];
        for (var i = 0; i < len; i++)
        {
            if (i in this)
                res[i] = fun.call(thisp, this[i], i, this);
        }

        return res;
    };
};

//IE HAS NO String.trim
if(typeof String.prototype.trim !== 'function') {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    }
}

// Предзагрузка изображений
function preload(images) {
    if (typeof document.body == "undefined") return;
    try {
        var div = document.createElement("div");
        var s = div.style;
        s.position = "absolute";
        s.top = s.left = 0;
        s.visibility = "hidden";
        document.body.appendChild(div);
        div.innerHTML = "<img src=\"" + images.join("\" /><img src=\"") + "\" />";
    } catch(e) {
        // Error. Do nothing.
    }
}

function getClientWidth()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}

function getClientHeight()
{
  return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}

function nl2br(str) {
	return str.replace(/([^>])\n/g, '$1<br/>');
}

// Клик по галочке "Мне понравилось в анонимных историях"
function likestory(story_id) {
    Message.sendData('/stories/like/' + story_id);
}

function toggleTest(day,test_id,element) {
    var $this = $(element);
    $.ajax({
        url: '/today/toggle/' + test_id,
        data: { day: day },
        type: 'POST',
        dataType: 'json',
        success: function(data) {
            if (data.code == '1')
            {    
                $this.removeClass('checked');
            }
            else if (data.code == '2')
            {
                $this.addClass('checked');
            }
        },
        error: function (data, status, e) {
        }
    });
}